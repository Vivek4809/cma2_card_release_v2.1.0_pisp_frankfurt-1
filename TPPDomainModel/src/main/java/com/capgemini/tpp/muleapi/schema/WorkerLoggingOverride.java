
package com.capgemini.tpp.muleapi.schema;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "enabled"
})
public class WorkerLoggingOverride implements Serializable
{

    @JsonProperty("enabled")
    private Boolean enabled;
    private static final long serialVersionUID = -2748791335144037961L;

	@JsonProperty("enabled")
	public Boolean isEnabled() {
		return enabled;
	}

    @JsonProperty("enabled")
    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

}