package com.capgemini.tpp.muleapi.schema;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Apis implements Serializable {

	private boolean enabled;

	private static final long serialVersionUID = 4394665404139102304L;

	@JsonProperty("enabled")
	public boolean isEnabled() {
		return enabled;
	}

	@JsonProperty("enabled")
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

}