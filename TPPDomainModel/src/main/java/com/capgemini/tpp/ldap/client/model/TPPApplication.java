/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL

 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.tpp.ldap.client.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * The Class TPPApplication.
 */

public class TPPApplication implements Serializable {
	

	private static final long serialVersionUID = 1L;
	private String cn;
	private String clientId;
	
	private List<String> redirectUrl;
	
	private String status;

	private List<String> softwareRoles;
	
	private String softwareClientName;
	private String softwareClientDescription;
	private Double softwareVersion;
	
	private String jwksUrl;
	private String termOfServiceURI;
	private String policyURI;
	private String muleAppId;
	private Date createTimestamp;
	

	public Date getCreateTimestamp() {
		return createTimestamp;
	}


	public void setCreateTimestamp(Date createTimestamp) {
		this.createTimestamp = createTimestamp;
	}


	public String getCn() {
		return cn;
	}


	public void setCn(String cn) {
		this.cn = cn;
	}



	public String getClientId() {
		return clientId;
	}



	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<String> getRedirectUrl() {
		return redirectUrl;
	}


	public void setRedirectUrl(List<String> redirectUrl) {
		this.redirectUrl = redirectUrl;
	}


	public List<String> getSoftwareRoles() {
		return softwareRoles;
	}


	public void setSoftwareRoles(List<String> softwareRoles) {
		this.softwareRoles = softwareRoles;
	}


	public String getSoftwareClientName() {
		return softwareClientName;
	}


	public void setSoftwareClientName(String softwareClientName) {
		this.softwareClientName = softwareClientName;
	}


	public String getSoftwareClientDescription() {
		return softwareClientDescription;
	}

	public void setSoftwareClientDescription(String softwareClientDescription) {
		this.softwareClientDescription = softwareClientDescription;
	}


	public Double getSoftwareVersion() {
		return softwareVersion;
	}


	public void setSoftwareVersion(Double softwareVersion) {
		this.softwareVersion = softwareVersion;
	}


	public String getJwksUrl() {
		return jwksUrl;
	}


	public void setJwksUrl(String jwksUrl) {
		this.jwksUrl = jwksUrl;
	}

	public String getTermOfServiceURI() {
		return termOfServiceURI;
	}


	public void setTermOfServiceURI(String termOfServiceURI) {
		this.termOfServiceURI = termOfServiceURI;
	}

	public String getPolicyURI() {
		return policyURI;
	}


	public void setPolicyURI(String policyURI) {
		this.policyURI = policyURI;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getMuleAppId() {
		return muleAppId;
	}

	public void setMuleAppId(String muleAppId) {
		this.muleAppId = muleAppId;
	}


	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TPPApplication [cn=");
		builder.append(cn);
		builder.append(", clientId=");
		builder.append(clientId);
		builder.append(", redirectUrl=");
		builder.append(redirectUrl);
		builder.append(", status=");
		builder.append(status);
		builder.append(", softwareRoles=");
		builder.append(softwareRoles);
		builder.append(", softwareClientName=");
		builder.append(softwareClientName);
		builder.append(", softwareClientDescription=");
		builder.append(softwareClientDescription);
		builder.append(", softwareVersion=");
		builder.append(softwareVersion);
		builder.append(", jwksUrl=");
		builder.append(jwksUrl);
		builder.append(", termOfServiceURI=");
		builder.append(termOfServiceURI);
		builder.append(", policyURI=");
		builder.append(policyURI);
		builder.append(", muleAppId=");
		builder.append(muleAppId);
		builder.append(", createTimestamp=");
		builder.append(createTimestamp);
		builder.append("]");
		return builder.toString();
	}

	
		
}