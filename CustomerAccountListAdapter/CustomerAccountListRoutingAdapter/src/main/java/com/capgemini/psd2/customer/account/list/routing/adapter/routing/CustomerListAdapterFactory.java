package com.capgemini.psd2.customer.account.list.routing.adapter.routing;

import com.capgemini.psd2.aisp.adapter.CustomerAccountListAdapter;

public interface CustomerListAdapterFactory {
	public CustomerAccountListAdapter getAdapterInstance(String coreSystemName);
}
