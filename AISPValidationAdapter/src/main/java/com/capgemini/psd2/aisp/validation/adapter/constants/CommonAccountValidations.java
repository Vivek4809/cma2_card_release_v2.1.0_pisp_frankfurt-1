package com.capgemini.psd2.aisp.validation.adapter.constants;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Currency;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.validator.routines.IBANValidator;
import org.iban4j.CountryCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.OBPSD2Exception;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.OBExchangeRate1;
import com.capgemini.psd2.pisp.domain.OBExchangeRateType2Code;
import com.capgemini.psd2.pisp.domain.OBExternalPaymentContext1Code;
import com.capgemini.psd2.pisp.domain.OBPostalAddress6;
import com.capgemini.psd2.pisp.domain.OBRemittanceInformation1;
import com.capgemini.psd2.pisp.domain.OBRisk1;
import com.capgemini.psd2.pisp.domain.OBRisk1DeliveryAddress;
import com.capgemini.psd2.pisp.domain.OBWriteInternationalConsent1;
import com.capgemini.psd2.aisp.validation.AispUtilities;
import com.capgemini.psd2.utilities.DateUtilitiesCMA2;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
@ConfigurationProperties("app")
public class CommonAccountValidations {

	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	private List<String> validSchemeNameList = new ArrayList<>();
	private List<String> validLocalInstrumentList = new ArrayList<>();
	private List<String> validFileTypeList = new ArrayList<>();

	public List<String> getValidSchemeNameList() {
		return validSchemeNameList;
	}

	public List<String> getValidLocalInstrumentList() {
		return validLocalInstrumentList;
	}

	public List<String> getValidFileTypeList() {
		return validFileTypeList;
	}

	private final Pattern bbanPattern = Pattern.compile("^[A-Z]{4}\\d{14}$");

	public void validateSchemeNameWithIdentification(String inputScheme, String identification,
			OBErrorCodeEnum errorCodeEnum) {
		if (validSchemeNameList.contains(inputScheme) == false)

			if (errorCodeEnum == null) {
				throw OBPSD2Exception.populateOBException(OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_SCHEME,
						ErrorMapKeys.INVALID_SCHEME);
			} else {
				throw OBPSD2Exception.populateOBException(errorCodeEnum,
						InternalServerErrorMessage.INVALID_SCHEME_RESPONSE);
			}

		switch (inputScheme) {
		case PaymentSetupConstants.UK_OBIE_IBAN:
		case PaymentSetupConstants.IBAN:
			validateIBAN(identification, errorCodeEnum);
			break;
		case PaymentSetupConstants.UK_OBIE_SORTCODEACCOUNTNUMBER:
		case PaymentSetupConstants.SORTCODEACCOUNTNUMBER:
			validateSortCodeAccountNumber(identification, errorCodeEnum);
			break;
		case PaymentSetupConstants.UK_OBIE_PAN:
		case PaymentSetupConstants.PAN:
			validatePAN(identification, errorCodeEnum);
			break;
		case PaymentSetupConstants.UK_OBIE_BBAN:
			validateBBAN(identification, errorCodeEnum);
			break;
		case PaymentSetupConstants.UK_OBIE_PAYM:
			validatePaym(identification, errorCodeEnum);
			break;
		}
	}

	public void validateIBAN(String iban, OBErrorCodeEnum errorCodeEnum) {
		try {
			if (!IBANValidator.getInstance().isValid(iban.trim())) {
				if (errorCodeEnum == null) {
					throw OBPSD2Exception.populateOBException(OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_ACCOUNTIDENTIFIER,
							ErrorMapKeys.ACCOUNT_IDENTIFIER_INVALID);
				} else {
					throw OBPSD2Exception.populateOBException(errorCodeEnum,
							InternalServerErrorMessage.IBAN_VALIDATION_FAILED);
				}
			}
		} catch (Exception exception) {
			if (errorCodeEnum == null) {
				throw OBPSD2Exception.populateOBException(OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_ACCOUNTIDENTIFIER,
						ErrorMapKeys.ACCOUNT_IDENTIFIER_INVALID);
			} else {
				throw OBPSD2Exception.populateOBException(errorCodeEnum,
						InternalServerErrorMessage.IBAN_VALIDATION_FAILED);
			}
		}

	}

	public void validateSortCodeAccountNumber(String identification, OBErrorCodeEnum errorCodeEnum) {
		if (!identification.matches("^[0-9]{6}[0-9]{8}$")) {
			if (errorCodeEnum == null) {
				throw OBPSD2Exception.populateOBException(OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_ACCOUNTIDENTIFIER,
						ErrorMapKeys.ACCOUNT_IDENTIFIER_INVALID);
			} else {
				throw OBPSD2Exception.populateOBException(errorCodeEnum,
						InternalServerErrorMessage.INVALID_SORTCODE_NUMBER);
			}
		}
	}

	public void validateBBAN(String bban, OBErrorCodeEnum errorCodeEnum) {
		try {
			Matcher matcher = bbanPattern.matcher(bban);
			if (!matcher.find()) {
				if (errorCodeEnum == null) {
					throw OBPSD2Exception.populateOBException(OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_ACCOUNTIDENTIFIER,
							ErrorMapKeys.ACCOUNT_IDENTIFIER_INVALID);
				} else {
					throw OBPSD2Exception.populateOBException(errorCodeEnum,
							InternalServerErrorMessage.BBAN_VALIDATION_FAILED);
				}
			}
		} catch (Exception exception) {
			if (errorCodeEnum == null) {
				throw OBPSD2Exception.populateOBException(OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_ACCOUNTIDENTIFIER,
						ErrorMapKeys.ACCOUNT_IDENTIFIER_INVALID);
			} else {
				throw OBPSD2Exception.populateOBException(errorCodeEnum,
						InternalServerErrorMessage.BBAN_VALIDATION_FAILED);
			}
		}
	}

	public void validatePAN(String schemeName, OBErrorCodeEnum errorCodeEnum) {

	}

	private void validatePaym(String identification, OBErrorCodeEnum errorCode) {

	}

	public void validateISOCountry(String countryCode, OBErrorCodeEnum errorCode) {
		if (null != countryCode && CountryCode.getByCode(countryCode) == null) {
			if (errorCode == null)
				throw OBPSD2Exception.populateOBException(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID,
						ErrorMapKeys.INVALID_COUNTRY_CODE);
			else
				throw OBPSD2Exception.populateOBException(errorCode, InternalServerErrorMessage.INVALID_COUNTRY_CODE);
		}
	}

	public boolean isValidCurrency(String currencyCode, OBErrorCodeEnum errorCode) {

		if (isEmpty(currencyCode)) {
			if (errorCode == null)
				throw OBPSD2Exception.populateOBException(OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_CURRENCY,
						ErrorMapKeys.CURRENCY_NOT_SUPPORTED);
			else
				throw OBPSD2Exception.populateOBException(errorCode, InternalServerErrorMessage.UNSUPPORTED_CURRENCY);
		}
		try {
			Currency.getInstance(currencyCode.trim());
			return true;
		} catch (Exception exception) {
			if (errorCode == null)
				throw OBPSD2Exception.populateOBException(OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_CURRENCY,
						ErrorMapKeys.CURRENCY_NOT_SUPPORTED);
			else
				throw OBPSD2Exception.populateOBException(errorCode, InternalServerErrorMessage.UNSUPPORTED_CURRENCY);
		}
	}

	public boolean isEmpty(String data) {
		boolean status = false;
		if (data == null || data.trim().length() == 0)
			status = true;

		return status;
	}

	public void validateSchemeNameWithSecondaryIdentification(String inputScheme, String secondaryIdentification) {
		if (validSchemeNameList.contains(inputScheme) == false)
			throw OBPSD2Exception.populateOBException(OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_SCHEME,
					ErrorMapKeys.INVALID_SCHEME);

		switch (inputScheme) {
		case PaymentSetupConstants.UK_OBIE_IBAN:
		case PaymentSetupConstants.IBAN:
		case PaymentSetupConstants.UK_OBIE_SORTCODEACCOUNTNUMBER:
		case PaymentSetupConstants.SORTCODEACCOUNTNUMBER:
			validateSecondaryIdentification(secondaryIdentification);
			break;
		}
	}

	private void validateSecondaryIdentification(String secondaryIdentification) {
		if (NullCheckUtils.isNullOrEmpty(secondaryIdentification)
				|| secondaryIdentification.length() > EnumAccountConstraint.SECONDARYIDENTIFCATION_LENGTH.getSize())
			throw OBPSD2Exception.populateOBException(OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_ACCOUNTSECONDARYIDENTIFIER,
					ErrorMapKeys.ACCOUNT_SECONDARYIDENTIFIER_INVALID);

	}

	public void validateIdentification(String identification) {
		if (NullCheckUtils.isNullOrEmpty(identification))
			throw OBPSD2Exception.populateOBException(OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_ACCOUNTIDENTIFIER,
					ErrorMapKeys.ACCOUNT_IDENTIFIER_INVALID);
	}

	public void validateDomesticAddressLine(List<String> addressLine, OBErrorCodeEnum errorCodeEnum) {
		if (!NullCheckUtils.isNullOrEmpty(addressLine)) {
			for (String addressLineValue : addressLine) {
				if (!NullCheckUtils.isNullOrEmpty(addressLineValue)) {
					if (addressLineValue.length() > EnumAccountConstraint.ADDRESSLINE_LENGTH.getSize()) {
						if (errorCodeEnum == null) {
							throw OBPSD2Exception.populateOBException(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID,
									ErrorMapKeys.INVALID_ADDRESS_LINE);
						} else {
							throw OBPSD2Exception.populateOBException(errorCodeEnum,
									InternalServerErrorMessage.INVALID_ADDRESS_LENGTH);
						}
					}
				} else {
					if (errorCodeEnum == null) {
						throw OBPSD2Exception.populateOBException(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID,
								ErrorMapKeys.INVALID_ADDRESS_LINE);
					} else {
						throw OBPSD2Exception.populateOBException(errorCodeEnum,
								InternalServerErrorMessage.INVALID_ADDRESS_LENGTH);
					}
				}
			}
		}
	}

	public void validateDomesticCountrySubDivision(String countrySubDivision) {
		if (!NullCheckUtils.isNullOrEmpty(countrySubDivision)
				&& countrySubDivision.length() > EnumAccountConstraint.COUNTRYSUBDIVISION_LENGTH.getSize())
			throw OBPSD2Exception.populateOBException(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID,
					ErrorMapKeys.INVALID_COUNTRY_SUBDIVISION);

	}

	public void validateDomesticCountrySubDivision(List<String> countrySubDivision, OBErrorCodeEnum errorCodeEnum) {
		if (!NullCheckUtils.isNullOrEmpty(countrySubDivision)) {
			int countrySubDivisionLength = 0;
			for (String countrySubDivisionValue : countrySubDivision) {
				if (!NullCheckUtils.isNullOrEmpty(countrySubDivisionValue)) {
					countrySubDivisionLength += countrySubDivisionValue.length();
				}
			}
			if (countrySubDivisionLength > EnumAccountConstraint.COUNTRYSUBDIVISION_LENGTH.getSize()) {
				if (errorCodeEnum == null) {
					throw OBPSD2Exception.populateOBException(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID,
							ErrorMapKeys.INVALID_COUNTRY_SUBDIVISION);
				} else {
					throw OBPSD2Exception.populateOBException(errorCodeEnum,
							InternalServerErrorMessage.INVALID_COUNTRY_SUBDIVISION);
				}
			}
		}
	}

	public void validateLocalInstrument(String localInstrument, OBErrorCodeEnum errorCode) {

		if (null != localInstrument && validLocalInstrumentList.contains(localInstrument) == false) {
			if (errorCode == null)
				throw OBPSD2Exception.populateOBException(OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_LOCALINSTRUMENT,
						ErrorMapKeys.LOCALINSTRUMENT_INVALID);
			else
				throw OBPSD2Exception.populateOBException(errorCode,
						InternalServerErrorMessage.INVALID_LOCAL_INSTRUMENT);
		}
	}

	public void validateRemittanceInformation(String localInstrument, OBRemittanceInformation1 remittanceInformation) {
		// The Faster Payments Scheme can only accept 18 characters for the
		// ReferenceInformation field - which is where this ISO field will be
		// mapped."
		// Request having RemittanceInformation block MAY have value of OPTIONAL
		// Reference field having length in between 1-35 characters
		if (!NullCheckUtils.isNullOrEmpty(remittanceInformation)) {
			if ("UK.OBIE.FPS".equals(localInstrument)
					&& ((NullCheckUtils.isNullOrEmpty(remittanceInformation.getReference()) || remittanceInformation
							.getReference().length() > EnumAccountConstraint.REFERENCE_LENGTH.getSize()))) {
				throw OBPSD2Exception.populateOBException(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID,
						ErrorMapKeys.REMITTANCE_ERROR);
			}
		}

	}

	public void validateDomesticRiskDeliveryAddress(OBRisk1DeliveryAddress obRisk1DeliveryAddress,
			OBErrorCodeEnum errorCodeEnum) {
		if (!NullCheckUtils.isNullOrEmpty(obRisk1DeliveryAddress)) {
			validateDomesticAddressLine(obRisk1DeliveryAddress.getAddressLine(), errorCodeEnum);
			validateISOCountry(obRisk1DeliveryAddress.getCountry(), errorCodeEnum);
		}
	}

	public void validateDomesticCreditorPostalAddress(OBPostalAddress6 creditorPostalAddress,
			OBErrorCodeEnum errorCodeEnum) {
		if (!NullCheckUtils.isNullOrEmpty(creditorPostalAddress)) {
			validateDomesticAddressLine(creditorPostalAddress.getAddressLine(), errorCodeEnum);
			validateDomesticCountrySubDivision(creditorPostalAddress.getCountrySubDivision());
			validateISOCountry(creditorPostalAddress.getCountry(), errorCodeEnum);
		}
	}

	public void validateDomesticPaymentContext(OBRisk1 obRisk1) {
		if (obRisk1.getPaymentContextCode() != null) {

			if (NullCheckUtils.isNullOrEmpty(obRisk1.getMerchantCategoryCode())
					|| NullCheckUtils.isNullOrEmpty(obRisk1.getMerchantCustomerIdentification())) {

				if (OBExternalPaymentContext1Code.ECOMMERCEGOODS.equals(obRisk1.getPaymentContextCode())
						|| OBExternalPaymentContext1Code.ECOMMERCESERVICES.equals(obRisk1.getPaymentContextCode())) {
					throw OBPSD2Exception.populateOBException(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID,
							ErrorMapKeys.PAYMENT_CONTEXT_ERROR);
				}
			}

			if (OBExternalPaymentContext1Code.ECOMMERCEGOODS.equals(obRisk1.getPaymentContextCode())
					&& NullCheckUtils.isNullOrEmpty(obRisk1.getDeliveryAddress())) {

				throw OBPSD2Exception.populateOBException(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID,
						ErrorMapKeys.PAYMENT_CONTEXT_ERROR);
			}
		}
	}

	public void validateHeaders() {
		System.out.println("Entered in CommonPaymentValidations.validateHeaders()");
		if (NullCheckUtils.isNullOrEmpty(reqHeaderAtrributes.getIdempotencyKey()))
			throw OBPSD2Exception.populateOBException(OBErrorCodeEnum.UK_OBIE_HEADER_MISSING, ErrorMapKeys.HEADER);
		if (!reqHeaderAtrributes.getIdempotencyKey().matches("^(?!\\s)(.*)(\\S)$"))
			throw OBPSD2Exception.populateOBException(OBErrorCodeEnum.UK_OBIE_HEADER_INVALID, ErrorMapKeys.HEADER);
		if (reqHeaderAtrributes.getIdempotencyKey().trim().length() > EnumAccountConstraint.IDEMPOTENCY_KEY_LENGTH
				.getSize())
			throw OBPSD2Exception.populateOBException(OBErrorCodeEnum.UK_OBIE_HEADER_INVALID, ErrorMapKeys.HEADER);
		System.out.println("Exited from CommonPaymentValidations.validateHeaders()");
	}

	public void validateInternationalPaymentContext(OBRisk1 risk) {
		if (risk.getPaymentContextCode() != null) {

			if (NullCheckUtils.isNullOrEmpty(risk.getMerchantCategoryCode())
					|| NullCheckUtils.isNullOrEmpty(risk.getMerchantCustomerIdentification())) {

				if (OBExternalPaymentContext1Code.ECOMMERCEGOODS.equals(risk.getPaymentContextCode())
						|| OBExternalPaymentContext1Code.ECOMMERCESERVICES.equals(risk.getPaymentContextCode())) {
					throw OBPSD2Exception.populateOBException(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID,
							ErrorMapKeys.PAYMENT_CONTEXT_ERROR);
				}
			}

			if (OBExternalPaymentContext1Code.ECOMMERCEGOODS.equals(risk.getPaymentContextCode())
					&& NullCheckUtils.isNullOrEmpty(risk.getDeliveryAddress())) {

				throw OBPSD2Exception.populateOBException(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID,
						ErrorMapKeys.PAYMENT_CONTEXT_ERROR);
			}
		}

	}

	public void validateInternationalRiskDeliveryAddress(OBRisk1DeliveryAddress deliveryAddress,
			OBErrorCodeEnum errorCodeEnum) {
		if (!NullCheckUtils.isNullOrEmpty(deliveryAddress)) {
			validateDomesticAddressLine(deliveryAddress.getAddressLine(), errorCodeEnum);
			validateISOCountry(deliveryAddress.getCountry(), errorCodeEnum);
		}
	}

	public void validateExchangeRate(OBExchangeRate1 exchangeRateInformation) {
		if (OBExchangeRateType2Code.AGREED.equals(exchangeRateInformation.getRateType())) {
			if (NullCheckUtils.isNullOrEmpty(exchangeRateInformation.getContractIdentification())
					|| NullCheckUtils.isNullOrEmpty(exchangeRateInformation.getExchangeRate())) {
				throw OBPSD2Exception.populateOBException(OBErrorCodeEnum.UK_OBIE_FIELD_EXPECTED,
						ErrorMapKeys.EXPECTED_EXCHANGE_DETAILS);
			}
		}

		if (OBExchangeRateType2Code.ACTUAL.equals(exchangeRateInformation.getRateType())
				|| OBExchangeRateType2Code.INDICATIVE.equals(exchangeRateInformation.getRateType())) {
			if (!NullCheckUtils.isNullOrEmpty(exchangeRateInformation.getContractIdentification())
					|| !NullCheckUtils.isNullOrEmpty(exchangeRateInformation.getExchangeRate())) {
				throw OBPSD2Exception.populateOBException(OBErrorCodeEnum.UK_OBIE_FIELD_UNEXPECTED,
						ErrorMapKeys.UNEXPECTED_EXCHANGE_DETAILS);
			}
		}

	}

	public void validateFileType(String fileType, ErrorCodeEnum errorCode) {

		if (null != fileType && validFileTypeList.contains(fileType) == false) {
			if (errorCode == null)
				throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_FILETYPE_NOT_VALID);
			else
				throw PSD2Exception.populatePSD2Exception(errorCode);
		}
	}

	/* validate and compare */
	public void validateDateTime(String dateTime) {

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
		int dateCompare;
		try {
			dateCompare = validateAndParseDateTimeFormat(dateTime)
					.compareTo(sdf.parse(AispUtilities.getCurrentDateInISOFormat()));
		} catch (ParseException e) {
			throw OBPSD2Exception.populateOBException(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.TECHNICAL_ERROR);
		}

		if (dateCompare < 0) {
			throw OBPSD2Exception.populateOBException(OBErrorCodeEnum.UK_OBIE_FIELD_INVALIDDATE,
					ErrorMapKeys.INVALID_DATE);
		}
	}

	/* validate for request dates */
	public Date validateAndParseDateTimeFormat(String dateTimeString) {

		// Add Offset to Completion Date in Request if it is not already
		// present.
		if (!DateUtilitiesCMA2.Offsetcheck(dateTimeString)) {
			DateUtilitiesCMA2.addOffset(dateTimeString);
		}

		// Validate that Completion Date is not prior to current date
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
		try {
			return sdf.parse(dateTimeString);
		} catch (ParseException e) {
			throw OBPSD2Exception.populateOBException(OBErrorCodeEnum.UK_OBIE_FIELD_INVALIDDATE,
					ErrorMapKeys.INVALID_DATE);
		}
	}

	/* validate for response dates */ 
	public Date validateAndParseDateTimeFormatForResponse(String dateTimeString) {

		// Add Offset to Completion Date in Request if it is not already
		// present.
		if (!DateUtilitiesCMA2.Offsetcheck(dateTimeString)) {
			DateUtilitiesCMA2.addOffsetResponse(dateTimeString);
		}

		// Validate that Completion Date is not prior to current date
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
		try {
			return sdf.parse(dateTimeString);
		} catch (ParseException e) {
			throw OBPSD2Exception.populateOBException(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.INVALID_DATE_ERROR);
		}
	}

	public void validateInstructionPriority(OBWriteInternationalConsent1 request) {
		if (null == request.getData().getInitiation().getInstructionPriority()) {
			if (null != request.getData().getInitiation().getExchangeRateInformation()
					&& (null != request.getData().getInitiation().getExchangeRateInformation().getRateType())
					&& OBExchangeRateType2Code.AGREED
							.equals(request.getData().getInitiation().getExchangeRateInformation().getRateType())) {
				throw OBPSD2Exception.populateOBException(OBErrorCodeEnum.UK_OBIE_FIELD_UNEXPECTED,
						ErrorMapKeys.INSTRUCTION_PRIORITY_RATE_TYPE);
			}
		}
	}
	
	public void validateMerchantCategoryCode(String merchantCategoryCode, String merchantCategoryCodeRegexValidator) {
		if(!NullCheckUtils.isNullOrEmpty(merchantCategoryCode)) {
			if(!merchantCategoryCode.matches(merchantCategoryCodeRegexValidator)) {
				throw OBPSD2Exception.populateOBException(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID,
						ErrorMapKeys.MERCHANT_CATEGORY_CODE_INVALID);
			}
		}
	}
}