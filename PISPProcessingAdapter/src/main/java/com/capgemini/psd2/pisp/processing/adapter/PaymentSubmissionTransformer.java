package com.capgemini.psd2.pisp.processing.adapter;

import java.util.Map;

public interface PaymentSubmissionTransformer<T,U> {

	// Interfaces for Payments Consents Transformer v3.0 API
	public T paymentSubmissionResponseTransformer(T paymentSubmissionResponse,
			Map<String, Object> paymentsPlatformResourceMap, String methodType);

	// Interfaces for Payments Consents Transformer v3.0 API
	public U paymentSubmissionRequestTransformer(U paymentSubmissionRequest);

}
