package com.capgemini.psd2.foundationservice.account.product.boi.adapter.test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyObject;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountProductsResponse;
import com.capgemini.psd2.foundationservice.account.product.boi.adapter.raml.domain.Account;
import com.capgemini.psd2.foundationservice.account.product.boi.adapter.raml.domain.Product;
import com.capgemini.psd2.foundationservice.account.product.boi.adapter.transformer.AccountProductFoundationServiceTransformer;
import com.capgemini.psd2.validator.PSD2Validator;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountProductFoundationServiceTransformerTest {
	@InjectMocks
	private AccountProductFoundationServiceTransformer accountProductFoundationServiceTransformer;

	@Mock
	@Qualifier("PSD2ResponseValidator")
	PSD2Validator validator;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);

	}

	@Test
	public void contextLoads() {

	}

	@Test
	public void testTransformAccountProduct1() {

		Account acc = new Account();
		Product product = new Product();

		product.setProductCode("123");
		product.setProductName("test");
		product.setProductDescription("BCA");

		acc.setProduct(product);

		Map<String, String> params = new HashMap<String, String>();
		params.put("accountId", "12345678");

		Mockito.doNothing().when(validator).validate(anyObject());
		PlatformAccountProductsResponse response = accountProductFoundationServiceTransformer
				.transformAccountProducts(acc, params);
		assertNotNull(response);

	}

	@Test
	public void testTransformAccountProduct2() {

		Account acc = new Account();
		Product product = new Product();

		product.setProductCode("123");
		product.setProductName("test");
		product.setProductDescription("PCA");

		acc.setProduct(product);

		Map<String, String> params = new HashMap<String, String>();
		params.put("accountId", "12345678");

		Mockito.doNothing().when(validator).validate(anyObject());
		PlatformAccountProductsResponse response = accountProductFoundationServiceTransformer
				.transformAccountProducts(acc, params);
		assertNotNull(response);

	}

	@Test
	public void testTransformAccountProduct3() {

		Account acc = new Account();
		Product product = new Product();

		product.setProductCode(null);
		product.setProductName("test");
		product.setProductDescription("PCA");

		acc.setProduct(product);

		Map<String, String> params = new HashMap<String, String>();
		params.put("accountId", "12345678");

		Mockito.doNothing().when(validator).validate(anyObject());
		PlatformAccountProductsResponse response = accountProductFoundationServiceTransformer
				.transformAccountProducts(acc, params);
		assertNotNull(response);

	}

	@Test
	public void testTransformAccountProduct4() {

		Account acc = new Account();
		Product product = new Product();

		product.setProductCode("123");
		product.setProductName(null);
		product.setProductDescription("PCA");

		acc.setProduct(product);

		Map<String, String> params = new HashMap<String, String>();
		params.put("accountId", "12345678");

		Mockito.doNothing().when(validator).validate(anyObject());
		PlatformAccountProductsResponse response = accountProductFoundationServiceTransformer
				.transformAccountProducts(acc, params);
		assertNotNull(response);

	}

	@Test
	public void testTransformAccountProduct5() {

		Account acc = new Account();
		Product product = new Product();

		product.setProductCode(null);
		product.setProductName(null);
		product.setProductDescription("PCA");

		acc.setProduct(product);

		Map<String, String> params = new HashMap<String, String>();
		params.put("accountId", "12345678");

		Mockito.doNothing().when(validator).validate(anyObject());
		PlatformAccountProductsResponse response = accountProductFoundationServiceTransformer
				.transformAccountProducts(acc, params);
		assertNotNull(response);

	}
	@Test
	public void testTransformAccountProduct6() {

		Account acc = new Account();
		Product product =null;

		acc.setProduct(product);

		Map<String, String> params = new HashMap<String, String>();
		params.put("accountId", "12345678");

		Mockito.doNothing().when(validator).validate(anyObject());
		PlatformAccountProductsResponse response = accountProductFoundationServiceTransformer
				.transformAccountProducts(acc, params);
		assertNotNull(response);

	}
}
