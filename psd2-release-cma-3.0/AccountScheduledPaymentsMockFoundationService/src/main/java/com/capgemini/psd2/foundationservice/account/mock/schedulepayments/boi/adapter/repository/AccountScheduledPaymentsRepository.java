package com.capgemini.psd2.foundationservice.account.mock.schedulepayments.boi.adapter.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.foundationservice.account.mock.schedulepayments.boi.adapter.domain.PartiesOnceOffPaymentScheduleScheduleItemsresponse;



public interface AccountScheduledPaymentsRepository extends MongoRepository<PartiesOnceOffPaymentScheduleScheduleItemsresponse, String>{

	/**
	 * Find by account nsc and account number.
	 *
	 * @param accountId the account Id
	 * @return the PartiesAccountsAccountNumberOnceOffPaymentScheduleScheduleItemsResponse
	 */
	

	public PartiesOnceOffPaymentScheduleScheduleItemsresponse findByUserId(String userId);
}
