/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.delegate;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountStandingOrdersResponse;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.raml.domain.StandingOrderScheduleInstructionsresponse;
import com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.transformer.AccountStandingOrderFoundationServiceTransformer;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.utilities.NullCheckUtils;

/**
 * The Class AccountStandingOrderFoundationServiceDelegate.
 */
@EnableConfigurationProperties
@ConfigurationProperties("foundationService")
@Component
public class AccountStandingOrderFoundationServiceDelegate {

	/** The adapterUtility. */

	/** The account standingOrder FS transformer. */

	@Autowired
	AccountStandingOrderFoundationServiceTransformer accountStandingOrderFSTransformer;

	@Value("${foundationService.channelInReqHeader:#{x-api-channel-Code}}")
	private String channelInReqHeader;

	/* The platform. */
	@Value("${foundationService.platform:#{PSD2API}}")
	private String platform;

	/** The user in req header. */
	@Value("${foundationService.userInReqHeadeRaml:#{x-api-source-user}}")
	private String userInReqHeadeRaml;

	/** The platform in req header. */
	@Value("${foundationService.platformInReqHeaderRaml:#{x-api-source-system}}")
	private String platformInReqHeaderRaml;

	/** The correlation id in req header. */
	@Value("${foundationService.transactionReqHeaderRaml:#{x-api-transaction-id}}")
	private String transactionReqHeaderRaml;

	/** The correlation id in req header. */
	@Value("${foundationService.correlationReqHeaderRaml:#{x-api-correlation-id}}")
	private String correlationReqHeaderRaml;

	public String getFoundationServiceURL(String baseURL, String version, String nsc, String payeeAccountNumber,
			String endURL) {

		if (NullCheckUtils.isNullOrEmpty(nsc)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_NSC_IN_REQUEST);
		}
		if (NullCheckUtils.isNullOrEmpty(payeeAccountNumber)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}

		return baseURL + "/" + version + "/accounts/" + nsc + "/" + payeeAccountNumber + "/" + endURL;
	}

	public PlatformAccountStandingOrdersResponse transformResponseFromFDToAPI(
			StandingOrderScheduleInstructionsresponse standingOrderScheduleInstructionsresponse,
			Map<String, String> params) {
		return accountStandingOrderFSTransformer
				.transformAccountStandingOrders(standingOrderScheduleInstructionsresponse, params);
	}

	public HttpHeaders createRequestHeaders(RequestInfo requestInfo, AccountMapping accountMapping,
			Map<String, String> params) {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add(userInReqHeadeRaml, accountMapping.getPsuId());
		httpHeaders.add(channelInReqHeader, params.get(PSD2Constants.CHANNEL_NAME));
		String platformId = params.get(PSD2Constants.PLATFORM_IN_REQ_HEADER);
		httpHeaders.add(platformInReqHeaderRaml, NullCheckUtils.isNullOrEmpty(platformId) ? platform : platformId);
		httpHeaders.add(transactionReqHeaderRaml, accountMapping.getCorrelationId());
		httpHeaders.add(correlationReqHeaderRaml, "");
		return httpHeaders;
	}

}
