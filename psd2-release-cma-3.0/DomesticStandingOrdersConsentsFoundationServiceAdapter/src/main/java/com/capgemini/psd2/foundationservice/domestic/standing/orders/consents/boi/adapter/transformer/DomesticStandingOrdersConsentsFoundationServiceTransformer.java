package com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.transformer;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.datetime.utility.TimeZoneDateTimeAdapter;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain.Address;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain.AuthorisationType;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain.AuthorisingPartyAccount;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain.Country;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain.Currency;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain.FinancialEventAmount;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain.PaymentInstrumentRiskFactor;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain.PaymentTransaction;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain.ProposalStatus;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain.ProposingPartyAccount;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain.StandingOrderInstructionProposal;
import com.capgemini.psd2.pisp.domain.CustomDStandingOrderConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDStandingOrderConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBAuthorisation1;
import com.capgemini.psd2.pisp.domain.OBCashAccountCreditor2;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBCharge1;
import com.capgemini.psd2.pisp.domain.OBCharge1Amount;
import com.capgemini.psd2.pisp.domain.OBChargeBearerType1Code;
import com.capgemini.psd2.pisp.domain.OBDomesticStandingOrder1;
import com.capgemini.psd2.pisp.domain.OBDomesticStandingOrder1FinalPaymentAmount;
import com.capgemini.psd2.pisp.domain.OBDomesticStandingOrder1FirstPaymentAmount;
import com.capgemini.psd2.pisp.domain.OBDomesticStandingOrder1RecurringPaymentAmount;
import com.capgemini.psd2.pisp.domain.OBExternalAuthorisation1Code;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.domain.OBExternalPaymentContext1Code;
import com.capgemini.psd2.pisp.domain.OBExternalPermissions2Code;
import com.capgemini.psd2.pisp.domain.OBRisk1;
import com.capgemini.psd2.pisp.domain.OBRisk1DeliveryAddress;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticStandingOrderConsentResponse1;
import com.capgemini.psd2.pisp.enums.ProcessConsentStatusEnum;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.validator.PSD2Validator;

@Component
public class DomesticStandingOrdersConsentsFoundationServiceTransformer {
	
	@Autowired
	private PSD2Validator psd2Validator;
	@Autowired
	private TimeZoneDateTimeAdapter timeZoneDateTimeAdapter = new TimeZoneDateTimeAdapter();
	
	
	public StandingOrderInstructionProposal transformDomesticStandingOrdersResponseFromAPIToFDForInsert(
			CustomDStandingOrderConsentsPOSTRequest standingOrderConsentsRequest) {
		
		StandingOrderInstructionProposal standingOrderInstructionProposal = new StandingOrderInstructionProposal();
		
		if(!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest)){
			
			if(!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest.getData())){
				
				if(!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest.getData().getPermission())){
			
					standingOrderInstructionProposal.setPermission(standingOrderConsentsRequest.getData().getPermission().toString());
				}
				if(!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest.getData().getInitiation())){
					if(!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest.getData().getInitiation().getFrequency())){
			
						standingOrderInstructionProposal.setFrequency(standingOrderConsentsRequest.getData().getInitiation().getFrequency());
					}
		
					if(!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest.getData().getInitiation().getReference())){
			
						standingOrderInstructionProposal.setReference((standingOrderConsentsRequest.getData().getInitiation().getReference()));
					}
		
					if(!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest.getData().getInitiation().getNumberOfPayments())){
			
						standingOrderInstructionProposal.setNumberOfPayments(Double.parseDouble(standingOrderConsentsRequest.getData().getInitiation().getNumberOfPayments()));
					}

					if(!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest.getData().getInitiation().getFirstPaymentDateTime())){
			
						standingOrderInstructionProposal.setFirstPaymentDateTime(OffsetDateTime.parse(standingOrderConsentsRequest.getData().getInitiation().getFirstPaymentDateTime()));
					}
					if(!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest.getData().getInitiation().getRecurringPaymentDateTime())){
			
						standingOrderInstructionProposal.setRecurringPaymentDateTime(OffsetDateTime.parse(standingOrderConsentsRequest.getData().getInitiation().getRecurringPaymentDateTime()));
					}
					if(!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest.getData().getInitiation().getFinalPaymentDateTime())){
			
						standingOrderInstructionProposal.setFinalPaymentDateTime(OffsetDateTime.parse(standingOrderConsentsRequest.getData().getInitiation().getFinalPaymentDateTime()));
					}
					
					if(!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest.getData().getInitiation().getFirstPaymentAmount())){
						PaymentTransaction firstPaymentAmount = new PaymentTransaction();
						if(!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest.getData().getInitiation().getFirstPaymentAmount().getAmount())){
					
							FinancialEventAmount financialEventAmount = new FinancialEventAmount();
							financialEventAmount.setTransactionCurrency(Double.parseDouble(standingOrderConsentsRequest.getData().getInitiation().getFirstPaymentAmount().getAmount()));
							firstPaymentAmount.setFinancialEventAmount(financialEventAmount);
						}
						
						if(!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest.getData().getInitiation().getFirstPaymentAmount().getCurrency())){
							Currency transactionCurrency = new Currency();
							transactionCurrency.setIsoAlphaCode(standingOrderConsentsRequest.getData().getInitiation().getFirstPaymentAmount().getCurrency());
							firstPaymentAmount.setTransactionCurrency(transactionCurrency);
						}
					standingOrderInstructionProposal.setFirstPaymentAmount(firstPaymentAmount);
					}
					
					if(!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest.getData().getInitiation().getRecurringPaymentAmount())){
						PaymentTransaction recurringPaymentAmount = new PaymentTransaction();
						if(!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest.getData().getInitiation().getRecurringPaymentAmount().getAmount())){
							FinancialEventAmount recFinancialEventAmount = new FinancialEventAmount();
							recFinancialEventAmount.setTransactionCurrency(Double.parseDouble(standingOrderConsentsRequest.getData().getInitiation().getRecurringPaymentAmount().getAmount()));
							recurringPaymentAmount.setFinancialEventAmount(recFinancialEventAmount );
						}
						if(!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest.getData().getInitiation().getRecurringPaymentAmount().getCurrency())){
							Currency recTransactionCurrency = new Currency();
							recTransactionCurrency.setIsoAlphaCode(standingOrderConsentsRequest.getData().getInitiation().getRecurringPaymentAmount().getCurrency());
							recurringPaymentAmount.setTransactionCurrency(recTransactionCurrency );
						}
					standingOrderInstructionProposal.setRecurringPaymentAmount(recurringPaymentAmount );
					}
					
					if(!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest.getData().getInitiation().getFinalPaymentAmount())){
						PaymentTransaction finalPaymentAmount = new PaymentTransaction();
						if(!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest.getData().getInitiation().getFinalPaymentAmount().getAmount())){
							FinancialEventAmount finalFinancialEventAmount = new FinancialEventAmount();
							finalFinancialEventAmount.setTransactionCurrency(Double.parseDouble(standingOrderConsentsRequest.getData().getInitiation().getFinalPaymentAmount().getAmount()));
							finalPaymentAmount.setFinancialEventAmount(finalFinancialEventAmount );
						}
						if(!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest.getData().getInitiation().getFinalPaymentAmount().getCurrency())){
							Currency finalTransactionCurrency = new Currency();
							finalTransactionCurrency.setIsoAlphaCode(standingOrderConsentsRequest.getData().getInitiation().getFinalPaymentAmount().getCurrency());
							finalPaymentAmount.setTransactionCurrency(finalTransactionCurrency );
						}
					standingOrderInstructionProposal.setFinalPaymentAmount(finalPaymentAmount );	
					}
					
					if(!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest.getData().getInitiation().getDebtorAccount())){
						AuthorisingPartyAccount authorisingPartyAccount = new AuthorisingPartyAccount();
						if(!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest.getData().getInitiation().getDebtorAccount().getSchemeName())){
							authorisingPartyAccount.setSchemeName(standingOrderConsentsRequest.getData().getInitiation().getDebtorAccount().getSchemeName());
						}
						if(!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest.getData().getInitiation().getDebtorAccount().getIdentification())){
							authorisingPartyAccount.setAccountNumber(standingOrderConsentsRequest.getData().getInitiation().getDebtorAccount().getIdentification());
						}
						if(!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest.getData().getInitiation().getDebtorAccount().getName())){
							authorisingPartyAccount.setAccountName(standingOrderConsentsRequest.getData().getInitiation().getDebtorAccount().getName());
						}
						if(!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest.getData().getInitiation().getDebtorAccount().getSecondaryIdentification())){
							authorisingPartyAccount.setSecondaryIdentification(standingOrderConsentsRequest.getData().getInitiation().getDebtorAccount().getSecondaryIdentification());
						}
						standingOrderInstructionProposal.setAuthorisingPartyAccount(authorisingPartyAccount );			
						}
					
					if(!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest.getData().getInitiation().getCreditorAccount())){
						ProposingPartyAccount proposingPartyAccount = new ProposingPartyAccount();
						if(!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest.getData().getInitiation().getCreditorAccount().getName())){
							proposingPartyAccount.setAccountName(standingOrderConsentsRequest.getData().getInitiation().getCreditorAccount().getName());
						}
						if(!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest.getData().getInitiation().getCreditorAccount().getIdentification())){
							proposingPartyAccount.setAccountNumber(standingOrderConsentsRequest.getData().getInitiation().getCreditorAccount().getIdentification());
						}
						if(!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest.getData().getInitiation().getCreditorAccount().getSchemeName())){
							proposingPartyAccount.setSchemeName(standingOrderConsentsRequest.getData().getInitiation().getCreditorAccount().getSchemeName());
						}
						if(!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest.getData().getInitiation().getCreditorAccount().getSecondaryIdentification())){
							proposingPartyAccount.setSecondaryIdentification(standingOrderConsentsRequest.getData().getInitiation().getCreditorAccount().getSecondaryIdentification());
						}
					standingOrderInstructionProposal.setProposingPartyAccount(proposingPartyAccount );	
					}
				}
				if(!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest.getData().getAuthorisation())){
					if(!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest.getData().getAuthorisation().getAuthorisationType())){
						standingOrderInstructionProposal.setAuthorisationType(AuthorisationType.fromValue(standingOrderConsentsRequest.getData().getAuthorisation().getAuthorisationType().toString()));	
					}
					if(!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest.getData().getAuthorisation().getCompletionDateTime())){
						standingOrderInstructionProposal.setAuthorisationDatetime(OffsetDateTime.parse(standingOrderConsentsRequest.getData().getAuthorisation().getCompletionDateTime()));
					}
				}
			}
			
			if(!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest.getRisk())){
			PaymentInstrumentRiskFactor paymentInstructionRiskFactorReference = new PaymentInstrumentRiskFactor();
			if(!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest.getRisk().getPaymentContextCode().toString())){
			paymentInstructionRiskFactorReference.setPaymentContextCode(standingOrderConsentsRequest.getRisk().getPaymentContextCode().toString());
			}
			if(!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest.getRisk().getMerchantCategoryCode())){
			paymentInstructionRiskFactorReference.setMerchantCategoryCode(standingOrderConsentsRequest.getRisk().getMerchantCategoryCode());
			}
			if(!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest.getRisk().getMerchantCustomerIdentification())){
			paymentInstructionRiskFactorReference.setMerchantCustomerIdentification(standingOrderConsentsRequest.getRisk().getMerchantCustomerIdentification());
			}			
			if(!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest.getRisk().getDeliveryAddress())){
				Address counterPartyAddress = new Address();
				if(!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest.getRisk().getDeliveryAddress().getAddressLine())){
					counterPartyAddress.setFirstAddressLine(standingOrderConsentsRequest.getRisk().getDeliveryAddress().getAddressLine().get(0));
				
					if(!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest.getRisk().getDeliveryAddress().getAddressLine().get(1))){
						counterPartyAddress.setSecondAddressLine(standingOrderConsentsRequest.getRisk().getDeliveryAddress().getAddressLine().get(1));
					}
				}
				if(!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest.getRisk().getDeliveryAddress().getStreetName())){
					counterPartyAddress.setGeoCodeBuildingName(standingOrderConsentsRequest.getRisk().getDeliveryAddress().getStreetName());
				}
				if(!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest.getRisk().getDeliveryAddress().getBuildingNumber())){
					counterPartyAddress.setGeoCodeBuildingNumber(standingOrderConsentsRequest.getRisk().getDeliveryAddress().getBuildingNumber());
				}
				if(!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest.getRisk().getDeliveryAddress().getPostCode())){
					counterPartyAddress.setPostCodeNumber(standingOrderConsentsRequest.getRisk().getDeliveryAddress().getPostCode());
				}
				if(!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest.getRisk().getDeliveryAddress().getTownName())){
					counterPartyAddress.setThirdAddressLine(standingOrderConsentsRequest.getRisk().getDeliveryAddress().getTownName());
				}
				if(!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest.getRisk().getDeliveryAddress().getCountrySubDivision())){
					counterPartyAddress.setFourthAddressLine(standingOrderConsentsRequest.getRisk().getDeliveryAddress().getCountrySubDivision());
					counterPartyAddress.setFifthAddressLine(standingOrderConsentsRequest.getRisk().getDeliveryAddress().getCountrySubDivision());
				}
				if(!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest.getRisk().getDeliveryAddress().getCountry())){
					Country addressCountry = new Country();
					addressCountry.setIsoCountryAlphaTwoCode(standingOrderConsentsRequest.getRisk().getDeliveryAddress().getCountry());
					counterPartyAddress.setAddressCountry(addressCountry );
				}
				paymentInstructionRiskFactorReference.setCounterPartyAddress(counterPartyAddress );
			}
			standingOrderInstructionProposal.setPaymentInstructionRiskFactorReference(paymentInstructionRiskFactorReference );
			}
		}
		
		return standingOrderInstructionProposal;
		
	}
	
	public <T> CustomDStandingOrderConsentsPOSTResponse transformDomesticStandingOrdersResponseFromFDToAPIForInsert(
			T standingOrderInstructionProposalResponse) {
		CustomDStandingOrderConsentsPOSTResponse customDStandingOrderConsentsPOSTResponse =  new CustomDStandingOrderConsentsPOSTResponse();
		StandingOrderInstructionProposal standingOrderInstructionProposal = (StandingOrderInstructionProposal) standingOrderInstructionProposalResponse;
		OBWriteDataDomesticStandingOrderConsentResponse1 obWriteStandingOrderConsentRes = new OBWriteDataDomesticStandingOrderConsentResponse1();
		
		if ((!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getPaymentInstructionProposalId())) && (!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getProposalStatus()))){
			
			if ((standingOrderInstructionProposal.getProposalStatus().toString()).equalsIgnoreCase(ProposalStatus.AWAITINGAUTHORISATION.toString())) {
				customDStandingOrderConsentsPOSTResponse.setConsentProcessStatus(ProcessConsentStatusEnum.PASS);
			}
		}
		if ((!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getPaymentInstructionProposalId())) && (!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getProposalStatus()))){
			
			if ((standingOrderInstructionProposal.getProposalStatus().toString()).equalsIgnoreCase(ProposalStatus.REJECTED.toString())) {
				customDStandingOrderConsentsPOSTResponse.setConsentProcessStatus(ProcessConsentStatusEnum.FAIL);
			}
		}
		
		if (!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal)) {
			if (!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getPaymentInstructionProposalId())) {
		         obWriteStandingOrderConsentRes.setConsentId(standingOrderInstructionProposal.getPaymentInstructionProposalId().toString());
		         }
			if (!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getProposalCreationDatetime())) {
				obWriteStandingOrderConsentRes.setCreationDateTime(timeZoneDateTimeAdapter.parseOffsetDateTimeToCMA(standingOrderInstructionProposal.getProposalCreationDatetime()));
		         }
			if (!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getProposalStatus())) {
				obWriteStandingOrderConsentRes.setStatus(OBExternalConsentStatus1Code.fromValue(standingOrderInstructionProposal.getProposalStatus().toString()));
		         }
			if (!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getProposalStatusUpdateDatetime())) {
				obWriteStandingOrderConsentRes.setStatusUpdateDateTime(timeZoneDateTimeAdapter.parseOffsetDateTimeToCMA(standingOrderInstructionProposal.getProposalStatusUpdateDatetime()));
		         }
			if (!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getPermission())) {
				obWriteStandingOrderConsentRes.setPermission(OBExternalPermissions2Code.fromValue(standingOrderInstructionProposal.getPermission().toString()));
		         }
		
	if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getCharges())){
			
			List<OBCharge1> charges = new ArrayList<OBCharge1>();
			OBCharge1 charge = new OBCharge1();
			if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getCharges().get(0).getChargeBearer().toString())){
				String chargeBearer1 = standingOrderInstructionProposal.getCharges().get(0).getChargeBearer().toString();
				OBChargeBearerType1Code oBChargeBearerType1Code = OBChargeBearerType1Code.fromValue(chargeBearer1);
				charge.setChargeBearer(oBChargeBearerType1Code);
			}
			
			if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getCharges().get(0).getType())){
				
				charge.setType(standingOrderInstructionProposal.getCharges().get(0).getType());
			}
			OBCharge1Amount amount = new OBCharge1Amount();
			if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getCharges().get(0).getAmount())){				
				if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getCharges().get(0).getAmount().getTransactionCurrency())) {
				Double chargeAmount = standingOrderInstructionProposal.getCharges().get(0).getAmount().getTransactionCurrency();
				amount.setAmount(String.valueOf(chargeAmount));
				}
			}
				if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getCharges().get(0).getCurrency())){
					if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getCharges().get(0).getCurrency().getIsoAlphaCode())) {
					
					amount.setCurrency(standingOrderInstructionProposal.getCharges().get(0).getCurrency().getIsoAlphaCode());
					}
				}	
			charge.setAmount(amount);	
			charges.add(charge);
			obWriteStandingOrderConsentRes.setCharges(charges);
		}
				
		    OBDomesticStandingOrder1 obDomesticStandingOrder1 = new OBDomesticStandingOrder1();
		 			 
			if (!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getFrequency())) {
				obDomesticStandingOrder1.setFrequency(standingOrderInstructionProposal.getFrequency());
		         }
			if (!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getReference())) {
				obDomesticStandingOrder1.setReference(standingOrderInstructionProposal.getReference());
			     }
			if (!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getNumberOfPayments())) {
				obDomesticStandingOrder1.setNumberOfPayments(standingOrderInstructionProposal.getNumberOfPayments().toString());
			     }
			if (!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getFirstPaymentDateTime())) {
				obDomesticStandingOrder1.setFirstPaymentDateTime(timeZoneDateTimeAdapter.parseOffsetDateTimeToCMA(standingOrderInstructionProposal.getFirstPaymentDateTime()));
			     }
			if (!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getRecurringPaymentDateTime())) {
				obDomesticStandingOrder1.setRecurringPaymentDateTime(timeZoneDateTimeAdapter.parseOffsetDateTimeToCMA(standingOrderInstructionProposal.getRecurringPaymentDateTime()));
			     }
			if (!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getFinalPaymentDateTime())) {
				obDomesticStandingOrder1.setFinalPaymentDateTime(timeZoneDateTimeAdapter.parseOffsetDateTimeToCMA(standingOrderInstructionProposal.getFinalPaymentDateTime()));
			     }
		  
		
		  
		if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getFirstPaymentAmount())){
			OBDomesticStandingOrder1FirstPaymentAmount firstPaymentAmount = new OBDomesticStandingOrder1FirstPaymentAmount();
			
			if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getFirstPaymentAmount().getFinancialEventAmount())) {
				if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getFirstPaymentAmount().getFinancialEventAmount().getTransactionCurrency())){
					firstPaymentAmount.setAmount(standingOrderInstructionProposal.getFirstPaymentAmount().getFinancialEventAmount().getTransactionCurrency().toString());
				}
			}
			if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getFirstPaymentAmount().getTransactionCurrency())){
				if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getFirstPaymentAmount().getTransactionCurrency().getIsoAlphaCode())) {
				   firstPaymentAmount.setCurrency(standingOrderInstructionProposal.getFirstPaymentAmount().getTransactionCurrency().getIsoAlphaCode());
			    }
			}
			obDomesticStandingOrder1.setFirstPaymentAmount(firstPaymentAmount);
		}
		
	
		if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getRecurringPaymentAmount())){
			OBDomesticStandingOrder1RecurringPaymentAmount recurringPaymentAmount = new OBDomesticStandingOrder1RecurringPaymentAmount();
			
			if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getRecurringPaymentAmount().getFinancialEventAmount())){
				if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getRecurringPaymentAmount().getFinancialEventAmount().getTransactionCurrency().toString())) {
				recurringPaymentAmount.setAmount(standingOrderInstructionProposal.getRecurringPaymentAmount().getFinancialEventAmount().getTransactionCurrency().toString());
				}
			}
			if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getRecurringPaymentAmount().getTransactionCurrency())){
				if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getRecurringPaymentAmount().getTransactionCurrency().getIsoAlphaCode())) {
				recurringPaymentAmount.setCurrency(standingOrderInstructionProposal.getRecurringPaymentAmount().getTransactionCurrency().getIsoAlphaCode());
				}
			}
			obDomesticStandingOrder1.setRecurringPaymentAmount(recurringPaymentAmount);
		}
		
		
		if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getFinalPaymentAmount())){
			OBDomesticStandingOrder1FinalPaymentAmount finalPaymentAmount = new OBDomesticStandingOrder1FinalPaymentAmount();
			if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getFinalPaymentAmount().getFinancialEventAmount())){
				if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getFinalPaymentAmount().getFinancialEventAmount().getTransactionCurrency())) {
				finalPaymentAmount.setAmount(standingOrderInstructionProposal.getFinalPaymentAmount().getFinancialEventAmount().getTransactionCurrency().toString());
			  }
			}
			if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getFinalPaymentAmount().getTransactionCurrency())){
				if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getFinalPaymentAmount().getTransactionCurrency().getIsoAlphaCode())) {
				finalPaymentAmount.setCurrency(standingOrderInstructionProposal.getFinalPaymentAmount().getTransactionCurrency().getIsoAlphaCode());
			  }
			}	
			obDomesticStandingOrder1.setFinalPaymentAmount(finalPaymentAmount);
		}
		
		
		if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getAuthorisingPartyAccount())){
			OBCashAccountDebtor3 accountDebtor = new OBCashAccountDebtor3();
			
				if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getAuthorisingPartyAccount().getSchemeName())) {
				accountDebtor.setSchemeName(standingOrderInstructionProposal.getAuthorisingPartyAccount().getSchemeName());
				}
			   if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getAuthorisingPartyAccount().getAccountNumber())) {
				accountDebtor.setIdentification(standingOrderInstructionProposal.getAuthorisingPartyAccount().getAccountNumber());
				}
			   if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getAuthorisingPartyAccount().getAccountName())) {
				accountDebtor.setName(standingOrderInstructionProposal.getAuthorisingPartyAccount().getAccountName());
				}
			
			   if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getAuthorisingPartyAccount().getSecondaryIdentification())){
				accountDebtor.setSecondaryIdentification(standingOrderInstructionProposal.getAuthorisingPartyAccount().getSecondaryIdentification());
			 }
			  obDomesticStandingOrder1.setDebtorAccount(accountDebtor);
		 }
		
		
		if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getProposingPartyAccount())){
			OBCashAccountCreditor2 accountCreditor = new OBCashAccountCreditor2();
			if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getProposingPartyAccount().getSchemeName())){
				accountCreditor.setSchemeName(standingOrderInstructionProposal.getProposingPartyAccount().getSchemeName());
			}
			if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getProposingPartyAccount().getAccountNumber())){
				accountCreditor.setIdentification(standingOrderInstructionProposal.getProposingPartyAccount().getAccountNumber());
			}
			if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getProposingPartyAccount().getAccountName())){
				accountCreditor.setName(standingOrderInstructionProposal.getProposingPartyAccount().getAccountName());
			}
			if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getProposingPartyAccount().getSecondaryIdentification())){
				accountCreditor.setSecondaryIdentification(standingOrderInstructionProposal.getProposingPartyAccount().getSecondaryIdentification());
			}
			obDomesticStandingOrder1.setCreditorAccount(accountCreditor);
		}
		
		      OBAuthorisation1 authorisation = null;
		      if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getAuthorisationType())){
		    	  authorisation = new OBAuthorisation1();
		    	  authorisation.setAuthorisationType(OBExternalAuthorisation1Code.fromValue(standingOrderInstructionProposal.getAuthorisationType().toString()));
				
				if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getAuthorisationDatetime())){
					authorisation.setCompletionDateTime(timeZoneDateTimeAdapter.parseOffsetDateTimeToCMA(standingOrderInstructionProposal.getAuthorisationDatetime()));
				}
				obWriteStandingOrderConsentRes.setAuthorisation(authorisation);
		      }

		    OBRisk1 risk = new OBRisk1();
		     if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference())){
			   
				if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference().getPaymentContextCode())){
					risk.setPaymentContextCode(OBExternalPaymentContext1Code.fromValue(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference().getPaymentContextCode()));
				}
				if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference().getMerchantCategoryCode())){
					risk.setMerchantCategoryCode(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference().getMerchantCategoryCode());
				}
				if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference().getMerchantCustomerIdentification())){
					risk.setMerchantCustomerIdentification(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference().getMerchantCustomerIdentification());
				}
			

		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
			   
			if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference().getCounterPartyAddress())){
				if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference().getCounterPartyAddress().getFirstAddressLine())) {
				List<String> addressList = new ArrayList<String>();
				if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference().getCounterPartyAddress().getSecondAddressLine())) {
					addressList.add(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference().getCounterPartyAddress().getFirstAddressLine());
					addressList.add(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference().getCounterPartyAddress().getSecondAddressLine());
				}
				else {
					addressList.add(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference().getCounterPartyAddress().getFirstAddressLine());
				}
				deliveryAddress.setAddressLine(addressList);
			}						
				if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference().getCounterPartyAddress().getGeoCodeBuildingName())) {
				deliveryAddress.setStreetName(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference().getCounterPartyAddress().getGeoCodeBuildingName());
					
			}			
			if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference().getCounterPartyAddress().getGeoCodeBuildingNumber())){
				deliveryAddress.setBuildingNumber(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference().getCounterPartyAddress().getGeoCodeBuildingNumber());
			}
			if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference().getCounterPartyAddress().getPostCodeNumber())){
				deliveryAddress.setPostCode(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference().getCounterPartyAddress().getPostCodeNumber());
			}
			if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference().getCounterPartyAddress().getThirdAddressLine())){
				deliveryAddress.setTownName(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference().getCounterPartyAddress().getThirdAddressLine());
			}
			if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference().getCounterPartyAddress().getFourthAddressLine())){
				String addressLine = null;
				if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference().getCounterPartyAddress().getFifthAddressLine())){
					addressLine = standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference().getCounterPartyAddress().getFourthAddressLine() + standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference().getCounterPartyAddress().getFifthAddressLine();
				}else {
					addressLine = standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference().getCounterPartyAddress().getFourthAddressLine();
				}
				deliveryAddress.setCountrySubDivision(addressLine);
			}
			if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference().getCounterPartyAddress().getAddressCountry().getIsoCountryAlphaTwoCode())){
				deliveryAddress.setCountry(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference().getCounterPartyAddress().getAddressCountry().getIsoCountryAlphaTwoCode());
			}
			risk.setDeliveryAddress(deliveryAddress);
				}
		   }
		 
		obWriteStandingOrderConsentRes.setInitiation(obDomesticStandingOrder1);	
		customDStandingOrderConsentsPOSTResponse.setData(obWriteStandingOrderConsentRes);
		customDStandingOrderConsentsPOSTResponse.setRisk(risk);
		}
		return customDStandingOrderConsentsPOSTResponse;
		
	}
	
	public <T> CustomDStandingOrderConsentsPOSTResponse transformDomesticStandingOrderResponse(T standingOrderInstructionProposalResponse) 
	{
		/* Root Element */
		CustomDStandingOrderConsentsPOSTResponse customDStandingOrderConsentsPOSTResponse =  new CustomDStandingOrderConsentsPOSTResponse();
		StandingOrderInstructionProposal standingOrderInstructionProposal = (StandingOrderInstructionProposal) standingOrderInstructionProposalResponse;
		OBWriteDataDomesticStandingOrderConsentResponse1 obWriteStandingOrderConsentRes = new OBWriteDataDomesticStandingOrderConsentResponse1();
		
		
		if (!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal)) {
			if (!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getPaymentInstructionProposalId())) {
		         obWriteStandingOrderConsentRes.setConsentId(standingOrderInstructionProposal.getPaymentInstructionProposalId().toString());
		         }
			if (!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getProposalCreationDatetime())) {
				obWriteStandingOrderConsentRes.setCreationDateTime(timeZoneDateTimeAdapter.parseOffsetDateTimeToCMA(standingOrderInstructionProposal.getProposalCreationDatetime()));
		         }
			if (!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getProposalStatus())) {
				obWriteStandingOrderConsentRes.setStatus(OBExternalConsentStatus1Code.fromValue(standingOrderInstructionProposal.getProposalStatus().toString()));
		         }
			if (!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getProposalStatusUpdateDatetime())) {
				obWriteStandingOrderConsentRes.setStatusUpdateDateTime(timeZoneDateTimeAdapter.parseOffsetDateTimeToCMA(standingOrderInstructionProposal.getProposalStatusUpdateDatetime()));
		         }
			if (!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getPermission())) {
				obWriteStandingOrderConsentRes.setPermission(OBExternalPermissions2Code.fromValue(standingOrderInstructionProposal.getPermission().toString()));
		         }
		
	if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getCharges())){
			
			List<OBCharge1> charges = new ArrayList<OBCharge1>();
			OBCharge1 charge = new OBCharge1();
			if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getCharges().get(0).getChargeBearer().toString())){
				String chargeBearer1 = standingOrderInstructionProposal.getCharges().get(0).getChargeBearer().toString();
				OBChargeBearerType1Code oBChargeBearerType1Code = OBChargeBearerType1Code.fromValue(chargeBearer1);
				charge.setChargeBearer(oBChargeBearerType1Code);
			}
			
			if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getCharges().get(0).getType())){
				
				charge.setType(standingOrderInstructionProposal.getCharges().get(0).getType());
			}
			OBCharge1Amount amount = new OBCharge1Amount();
			if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getCharges().get(0).getAmount())){				
				if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getCharges().get(0).getAmount().getTransactionCurrency())) {
				Double chargeAmount = standingOrderInstructionProposal.getCharges().get(0).getAmount().getTransactionCurrency();
				amount.setAmount(String.valueOf(chargeAmount));
				}
			}
				if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getCharges().get(0).getCurrency())){
					if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getCharges().get(0).getCurrency().getIsoAlphaCode())) {
					
					amount.setCurrency(standingOrderInstructionProposal.getCharges().get(0).getCurrency().getIsoAlphaCode());
					}
				}	
			charge.setAmount(amount);	
			charges.add(charge);
			obWriteStandingOrderConsentRes.setCharges(charges);
		}
				
		    OBDomesticStandingOrder1 obDomesticStandingOrder1 = new OBDomesticStandingOrder1();
		 			 
			if (!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getFrequency())) {
				obDomesticStandingOrder1.setFrequency(standingOrderInstructionProposal.getFrequency());
		         }
			if (!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getReference())) {
				obDomesticStandingOrder1.setReference(standingOrderInstructionProposal.getReference());
			     }
			if (!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getNumberOfPayments())) {
				obDomesticStandingOrder1.setNumberOfPayments(standingOrderInstructionProposal.getNumberOfPayments().toString());
			     }
			if (!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getFirstPaymentDateTime())) {
				obDomesticStandingOrder1.setFirstPaymentDateTime(timeZoneDateTimeAdapter.parseOffsetDateTimeToCMA(standingOrderInstructionProposal.getFirstPaymentDateTime()));
			     }
			if (!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getRecurringPaymentDateTime())) {
				obDomesticStandingOrder1.setRecurringPaymentDateTime(timeZoneDateTimeAdapter.parseOffsetDateTimeToCMA(standingOrderInstructionProposal.getRecurringPaymentDateTime()));
			     }
			if (!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getFinalPaymentDateTime())) {
				obDomesticStandingOrder1.setFinalPaymentDateTime(timeZoneDateTimeAdapter.parseOffsetDateTimeToCMA(standingOrderInstructionProposal.getFinalPaymentDateTime()));
			     }
		  
		
		  
		if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getFirstPaymentAmount())){
			OBDomesticStandingOrder1FirstPaymentAmount firstPaymentAmount = new OBDomesticStandingOrder1FirstPaymentAmount();
			
			if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getFirstPaymentAmount().getFinancialEventAmount())) {
				if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getFirstPaymentAmount().getFinancialEventAmount().getTransactionCurrency())){
					firstPaymentAmount.setAmount(standingOrderInstructionProposal.getFirstPaymentAmount().getFinancialEventAmount().getTransactionCurrency().toString());
				}
			}
			if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getFirstPaymentAmount().getTransactionCurrency())){
				if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getFirstPaymentAmount().getTransactionCurrency().getIsoAlphaCode())) {
				   firstPaymentAmount.setCurrency(standingOrderInstructionProposal.getFirstPaymentAmount().getTransactionCurrency().getIsoAlphaCode());
			    }
			}
			obDomesticStandingOrder1.setFirstPaymentAmount(firstPaymentAmount);
		}
		
	
		if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getRecurringPaymentAmount())){
			OBDomesticStandingOrder1RecurringPaymentAmount recurringPaymentAmount = new OBDomesticStandingOrder1RecurringPaymentAmount();
			
			if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getRecurringPaymentAmount().getFinancialEventAmount())){
				if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getRecurringPaymentAmount().getFinancialEventAmount().getTransactionCurrency().toString())) {
				recurringPaymentAmount.setAmount(standingOrderInstructionProposal.getRecurringPaymentAmount().getFinancialEventAmount().getTransactionCurrency().toString());
				}
			}
			if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getRecurringPaymentAmount().getTransactionCurrency())){
				if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getRecurringPaymentAmount().getTransactionCurrency().getIsoAlphaCode())) {
				recurringPaymentAmount.setCurrency(standingOrderInstructionProposal.getRecurringPaymentAmount().getTransactionCurrency().getIsoAlphaCode());
				}
			}
			obDomesticStandingOrder1.setRecurringPaymentAmount(recurringPaymentAmount);
		}
		
		
		if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getFinalPaymentAmount())){
			OBDomesticStandingOrder1FinalPaymentAmount finalPaymentAmount = new OBDomesticStandingOrder1FinalPaymentAmount();
			if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getFinalPaymentAmount().getFinancialEventAmount())){
				if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getFinalPaymentAmount().getFinancialEventAmount().getTransactionCurrency())) {
				finalPaymentAmount.setAmount(standingOrderInstructionProposal.getFinalPaymentAmount().getFinancialEventAmount().getTransactionCurrency().toString());
			  }
			}
			if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getFinalPaymentAmount().getTransactionCurrency())){
				if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getFinalPaymentAmount().getTransactionCurrency().getIsoAlphaCode())) {
				finalPaymentAmount.setCurrency(standingOrderInstructionProposal.getFinalPaymentAmount().getTransactionCurrency().getIsoAlphaCode());
			  }
			}	
			obDomesticStandingOrder1.setFinalPaymentAmount(finalPaymentAmount);
		}
		
		
		if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getAuthorisingPartyAccount())){
			OBCashAccountDebtor3 accountDebtor = new OBCashAccountDebtor3();
			
				if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getAuthorisingPartyAccount().getSchemeName())) {
				accountDebtor.setSchemeName(standingOrderInstructionProposal.getAuthorisingPartyAccount().getSchemeName());
				}
			   if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getAuthorisingPartyAccount().getAccountNumber())) {
				accountDebtor.setIdentification(standingOrderInstructionProposal.getAuthorisingPartyAccount().getAccountNumber());
				}
			   if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getAuthorisingPartyAccount().getAccountName())) {
				accountDebtor.setName(standingOrderInstructionProposal.getAuthorisingPartyAccount().getAccountName());
				}
			
			   if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getAuthorisingPartyAccount().getSecondaryIdentification())){
				accountDebtor.setSecondaryIdentification(standingOrderInstructionProposal.getAuthorisingPartyAccount().getSecondaryIdentification());
			 }
			  obDomesticStandingOrder1.setDebtorAccount(accountDebtor);
		 }
		
		
		if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getProposingPartyAccount())){
			OBCashAccountCreditor2 accountCreditor = new OBCashAccountCreditor2();
			if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getProposingPartyAccount().getSchemeName())){
				accountCreditor.setSchemeName(standingOrderInstructionProposal.getProposingPartyAccount().getSchemeName());
			}
			if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getProposingPartyAccount().getAccountNumber())){
				accountCreditor.setIdentification(standingOrderInstructionProposal.getProposingPartyAccount().getAccountNumber());
			}
			if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getProposingPartyAccount().getAccountName())){
				accountCreditor.setName(standingOrderInstructionProposal.getProposingPartyAccount().getAccountName());
			}
			if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getProposingPartyAccount().getSecondaryIdentification())){
				accountCreditor.setSecondaryIdentification(standingOrderInstructionProposal.getProposingPartyAccount().getSecondaryIdentification());
			}
			obDomesticStandingOrder1.setCreditorAccount(accountCreditor);
		}
		
		      OBAuthorisation1 authorisation = null;
		      if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getAuthorisationType())){
		    	  authorisation = new OBAuthorisation1();
		    	  authorisation.setAuthorisationType(OBExternalAuthorisation1Code.fromValue(standingOrderInstructionProposal.getAuthorisationType().toString()));
				if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getAuthorisationDatetime())){
					authorisation.setCompletionDateTime(timeZoneDateTimeAdapter.parseOffsetDateTimeToCMA(standingOrderInstructionProposal.getAuthorisationDatetime()));
				}
				obWriteStandingOrderConsentRes.setAuthorisation(authorisation);
		      }

		    OBRisk1 risk = new OBRisk1();
		     if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference())){
			   
				if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference().getPaymentContextCode())){
					risk.setPaymentContextCode(OBExternalPaymentContext1Code.fromValue(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference().getPaymentContextCode()));
				}
				if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference().getMerchantCategoryCode())){
					risk.setMerchantCategoryCode(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference().getMerchantCategoryCode());
				}
				if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference().getMerchantCustomerIdentification())){
					risk.setMerchantCustomerIdentification(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference().getMerchantCustomerIdentification());
				}
			

		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
			   
			if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference().getCounterPartyAddress())){
				if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference().getCounterPartyAddress().getFirstAddressLine())) {
				List<String> addressList = new ArrayList<String>();
				if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference().getCounterPartyAddress().getSecondAddressLine())) {
					addressList.add(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference().getCounterPartyAddress().getFirstAddressLine());
					addressList.add(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference().getCounterPartyAddress().getSecondAddressLine());
				}
				else {
					addressList.add(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference().getCounterPartyAddress().getFirstAddressLine());
				}
				deliveryAddress.setAddressLine(addressList);
			}						
				if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference().getCounterPartyAddress().getGeoCodeBuildingName())) {
				deliveryAddress.setStreetName(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference().getCounterPartyAddress().getGeoCodeBuildingName());
					
			}			
			if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference().getCounterPartyAddress().getGeoCodeBuildingNumber())){
				deliveryAddress.setBuildingNumber(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference().getCounterPartyAddress().getGeoCodeBuildingNumber());
			}
			if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference().getCounterPartyAddress().getPostCodeNumber())){
				deliveryAddress.setPostCode(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference().getCounterPartyAddress().getPostCodeNumber());
			}
			if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference().getCounterPartyAddress().getThirdAddressLine())){
				deliveryAddress.setTownName(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference().getCounterPartyAddress().getThirdAddressLine());
			}
			if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference().getCounterPartyAddress().getFourthAddressLine())){
				String addressLine = null;
				if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference().getCounterPartyAddress().getFifthAddressLine())){
					addressLine = standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference().getCounterPartyAddress().getFourthAddressLine() + standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference().getCounterPartyAddress().getFifthAddressLine();
				}else {
					addressLine = standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference().getCounterPartyAddress().getFourthAddressLine();
				}
				deliveryAddress.setCountrySubDivision(addressLine);
			}
			if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference().getCounterPartyAddress().getAddressCountry().getIsoCountryAlphaTwoCode())){
				deliveryAddress.setCountry(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference().getCounterPartyAddress().getAddressCountry().getIsoCountryAlphaTwoCode());
			}
			risk.setDeliveryAddress(deliveryAddress);
				}
		   }
		 
		obWriteStandingOrderConsentRes.setInitiation(obDomesticStandingOrder1);	
		customDStandingOrderConsentsPOSTResponse.setData(obWriteStandingOrderConsentRes);
		customDStandingOrderConsentsPOSTResponse.setRisk(risk);
		}
		return customDStandingOrderConsentsPOSTResponse;
		
	} 

}
