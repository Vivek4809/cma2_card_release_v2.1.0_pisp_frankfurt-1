package com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * The action on the entity or resource which resulted in the event originating from the source / source sub system
 */
public enum EventAction {
  
  AUTHENTICATE("Authenticate"),
  
  AUTHORIZE("Authorize"),
  
  CREATE("Create"),
  
  UPDATE("Update"),
  
  DELETE("Delete");

  private String value;

  EventAction(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static EventAction fromValue(String text) {
    for (EventAction b : EventAction.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
}

