package com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain;

import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * EventData1
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-01-16T16:32:31.691+05:30")

public class EventData1   {
  @JsonProperty("electronicDevice")
  private ElectronicDevice electronicDevice = null;

  @JsonProperty("customerAuthenticationSession")
  private CustomerAuthenticationSession customerAuthenticationSession = null;

  @JsonProperty("digitalUser")
  private DigitalUser digitalUser = null;

  public EventData1 electronicDevice(ElectronicDevice electronicDevice) {
    this.electronicDevice = electronicDevice;
    return this;
  }

  /**
   * Get electronicDevice
   * @return electronicDevice
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public ElectronicDevice getElectronicDevice() {
    return electronicDevice;
  }

  public void setElectronicDevice(ElectronicDevice electronicDevice) {
    this.electronicDevice = electronicDevice;
  }

  public EventData1 customerAuthenticationSession(CustomerAuthenticationSession customerAuthenticationSession) {
    this.customerAuthenticationSession = customerAuthenticationSession;
    return this;
  }

  /**
   * Get customerAuthenticationSession
   * @return customerAuthenticationSession
  **/
  @ApiModelProperty(value = "")

  @Valid

  public CustomerAuthenticationSession getCustomerAuthenticationSession() {
    return customerAuthenticationSession;
  }

  public void setCustomerAuthenticationSession(CustomerAuthenticationSession customerAuthenticationSession) {
    this.customerAuthenticationSession = customerAuthenticationSession;
  }

  public EventData1 digitalUser(DigitalUser digitalUser) {
    this.digitalUser = digitalUser;
    return this;
  }

  /**
   * Get digitalUser
   * @return digitalUser
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public DigitalUser getDigitalUser() {
    return digitalUser;
  }

  public void setDigitalUser(DigitalUser digitalUser) {
    this.digitalUser = digitalUser;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    EventData1 eventData1 = (EventData1) o;
    return Objects.equals(this.electronicDevice, eventData1.electronicDevice) &&
        Objects.equals(this.customerAuthenticationSession, eventData1.customerAuthenticationSession) &&
        Objects.equals(this.digitalUser, eventData1.digitalUser);
  }

  @Override
  public int hashCode() {
    return Objects.hash(electronicDevice, customerAuthenticationSession, digitalUser);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class EventData1 {\n");
    
    sb.append("    electronicDevice: ").append(toIndentedString(electronicDevice)).append("\n");
    sb.append("    customerAuthenticationSession: ").append(toIndentedString(customerAuthenticationSession)).append("\n");
    sb.append("    digitalUser: ").append(toIndentedString(digitalUser)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

