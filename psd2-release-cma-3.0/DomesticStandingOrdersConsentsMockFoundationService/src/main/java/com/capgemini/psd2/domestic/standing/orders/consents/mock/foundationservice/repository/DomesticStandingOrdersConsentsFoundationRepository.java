package com.capgemini.psd2.domestic.standing.orders.consents.mock.foundationservice.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.capgemini.psd2.domestic.standing.orders.consents.mock.foundationservice.raml.domain.StandingOrderInstructionProposal;

@Repository
public interface DomesticStandingOrdersConsentsFoundationRepository extends MongoRepository<StandingOrderInstructionProposal, String> {

	
	public StandingOrderInstructionProposal findByPaymentInstructionProposalId(String PaymentInstructionProposalId);

}
