package com.capgemini.psd2.domestic.standing.orders.consents.mock.foundationservice.raml.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Defines if the schedule Item Date is the arrival date of the payment or execution date
 */
public enum RequestedOccurenceDateType {
  
  EXECUTION("Execution"),
  
  ARRIVAL("Arrival");

  private String value;

  RequestedOccurenceDateType(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static RequestedOccurenceDateType fromValue(String text) {
    for (RequestedOccurenceDateType b : RequestedOccurenceDateType.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
}

