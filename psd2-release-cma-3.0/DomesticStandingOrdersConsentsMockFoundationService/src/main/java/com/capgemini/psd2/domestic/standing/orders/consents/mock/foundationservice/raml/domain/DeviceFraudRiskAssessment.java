package com.capgemini.psd2.domestic.standing.orders.consents.mock.foundationservice.raml.domain;

import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Device Fraud Risk Assessment object
 */
@ApiModel(description = "Device Fraud Risk Assessment object")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-01-16T16:32:31.691+05:30")

public class DeviceFraudRiskAssessment   {
  @JsonProperty("eventData")
  private EventData eventData = null;

  @JsonProperty("eventAdditionalData")
  private EventAdditionalData eventAdditionalData = null;

  public DeviceFraudRiskAssessment eventData(EventData eventData) {
    this.eventData = eventData;
    return this;
  }

  /**
   * Get eventData
   * @return eventData
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public EventData getEventData() {
    return eventData;
  }

  public void setEventData(EventData eventData) {
    this.eventData = eventData;
  }

  public DeviceFraudRiskAssessment eventAdditionalData(EventAdditionalData eventAdditionalData) {
    this.eventAdditionalData = eventAdditionalData;
    return this;
  }

  /**
   * Get eventAdditionalData
   * @return eventAdditionalData
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public EventAdditionalData getEventAdditionalData() {
    return eventAdditionalData;
  }

  public void setEventAdditionalData(EventAdditionalData eventAdditionalData) {
    this.eventAdditionalData = eventAdditionalData;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DeviceFraudRiskAssessment deviceFraudRiskAssessment = (DeviceFraudRiskAssessment) o;
    return Objects.equals(this.eventData, deviceFraudRiskAssessment.eventData) &&
        Objects.equals(this.eventAdditionalData, deviceFraudRiskAssessment.eventAdditionalData);
  }

  @Override
  public int hashCode() {
    return Objects.hash(eventData, eventAdditionalData);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DeviceFraudRiskAssessment {\n");
    
    sb.append("    eventData: ").append(toIndentedString(eventData)).append("\n");
    sb.append("    eventAdditionalData: ").append(toIndentedString(eventAdditionalData)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

