
package com.capgemini.psd2.foundationservice.payment.submission.boi.adapter.client;

import org.springframework.http.HttpHeaders;

import com.capgemini.psd2.foundationservice.payment.submission.boi.adapter.domain.PaymentInstruction;
import com.capgemini.psd2.foundationservice.payment.submission.boi.adapter.domain.ValidationPassed;
import com.capgemini.psd2.rest.client.model.RequestInfo;

@FunctionalInterface
public interface PaymentSubmissionFoundationServiceClient {

	public ValidationPassed executePaymentSubmission(RequestInfo requestInfo, PaymentInstruction paymentInstruction, Class<ValidationPassed> response, HttpHeaders httpHeaders);

}
