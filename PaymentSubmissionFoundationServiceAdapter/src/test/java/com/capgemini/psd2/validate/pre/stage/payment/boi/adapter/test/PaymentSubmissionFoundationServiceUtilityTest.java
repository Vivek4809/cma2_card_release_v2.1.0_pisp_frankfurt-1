package com.capgemini.psd2.validate.pre.stage.payment.boi.adapter.test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyObject;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.foundationservice.payment.submission.boi.adapter.domain.PaymentInstruction;
import com.capgemini.psd2.foundationservice.payment.submission.boi.adapter.domain.ValidationPassed;
import com.capgemini.psd2.foundationservice.payment.submission.boi.adapter.transformer.PaymentSubmissionFoundationServiceTransformer;
import com.capgemini.psd2.foundationservice.payment.submission.boi.adapter.utility.PaymentSubmissionFoundationServiceUtility;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.domain.PaymentSubmissionExecutionResponse;
import com.capgemini.psd2.pisp.domain.CustomPaymentSubmissionPOSTRequest;
import com.capgemini.psd2.rest.client.model.RequestInfo;

@RunWith(SpringJUnit4ClassRunner.class)
public class PaymentSubmissionFoundationServiceUtilityTest {
	
	@InjectMocks
	private PaymentSubmissionFoundationServiceUtility paymentSubmissionFoundationServiceUtility;
	
	@Mock
	private PaymentSubmissionFoundationServiceTransformer transformer;
	
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void contextLoads() {
	}
	
	@Test
	public void transformResponseFromAPIToFDForPaymentSubmission(){
		
		PaymentInstruction paymentInstruction = new PaymentInstruction();
		CustomPaymentSubmissionPOSTRequest paymentSubmissionPOSTRequest = new CustomPaymentSubmissionPOSTRequest();
		Map<String, String> params = new HashMap<String, String>();
		
		Mockito.when(transformer.transformPaymentSubmissionPOSTRequest(anyObject(), anyObject())).thenReturn(paymentInstruction);
		
		paymentInstruction = paymentSubmissionFoundationServiceUtility.transformRequestFromAPIToFS(paymentSubmissionPOSTRequest, params);
		assertNotNull(paymentInstruction);
	}
	
	@Test
	public void transformResponseFromFSToAPIForPaymentSubmission(){
		
		ValidationPassed validationPassed = new ValidationPassed();
		PaymentSubmissionExecutionResponse paymentSubmissionExecutionResponse = new PaymentSubmissionExecutionResponse();
		
		Mockito.when(transformer.transformPaymentSubmissionResponse(anyObject())).thenReturn(paymentSubmissionExecutionResponse);
		
		paymentSubmissionExecutionResponse = paymentSubmissionFoundationServiceUtility.transformResponseFromFSToAPI(validationPassed);
		assertNotNull(paymentSubmissionExecutionResponse);
	}
	
	
	@Test
	public void createRequestHeadersUserHeaderNull(){
		
		ReflectionTestUtils.setField(paymentSubmissionFoundationServiceUtility, "userInReqHeader", "test");
		ReflectionTestUtils.setField(paymentSubmissionFoundationServiceUtility, "channelInReqHeader", "test");
		ReflectionTestUtils.setField(paymentSubmissionFoundationServiceUtility, "platformInReqHeader", "test");
		ReflectionTestUtils.setField(paymentSubmissionFoundationServiceUtility, "correlationReqHeader", "test");
		ReflectionTestUtils.setField(paymentSubmissionFoundationServiceUtility, "clientIdReqHeader_Payments", "client_id");
		ReflectionTestUtils.setField(paymentSubmissionFoundationServiceUtility, "clientSecretReqHeader_Payments", "client_secret");
		ReflectionTestUtils.setField(paymentSubmissionFoundationServiceUtility, "clientIdReqHeaderValue","clientIdReqHeaderValue");
		ReflectionTestUtils.setField(paymentSubmissionFoundationServiceUtility, "clientSecretReqHeaderValue", "clientSecretReqHeaderValue");

		CustomPaymentSubmissionPOSTRequest paymentSubmissionPOSTRequest = new CustomPaymentSubmissionPOSTRequest();
		
		Map<String, String> params = new HashMap<String, String>();
		params.put(PSD2Constants.USER_IN_REQ_HEADER, null);
		params.put(PSD2Constants.CHANNEL_IN_REQ_HEADER, "BOL");
		params.put(PSD2Constants.CORRELATION_REQ_HEADER, null);
		params.put(PSD2Constants.PLATFORM_IN_REQ_HEADER, "testPlatform");
		
		RequestInfo requestInfo = new RequestInfo();
		HttpHeaders headers = new HttpHeaders();
		
		headers = paymentSubmissionFoundationServiceUtility.createRequestHeaders(requestInfo, paymentSubmissionPOSTRequest, params);
		assertNotNull(headers);
		
	}
	
	@Test
	public void createRequestHeadersChannelHeaderNull(){
		
		ReflectionTestUtils.setField(paymentSubmissionFoundationServiceUtility, "userInReqHeader", "test");
		ReflectionTestUtils.setField(paymentSubmissionFoundationServiceUtility, "channelInReqHeader", "test");
		ReflectionTestUtils.setField(paymentSubmissionFoundationServiceUtility, "platformInReqHeader", "test");
		ReflectionTestUtils.setField(paymentSubmissionFoundationServiceUtility, "correlationReqHeader", "test");
		ReflectionTestUtils.setField(paymentSubmissionFoundationServiceUtility, "clientIdReqHeader_Payments", "client_id");
		ReflectionTestUtils.setField(paymentSubmissionFoundationServiceUtility, "clientSecretReqHeader_Payments", "client_secret");
		ReflectionTestUtils.setField(paymentSubmissionFoundationServiceUtility, "clientIdReqHeaderValue","clientIdReqHeaderValue");
		ReflectionTestUtils.setField(paymentSubmissionFoundationServiceUtility, "clientSecretReqHeaderValue", "clientSecretReqHeaderValue");
		
		CustomPaymentSubmissionPOSTRequest paymentSubmissionPOSTRequest = new CustomPaymentSubmissionPOSTRequest();
		Map<String, String> params = new HashMap<String, String>();
		params.put(PSD2Constants.USER_IN_REQ_HEADER, "User");
		params.put(PSD2Constants.CHANNEL_IN_REQ_HEADER, null);
		params.put(PSD2Constants.CORRELATION_REQ_HEADER, "testCorrelation");
		params.put(PSD2Constants.PLATFORM_IN_REQ_HEADER, null);
		
		RequestInfo requestInfo = new RequestInfo();
		HttpHeaders headers = new HttpHeaders();
		
		headers = paymentSubmissionFoundationServiceUtility.createRequestHeaders(requestInfo, paymentSubmissionPOSTRequest, params);
		assertNotNull(headers);
		
	}
	
	

}
