package com.capgemini.psd2.pisp.standing.order.consent.routing.adapter.routing;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.pisp.stage.operations.adapter.DomesticStandingOrdersPaymentStagingAdapter;

@Component
public class DStandingOrderConsentsCoreSystemAdapterFactory implements ApplicationContextAware, DStandingOrderConsentAdapterFactory {

	/** The application context. */
	private ApplicationContext applicationContext;
	
	@Override
	public DomesticStandingOrdersPaymentStagingAdapter getDomesticStandingOrdersSetupStagingInstance(
			String coreSystemName) {
		return (DomesticStandingOrdersPaymentStagingAdapter) applicationContext.getBean(coreSystemName);
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext){
		
		this.applicationContext = applicationContext;
	}

}
