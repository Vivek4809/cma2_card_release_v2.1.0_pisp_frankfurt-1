package com.capgemini.psd2.scaconsenthelper.constants;

public interface SCAConsentHelperConstants {
	
	public static final String AUTHORIZATION_HEADER= "Authorization";	
	public static final String BASIC = "Basic";
	public static final String INTENT_DATA = "intentData";
	public static final String CUSTOM_SESSION_CONTROLL_COOKIE="SessionControllCookie";
	public static final String EXCEPTION = "exception";
	public static final String OAUTH_URL_PARAM = "oAuthUrl";
	public static final String UNAUTHORIZED_ERROR ="unauthorizedFlag";
}
