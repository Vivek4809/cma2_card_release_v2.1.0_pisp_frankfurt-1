package com.capgemini.psd2.foundationservice.insertprestagepayment.boi.adapter.test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyObject;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Qualifier;

import com.capgemini.psd2.adapter.datetime.utility.TimeZoneDateTimeAdapter;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.adapter.fraudnet.domain.FraudServiceResponse;
import com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.constants.InsertPreStagePaymentFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.domain.DeliveryAddress;
import com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.domain.FraudInputs;
import com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.domain.PayeeInformation;
import com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.domain.PayerInformation;
import com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.domain.Payment;
import com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.domain.PaymentInformation;
import com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.domain.PaymentInstruction;
import com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.domain.ValidationPassed;
import com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.transformer.InsertPreStagePaymentFoundationServiceTransformer;
import com.capgemini.psd2.pisp.domain.CreditorAccount;
import com.capgemini.psd2.pisp.domain.CreditorAgent;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.domain.DebtorAccount;
import com.capgemini.psd2.pisp.domain.DebtorAgent;
import com.capgemini.psd2.pisp.domain.DebtorAgent.SchemeNameEnum;
import com.capgemini.psd2.pisp.domain.PaymentSetup;
import com.capgemini.psd2.pisp.domain.PaymentSetupInitiation;
import com.capgemini.psd2.pisp.domain.PaymentSetupInitiationInstructedAmount;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponse;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponse.StatusEnum;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponseInitiation;
import com.capgemini.psd2.pisp.domain.PaymentSetupStagingResponse;
import com.capgemini.psd2.pisp.domain.RemittanceInformation;
import com.capgemini.psd2.pisp.domain.Risk;
import com.capgemini.psd2.pisp.domain.Risk.PaymentContextCodeEnum;
import com.capgemini.psd2.pisp.domain.RiskDeliveryAddress;
import com.capgemini.psd2.validator.PSD2Validator;

	public class InsertPreStagePaymentFoundationServiceTransformerTest {
			
			@Before
			public void setUp(){
				MockitoAnnotations.initMocks(this);
			}
			
			/**
			 * Context loads.
			 */
			@Test
			public void contextLoads() {
			}
			
			@InjectMocks
			private InsertPreStagePaymentFoundationServiceTransformer transformer;
			
			@Mock
			@Qualifier("PSD2ResponseValidator")
			private PSD2Validator validator;
			
			@Mock
			private TimeZoneDateTimeAdapter timeZoneDateTimeAdapter;
			
			@Test
			public void transformPaymentInformationAPIToFDForInsertTest1(){
				
				Map<String, String> params = new HashMap<String, String>();

				params.put(InsertPreStagePaymentFoundationServiceConstants.USER_ID, "testUser");
				params.put(InsertPreStagePaymentFoundationServiceConstants.CHANNEL_ID, "BOL");
				params.put(InsertPreStagePaymentFoundationServiceConstants.CORRELATION_ID, "testCorrelation");
				params.put(InsertPreStagePaymentFoundationServiceConstants.PLATFORM_ID, "testPlatform");
				params.put(InsertPreStagePaymentFoundationServiceConstants.VALIDATION_STATUS, "Pending");
				
				PaymentSetup data = new PaymentSetup();
				PaymentSetupInitiation initiation = new PaymentSetupInitiation();
				PaymentInstruction paymentInstruction = new PaymentInstruction();
				CustomPaymentSetupPOSTRequest paymentSetupPOSTRequest = new CustomPaymentSetupPOSTRequest();
				
				initiation.setInstructionIdentification("ANSM023");
				initiation.setEndToEndIdentification("FRESCO.21302.GFX.37");
				
				DebtorAccount debtorAccount = new DebtorAccount();
				debtorAccount.setIdentification("01234567891011");
				debtorAccount.setName("Andrea Smith");
				debtorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAccount.SchemeNameEnum.SORTCODEACCOUNTNUMBER);
				
				DebtorAgent debtorAgent = new DebtorAgent();
				debtorAgent.setIdentification("SC112800");
				debtorAgent.setSchemeName(SchemeNameEnum.BICFI);		
				
				CreditorAccount creditorAccount = new CreditorAccount();
				creditorAccount.setIdentification("01234567891011");
				creditorAccount.setName("Bob Clements");
				creditorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.CreditorAccount.SchemeNameEnum.SORTCODEACCOUNTNUMBER);
				
				CreditorAgent creditorAgent = new CreditorAgent();
				creditorAgent.setIdentification("080800");//In FS side it is expected as Integer
				creditorAgent.setSchemeName(com.capgemini.psd2.pisp.domain.CreditorAgent.SchemeNameEnum.BICFI);
				
				PaymentSetupInitiationInstructedAmount instructedAmount = new PaymentSetupInitiationInstructedAmount();
				instructedAmount.setAmount("30.00");
				instructedAmount.setCurrency("GBP");
				
				RemittanceInformation remittanceInformation = new RemittanceInformation();
				remittanceInformation.setReference("FRESCO-037");
				remittanceInformation.setUnstructured("Internal ops code 5120103");
				
				List<String> addressLine = new LinkedList<>();
				addressLine.add("test1");
				addressLine.add("test2");
				
				List<String> countrySubDivision = new LinkedList<>();
				countrySubDivision.add("test1");
				countrySubDivision.add("test2");
				
				RiskDeliveryAddress riskDeliveryAddress = new RiskDeliveryAddress();
				riskDeliveryAddress.setAddressLine(addressLine);
				riskDeliveryAddress.setCountrySubDivision(countrySubDivision);
				riskDeliveryAddress.setCountry("India");
				initiation.setCreditorAccount(creditorAccount);
				initiation.setCreditorAgent(creditorAgent);
				initiation.setDebtorAccount(debtorAccount);
				initiation.setDebtorAgent(debtorAgent);
				initiation.setInstructedAmount(instructedAmount);
				initiation.setRemittanceInformation(remittanceInformation);
				
				
				Risk risk = new Risk();
				risk.setPaymentContextCode(PaymentContextCodeEnum.PERSONTOPERSON);
				risk.setDeliveryAddress(riskDeliveryAddress);
				
				data.setInitiation(initiation);
				
					
				paymentSetupPOSTRequest.setData(data);
				paymentSetupPOSTRequest.setRisk(risk);
				
				paymentInstruction = transformer.transformPaymentInformationAPIToFDForInsert(paymentSetupPOSTRequest, params);
				assertNotNull(paymentInstruction);
			}
			
			@Test
			public void testTransformPaymentRetrievalTestRiskNull(){
				
				Map<String, String> params = new HashMap<String, String>();

				params.put(InsertPreStagePaymentFoundationServiceConstants.USER_ID, "testUser");
				params.put(InsertPreStagePaymentFoundationServiceConstants.CHANNEL_ID, "BOL");
				params.put(InsertPreStagePaymentFoundationServiceConstants.CORRELATION_ID, "testCorrelation");
				params.put(InsertPreStagePaymentFoundationServiceConstants.PLATFORM_ID, "testPlatform");
				params.put(InsertPreStagePaymentFoundationServiceConstants.VALIDATION_STATUS, "Pending");
				
				PaymentSetup data = new PaymentSetup();
				PaymentSetupInitiation initiation = new PaymentSetupInitiation();
				PaymentInstruction paymentInstruction = new PaymentInstruction();
				CustomPaymentSetupPOSTRequest paymentSetupPOSTRequest = new CustomPaymentSetupPOSTRequest();
				
				initiation.setInstructionIdentification("ANSM023");
				initiation.setEndToEndIdentification("FRESCO.21302.GFX.37");
				
				DebtorAccount debtorAccount = new DebtorAccount();
				debtorAccount.setIdentification("01234567891011");
				debtorAccount.setName("Andrea Smith");
				debtorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAccount.SchemeNameEnum.SORTCODEACCOUNTNUMBER);
				
				DebtorAgent debtorAgent = new DebtorAgent();
				debtorAgent.setIdentification("SC112800");
				debtorAgent.setSchemeName(SchemeNameEnum.BICFI);		
				
				CreditorAccount creditorAccount = new CreditorAccount();
				creditorAccount.setIdentification("01234567891011");
				creditorAccount.setName("Bob Clements");
				creditorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.CreditorAccount.SchemeNameEnum.SORTCODEACCOUNTNUMBER);
				
				CreditorAgent creditorAgent = new CreditorAgent();
				creditorAgent.setIdentification("080800");//In FS side it is expected as Integer
				creditorAgent.setSchemeName(com.capgemini.psd2.pisp.domain.CreditorAgent.SchemeNameEnum.BICFI);
				
				PaymentSetupInitiationInstructedAmount instructedAmount = new PaymentSetupInitiationInstructedAmount();
				instructedAmount.setAmount("30.00");
				instructedAmount.setCurrency("GBP");
				
				RemittanceInformation remittanceInformation = new RemittanceInformation();
				remittanceInformation.setReference("FRESCO-037");
				remittanceInformation.setUnstructured("Internal ops code 5120103");
				
				List<String> addressLine = new LinkedList<>();
				addressLine.add("test1");
				addressLine.add("test2");
				
				List<String> countrySubDivision = new LinkedList<>();
				countrySubDivision.add("test1");
				countrySubDivision.add("test2");
				
				RiskDeliveryAddress riskDeliveryAddress = new RiskDeliveryAddress();
				riskDeliveryAddress.setAddressLine(addressLine);
				riskDeliveryAddress.setCountrySubDivision(countrySubDivision);
				riskDeliveryAddress.setCountry("India");
				initiation.setCreditorAccount(creditorAccount);
				initiation.setCreditorAgent(creditorAgent);
				initiation.setDebtorAccount(debtorAccount);
				initiation.setDebtorAgent(debtorAgent);
				initiation.setInstructedAmount(instructedAmount);
				initiation.setRemittanceInformation(remittanceInformation);
				
				
				Risk risk = new Risk();
				risk.setPaymentContextCode(PaymentContextCodeEnum.PERSONTOPERSON);
				risk.setDeliveryAddress(null);
				
				data.setInitiation(initiation);
				
					
				paymentSetupPOSTRequest.setData(data);
				paymentSetupPOSTRequest.setRisk(risk);
				
				paymentInstruction = transformer.transformPaymentInformationAPIToFDForInsert(paymentSetupPOSTRequest, params);
				assertNotNull(paymentInstruction);
			}
			
			@Test
			public void transformPaymentInformationAPIToFDForInsertTest2(){
				
				Map<String, String> params = new HashMap<String, String>();

				params.put(InsertPreStagePaymentFoundationServiceConstants.USER_ID, "testUser");
				params.put(InsertPreStagePaymentFoundationServiceConstants.CHANNEL_ID, "BOL");
				params.put(InsertPreStagePaymentFoundationServiceConstants.CORRELATION_ID, "testCorrelation");
				params.put(InsertPreStagePaymentFoundationServiceConstants.PLATFORM_ID, "testPlatform");
				params.put(InsertPreStagePaymentFoundationServiceConstants.VALIDATION_STATUS, "Pending");
				
				PaymentSetup data = new PaymentSetup();
				PaymentSetupInitiation initiation = new PaymentSetupInitiation();
				PaymentInstruction paymentInstruction = new PaymentInstruction();
				CustomPaymentSetupPOSTRequest paymentSetupPOSTRequest = new CustomPaymentSetupPOSTRequest();
				
				initiation.setInstructionIdentification("ANSM023");
				initiation.setEndToEndIdentification("FRESCO.21302.GFX.37");
				
				DebtorAccount debtorAccount = new DebtorAccount();
				debtorAccount.setIdentification("01234567");
				debtorAccount.setName("Andrea Smith");
				debtorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAccount.SchemeNameEnum.IBAN);
				
				CreditorAccount creditorAccount = new CreditorAccount();
				creditorAccount.setIdentification("21325698");
				creditorAccount.setName("Bob Clements");
				creditorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.CreditorAccount.SchemeNameEnum.IBAN);
				
				PaymentSetupInitiationInstructedAmount instructedAmount = new PaymentSetupInitiationInstructedAmount();
				instructedAmount.setAmount("30.00");
				instructedAmount.setCurrency("GBP");
				
				RemittanceInformation remittanceInformation = new RemittanceInformation();
				remittanceInformation.setReference("FRESCO-037");
				remittanceInformation.setUnstructured("Internal ops code 5120103");
				
				RiskDeliveryAddress riskDeliveryAddress = new RiskDeliveryAddress();
				riskDeliveryAddress.setCountry(null);
				
				initiation.setCreditorAccount(creditorAccount);
				initiation.setDebtorAccount(debtorAccount);
				initiation.setInstructedAmount(instructedAmount);
				initiation.setRemittanceInformation(remittanceInformation);
				
				Risk risk = new Risk();
				risk.setPaymentContextCode(PaymentContextCodeEnum.PERSONTOPERSON);
				risk.setDeliveryAddress(riskDeliveryAddress);
				
				data.setInitiation(initiation);
				
					
				paymentSetupPOSTRequest.setData(data);
				paymentSetupPOSTRequest.setRisk(risk);
				
				paymentInstruction = transformer.transformPaymentInformationAPIToFDForInsert(paymentSetupPOSTRequest, params);
				assertNotNull(paymentInstruction);
			}
			
			@Test
			public void transformPaymentInformationFDToAPIForInsertTest(){
				
				ValidationPassed validationPassed = new ValidationPassed();
				validationPassed.setSuccessCode("test");
				validationPassed.setSuccessMessage("test");
				
				PaymentSetupStagingResponse response = transformer.transformPaymentInformationFDToAPIForInsert(validationPassed);
				assertNotNull(response);
			}
			
			@Test
			public void transformPaymentInformationFDToAPIForUpdateTest(){
			
					ValidationPassed validationPassed = new ValidationPassed();
					validationPassed.setSuccessCode("test");
					validationPassed.setSuccessMessage("test");
					
					PaymentSetupStagingResponse response = new PaymentSetupStagingResponse();
					
					response = transformer.transformPaymentInformationFDToAPIForUpdate(validationPassed);
					assertNotNull(response);
			}
			
			@Test
			public void transformPaymentInformationAPIToFDForUpdateTest1(){
				
				//For Update
				CustomPaymentSetupPOSTResponse pr = new CustomPaymentSetupPOSTResponse();
	              PaymentSetupResponse data = new PaymentSetupResponse();
	              PaymentSetupResponseInitiation initiation = new PaymentSetupResponseInitiation();
	              
	              FraudServiceResponse fraudServiceResponse = new FraudServiceResponse();
	              fraudServiceResponse.setDecisionType("Decision");
	              
	              initiation.setInstructionIdentification("ANSM023");
	              initiation.setEndToEndIdentification("FRESCO.21302.GFX.37");
	              //initiation.setEndToEndIdentification("FS_PMI_004");
	              
	              DebtorAccount debtorAccount = new DebtorAccount();
	              debtorAccount.setIdentification("01234567891011");
	              debtorAccount.setName("Andrea Smith");
	              debtorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAccount.SchemeNameEnum.SORTCODEACCOUNTNUMBER);
	              
	              DebtorAgent debtorAgent = new DebtorAgent();
	              debtorAgent.setIdentification("SC112800");
	              debtorAgent.setSchemeName(SchemeNameEnum.BICFI);         
	              
	              CreditorAccount creditorAccount = new CreditorAccount();
	              creditorAccount.setIdentification("01234567891011");
	              creditorAccount.setName("Bob Clements");
	              creditorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.CreditorAccount.SchemeNameEnum.SORTCODEACCOUNTNUMBER);
	              
	              CreditorAgent creditorAgent = new CreditorAgent();
	              creditorAgent.setIdentification("080800");//In FS side it is expected as Integer
	              creditorAgent.setSchemeName(com.capgemini.psd2.pisp.domain.CreditorAgent.SchemeNameEnum.BICFI);
	              
	              PaymentSetupInitiationInstructedAmount instructedAmount = new PaymentSetupInitiationInstructedAmount();
	              instructedAmount.setAmount("30.00");
	              instructedAmount.setCurrency("GBP");
	              
	              RemittanceInformation remittanceInformation = new RemittanceInformation();
	              remittanceInformation.setReference("FRESCO-037");
	              remittanceInformation.setUnstructured("Internal ops code 5120103");
	              
	              List<String> addressLine = new LinkedList<>();
	              addressLine.add("test1");
	              addressLine.add("test2");
					
	              List<String> countrySubDivision = new LinkedList<>();
	              countrySubDivision.add("test1");
	              countrySubDivision.add("test2");
					
	              RiskDeliveryAddress riskDeliveryAddress = new RiskDeliveryAddress();
	              riskDeliveryAddress.setAddressLine(addressLine);
	              riskDeliveryAddress.setCountrySubDivision(countrySubDivision);
	              riskDeliveryAddress.setCountry("India");
	              
	              initiation.setCreditorAccount(creditorAccount);
	              initiation.setCreditorAgent(creditorAgent);
	              initiation.setDebtorAccount(debtorAccount);
	              initiation.setDebtorAgent(debtorAgent);
	              initiation.setInstructedAmount(instructedAmount);
	              initiation.setRemittanceInformation(remittanceInformation);
	              
	              
	              Risk risk = new Risk();
	              risk.setPaymentContextCode(PaymentContextCodeEnum.PERSONTOPERSON);
	              risk.setDeliveryAddress(riskDeliveryAddress);
	              
	              data.setInitiation(initiation);
	              
	              //For Update
	              data.setPaymentId("817d632e0d1a42c8881a988c4c27d3a1");
	              data.setStatus(StatusEnum.ACCEPTEDTECHNICALVALIDATION);
	              String date = "2016-01-01T17:07:20+0000";
	              data.setCreationDateTime(date);
	              
	              pr.setFraudnetResponse(fraudServiceResponse);
	              pr.setData(data);
	              pr.setRisk(risk);

	              Map<String, String> params = new HashMap<String, String>();

	              params.put(InsertPreStagePaymentFoundationServiceConstants.USER_ID, "testUser");
	              params.put(InsertPreStagePaymentFoundationServiceConstants.CHANNEL_ID, "BOL");
	              params.put(InsertPreStagePaymentFoundationServiceConstants.CORRELATION_ID, "testCorrelation");
	              params.put(InsertPreStagePaymentFoundationServiceConstants.PLATFORM_ID, "testPlatform");
	              params.put(InsertPreStagePaymentFoundationServiceConstants.VALIDATION_STATUS, "Pending");


				ValidationPassed validationPassed = new ValidationPassed();
				
				validationPassed.setSuccessMessage("test");
				
				PaymentInstruction paymentInstruction = transformer.transformPaymentInformationAPIToFDForUpdate(pr, params);
				assertNotNull(paymentInstruction);
				
			}
			
			@Test
			public void transformPaymentInformationAPIToFDForUpdateTest2(){
				
				//For Update
				CustomPaymentSetupPOSTResponse pr = new CustomPaymentSetupPOSTResponse();
	              PaymentSetupResponse data = new PaymentSetupResponse();
	              PaymentSetupResponseInitiation initiation = new PaymentSetupResponseInitiation();
	              
	              initiation.setInstructionIdentification("ANSM023");
	              initiation.setEndToEndIdentification("FRESCO.21302.GFX.37");
	              initiation.setEndToEndIdentification("FS_PMI_004");
	              
	              DebtorAccount debtorAccount = new DebtorAccount();
	              debtorAccount.setIdentification("01234567");
	              debtorAccount.setName("Andrea Smith");
	              debtorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAccount.SchemeNameEnum.IBAN);
	
	              CreditorAccount creditorAccount = new CreditorAccount();
	              creditorAccount.setIdentification("21325698");
	              creditorAccount.setName("Bob Clements");
	              creditorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.CreditorAccount.SchemeNameEnum.IBAN);

	              PaymentSetupInitiationInstructedAmount instructedAmount = new PaymentSetupInitiationInstructedAmount();
	              instructedAmount.setAmount("30.00");
	              instructedAmount.setCurrency("GBP");
	              
	              RemittanceInformation remittanceInformation = new RemittanceInformation();
	              remittanceInformation.setReference("FRESCO-037");
	              remittanceInformation.setUnstructured("Internal ops code 5120103");
	              
	              RiskDeliveryAddress riskDeliveryAddress = new RiskDeliveryAddress();
	              riskDeliveryAddress.setCountry("India");
	              
	              initiation.setCreditorAccount(creditorAccount);
	              initiation.setDebtorAccount(debtorAccount);
	              initiation.setInstructedAmount(instructedAmount);
	              initiation.setRemittanceInformation(remittanceInformation);
	              
	              
	              Risk risk = new Risk();
	              risk.setPaymentContextCode(PaymentContextCodeEnum.PERSONTOPERSON);
	              risk.setDeliveryAddress(riskDeliveryAddress);
	              
	              data.setInitiation(initiation);
	              
	              //For Update
	              data.setPaymentId("817d632e0d1a42c8881a988c4c27d3a1");
	              data.setStatus(StatusEnum.ACCEPTEDTECHNICALVALIDATION);
	              String date = "2016-01-01T17:07:20+0000";
	              data.setCreationDateTime(date);
	              
	              pr.setData(data);
	              pr.setRisk(risk);

	              Map<String, String> params = new HashMap<String, String>();

	              params.put(InsertPreStagePaymentFoundationServiceConstants.USER_ID, "testUser");
	              params.put(InsertPreStagePaymentFoundationServiceConstants.CHANNEL_ID, "BOL");
	              params.put(InsertPreStagePaymentFoundationServiceConstants.CORRELATION_ID, "testCorrelation");
	              params.put(InsertPreStagePaymentFoundationServiceConstants.PLATFORM_ID, "testPlatform");
	              params.put(InsertPreStagePaymentFoundationServiceConstants.VALIDATION_STATUS, "Pending");


				ValidationPassed validationPassed = new ValidationPassed();
				
				validationPassed.setSuccessMessage("test");
				
				PaymentInstruction paymentInstruction = transformer.transformPaymentInformationAPIToFDForUpdate(pr, params);
				assertNotNull(paymentInstruction);
				
			}
			@Test
			public void testTransformPaymentRetrievalTest1(){
				PaymentInstruction paymentInstruction = new PaymentInstruction();
				
				FraudInputs fraudInputs = new FraudInputs();
				fraudInputs.setSystemResponse("Response");
				
				Map<String, String> params = new HashMap<>();
				params.put(InsertPreStagePaymentFoundationServiceConstants.USER_ID, "testUser");
				params.put(InsertPreStagePaymentFoundationServiceConstants.CHANNEL_ID, "BOL");
				params.put(InsertPreStagePaymentFoundationServiceConstants.CORRELATION_ID, "testCorrelation");
				params.put(InsertPreStagePaymentFoundationServiceConstants.PLATFORM_ID, "testPlatform");
				params.put(InsertPreStagePaymentFoundationServiceConstants.VALIDATION_STATUS, "Pending");
				
				Payment payment= new Payment();
				payment.setPaymentId("FSPID_0000000000226");
				payment.setRequestType("Q");
	
				Date createdOn = new Date();
				payment.setCreatedOn(createdOn); 
				payment.setChannelUserID("59000037");
	
				
				PayeeInformation payee= new PayeeInformation();
				payee.setIban("IE06BOFI90256597810260");
				payee.setBic("1");
		
				payee.setBeneficiaryAccountNumber("97810260");
				payee.setBeneficiaryName("Mark Rogers");
				payee.setAccountSecondaryID("83445202");

				DeliveryAddress deliveryAddress = new DeliveryAddress();
				deliveryAddress.setAddressLine1("No1 Dublin Road");
				deliveryAddress.setAddressLine2("Dublin 2");
				deliveryAddress.setStreetName("AcaciaAvenue");
				deliveryAddress.setBuildingNumber("1234");
				deliveryAddress.setPostCode("S571201");
				deliveryAddress.setTownName("WallStreet");
				deliveryAddress.setCountrySubDivision1("NI");
				deliveryAddress.setCountrySubDivision2("UK");
				deliveryAddress.setCountry("Ireland");
				payee.setDeliveryAddress(deliveryAddress);
				payee.setBeneficiaryNsc("902565");

				payee.setBeneficiaryCurrency("EUR");
				payee.setMerchentCategoryCode("123456");

				
				
				
				PaymentInformation paymentInfo= new PaymentInformation();
				paymentInfo.setPaymentContextCode("PersonToPerson");
				paymentInfo.setAmount(new BigDecimal ("200.0"));
				paymentInfo.setInstructionIdentification("987654432");
				Date paymentDate = new Date();
				paymentInfo.setPaymentDate(paymentDate); 
				

				paymentInfo.setInitiatingPartyName("gergaerg");
				paymentInfo.setBeneficiaryReference("gergeargerg");
				paymentInfo.setJurisdiction("ROI");
				Date createdDateTime = new Date();
				paymentInfo.setCreatedDateTime(createdDateTime);; 
				paymentInfo.setUnstructured("ngf12345");
				paymentInfo.setStatus("fefv123445");
				paymentInfo.setFraudInputs(fraudInputs);
				
				
				
				PayerInformation payer= new PayerInformation();
				payer.setAccountName("Savings Account");
				//payer.setCifLinkID("8434623");
				payer.setIban("IE65BOFI90570710059532");
				payer.setBic("BOFIIE2DXXX");
				payer.setNsc("905707");
				payer.setAccountNumber("10059532");

				payer.setAccountSecondaryID("835181213");
				payer.setPayerCurrency("EUR");
				payer.setMerchantCustomerIdentification("12345");
				
				payment.setPayeeInformation(payee);
				payment.setPayerInformation(payer);
				payment.setPaymentInformation(paymentInfo);
				
				
				paymentInstruction.setPayment(payment);
				
				Mockito.doNothing().when(validator).validate(anyObject());
				CustomPaymentSetupPOSTResponse paymentSetupPOSTResponse = transformer.transformPaymentRetrieval(paymentInstruction);
			
				assertNotNull(paymentSetupPOSTResponse);
			}
			@Test
			public void testTransformPaymentRetrievalTestAccntnoNscNotNull(){
				PaymentInstruction paymentInstruction = new PaymentInstruction();

				Map<String, String> params = new HashMap<>();

				params.put(InsertPreStagePaymentFoundationServiceConstants.USER_ID, "testUser");
				params.put(InsertPreStagePaymentFoundationServiceConstants.CHANNEL_ID, "BOL");
				params.put(InsertPreStagePaymentFoundationServiceConstants.CORRELATION_ID, "testCorrelation");
				params.put(InsertPreStagePaymentFoundationServiceConstants.PLATFORM_ID, "testPlatform");
				params.put(InsertPreStagePaymentFoundationServiceConstants.VALIDATION_STATUS, "Pending");
				
				
				Payment payment= new Payment();
				
				payment.setPaymentId("FSPID_0000000000226");
				payment.setRequestType("Q");

				Date createdOn = new Date();
				payment.setCreatedOn(createdOn); 
				payment.setChannelUserID("59000037");

				
				PayeeInformation payee= new PayeeInformation();
				payee.setIban("IE06BOFI90256597810260");
				payee.setBic(null);

				payee.setBeneficiaryAccountNumber(null);
				payee.setBeneficiaryName("Mark Rogers");
				payee.setAccountSecondaryID("83445202");

				DeliveryAddress deliveryAddress = new DeliveryAddress();
				deliveryAddress.setAddressLine1("No1 Dublin Road");
				deliveryAddress.setAddressLine2("Dublin 2");
				deliveryAddress.setStreetName("AcaciaAvenue");
				deliveryAddress.setBuildingNumber("1234");
				deliveryAddress.setPostCode("S571201");
				deliveryAddress.setTownName("WallStreet");
				deliveryAddress.setCountrySubDivision1("NI");
				deliveryAddress.setCountrySubDivision2("UK");
				deliveryAddress.setCountry("Ireland");
				payee.setDeliveryAddress(deliveryAddress);
				payee.setBeneficiaryNsc("902565");

				payee.setBeneficiaryCurrency("EUR");
				payee.setMerchentCategoryCode("123456");

				
				
				
				PaymentInformation paymentInfo= new PaymentInformation();
				paymentInfo.setPaymentContextCode("PersonToPerson");
				paymentInfo.setAmount(new BigDecimal ("200.0"));
				paymentInfo.setInstructionIdentification("987654432");
				Date paymentDate = new Date();
				paymentInfo.setPaymentDate(paymentDate); 
				

				paymentInfo.setInitiatingPartyName("gergaerg");

				paymentInfo.setBeneficiaryReference("gergeargerg");

				paymentInfo.setJurisdiction("ROI");

				Date createdDateTime = new Date();
				paymentInfo.setCreatedDateTime(createdDateTime);; 

				paymentInfo.setUnstructured("ngf12345");
				paymentInfo.setStatus("fefv123445");
				
				
				
				
				PayerInformation payer= new PayerInformation();
				payer.setAccountName("Savings Account");
				//payer.setCifLinkID("8434623");
				//payer.setIban("IE65BOFI90570710059532");
				payer.setBic(null);
				payer.setNsc("905707");
				payer.setAccountNumber("10059532");

				payer.setAccountSecondaryID("835181213");
				payer.setPayerCurrency("EUR");
				payer.setMerchantCustomerIdentification("12345");
				
				payment.setPayeeInformation(payee);
				payment.setPayerInformation(payer);
				payment.setPaymentInformation(paymentInfo);
				
				
				paymentInstruction.setPayment(payment);
				
				Mockito.doNothing().when(validator).validate(anyObject());
				CustomPaymentSetupPOSTResponse paymentSetupPOSTResponse = transformer.transformPaymentRetrieval(paymentInstruction);
			
				assertNotNull(paymentSetupPOSTResponse);
			}
			
			@Test
			public void testTransformPaymentRetrievalTest2(){
				PaymentInstruction paymentInstruction = new PaymentInstruction();

				Map<String, String> params = new HashMap<>();

				params.put(InsertPreStagePaymentFoundationServiceConstants.USER_ID, "testUser");
				params.put(InsertPreStagePaymentFoundationServiceConstants.CHANNEL_ID, "BOL");
				params.put(InsertPreStagePaymentFoundationServiceConstants.CORRELATION_ID, "testCorrelation");
				params.put(InsertPreStagePaymentFoundationServiceConstants.PLATFORM_ID, "testPlatform");
				params.put(InsertPreStagePaymentFoundationServiceConstants.VALIDATION_STATUS, "Pending");
				
				
				Payment payment= new Payment();
				
				payment.setPaymentId("FSPID_0000000000226");
				payment.setRequestType("Q");

				Date createdOn = new Date();
				payment.setCreatedOn(createdOn); 
				payment.setChannelUserID("59000037");

				
				PayeeInformation payee= new PayeeInformation();
				payee.setIban("IE06BOFI90256597810260");
				payee.setBic(null);

				payee.setBeneficiaryAccountNumber(null);
				payee.setBeneficiaryName("Mark Rogers");
				payee.setAccountSecondaryID("83445202");

				DeliveryAddress deliveryAddress = new DeliveryAddress();
				deliveryAddress.setAddressLine1("No1 Dublin Road");
				deliveryAddress.setAddressLine2("Dublin 2");
				deliveryAddress.setStreetName("AcaciaAvenue");
				deliveryAddress.setBuildingNumber("1234");
				deliveryAddress.setPostCode("S571201");
				deliveryAddress.setTownName("WallStreet");
				deliveryAddress.setCountrySubDivision1("NI");
				deliveryAddress.setCountrySubDivision2("UK");
				deliveryAddress.setCountry("Ireland");
				payee.setDeliveryAddress(deliveryAddress);
				payee.setBeneficiaryNsc("902565");

				payee.setBeneficiaryCurrency("EUR");
				payee.setMerchentCategoryCode("123456");

				
				
				
				PaymentInformation paymentInfo= new PaymentInformation();
				paymentInfo.setPaymentContextCode("PersonToPerson");
				paymentInfo.setAmount(new BigDecimal ("200.0"));
				paymentInfo.setInstructionIdentification("987654432");
				Date paymentDate = new Date();
				paymentInfo.setPaymentDate(paymentDate); 
				
	
				paymentInfo.setInitiatingPartyName("gergaerg");

				paymentInfo.setBeneficiaryReference("gergeargerg");

				paymentInfo.setJurisdiction("ROI");

				Date createdDateTime = new Date();
				paymentInfo.setCreatedDateTime(createdDateTime);; 

				paymentInfo.setUnstructured("ngf12345");
				paymentInfo.setStatus("fefv123445");
				
				
				
				
				PayerInformation payer= new PayerInformation();
				payer.setAccountName("Savings Account");
				//payer.setCifLinkID("8434623");
				payer.setIban("IE65BOFI90570710059532");
				payer.setBic(null);
				payer.setNsc("905707");
				payer.setAccountNumber(null);
				payer.setAccountSecondaryID("835181213");
				payer.setPayerCurrency("EUR");
				payer.setMerchantCustomerIdentification("12345");
				
				payment.setPayeeInformation(payee);
				payment.setPayerInformation(payer);
				payment.setPaymentInformation(paymentInfo);
				
				
				paymentInstruction.setPayment(payment);
				
				Mockito.doNothing().when(validator).validate(anyObject());
				CustomPaymentSetupPOSTResponse paymentSetupPOSTResponse = transformer.transformPaymentRetrieval(paymentInstruction);
			
				assertNotNull(paymentSetupPOSTResponse);
			}
			@Test(expected=AdapterException.class)
			public void transformPaymentInformationFDToAPIForInsertTestValidationPassedNull(){
			
					ValidationPassed validationPassed = new ValidationPassed();
					validationPassed.setSuccessCode("test");
					validationPassed.setSuccessMessage(null);
					
					PaymentSetupStagingResponse response = new PaymentSetupStagingResponse();
					
					response = transformer.transformPaymentInformationFDToAPIForInsert(validationPassed);
					
			}
			@Test(expected=AdapterException.class)
			public void transformPaymentInformationFDToAPIForUpdateTestValidationPassedNull(){
			
					ValidationPassed validationPassed = new ValidationPassed();
					validationPassed.setSuccessCode("test");
					validationPassed.setSuccessMessage(null);
					
					PaymentSetupStagingResponse response = new PaymentSetupStagingResponse();
					
					response = transformer.transformPaymentInformationFDToAPIForUpdate(validationPassed);
					
			}
			
	}


