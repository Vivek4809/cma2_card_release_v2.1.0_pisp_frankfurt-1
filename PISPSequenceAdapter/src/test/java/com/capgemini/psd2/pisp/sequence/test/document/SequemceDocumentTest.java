package com.capgemini.psd2.pisp.sequence.test.document;

import static org.junit.Assert.*;

import org.junit.Test;

import com.capgemini.psd2.pisp.sequence.document.SequenceDocument;

public class SequemceDocumentTest {

	@Test
	public void sequenceDocumentTest() {
		SequenceDocument sequenceDocument = new SequenceDocument();
		sequenceDocument.setId("1234");
		sequenceDocument.setCounter(Long.parseLong("1111"));
		assertEquals("1234", sequenceDocument.getId());
		assertEquals(Long.parseLong("1111"), sequenceDocument.getCounter());
	}
}
