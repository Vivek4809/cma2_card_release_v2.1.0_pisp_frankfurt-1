package com.capgemini.psd2.pisp.international.payments.controller;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.domain.CustomIPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.PaymentInternationalSubmitPOST201Response;
import com.capgemini.psd2.pisp.international.payments.service.InternationalPaymentsService;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.annotations.ApiParam;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-10-31T12:46:39.064+05:30")

@Controller
public class InternationalPaymentsApiController implements InternationalPaymentsApi {

	@Autowired
	private InternationalPaymentsService service;

	@Value("${app.responseErrorMessageEnabled}")
	private String responseErrorMessageEnabled;

	private final ObjectMapper objectMapper;

	private final HttpServletRequest request;

	@org.springframework.beans.factory.annotation.Autowired
	public InternationalPaymentsApiController(ObjectMapper objectMapper, HttpServletRequest request) {
		this.objectMapper = objectMapper;
		this.request = request;
	}

	@Override
	public Optional<ObjectMapper> getObjectMapper() {
		return Optional.ofNullable(objectMapper);
	}

	@Override
	public Optional<HttpServletRequest> getRequest() {
		return Optional.ofNullable(request);
	}

	@Override
	public ResponseEntity<PaymentInternationalSubmitPOST201Response> createInternationalPayments(
			@ApiParam(value = "Default", required = true) @Valid @RequestBody CustomIPaymentsPOSTRequest obWriteInternational1Param,
			@ApiParam(value = "The unique id of the ASPSP to which the request is issued. The unique id will be issued by OB.", required = true) @RequestHeader(value = "x-fapi-financial-id", required = true) String xFapiFinancialId,
			@ApiParam(value = "An Authorisation Token as per https://tools.ietf.org/html/rfc6750", required = true) @RequestHeader(value = "Authorization", required = true) String authorization,
			@ApiParam(value = "Every request will be processed only once per x-idempotency-key.  The Idempotency Key will be valid for 24 hours.", required = true) @RequestHeader(value = "x-idempotency-key", required = true) String xIdempotencyKey,
			@ApiParam(value = "A detached JWS signature of the body of the payload.", required = true) @RequestHeader(value = "x-jws-signature", required = true) String xJwsSignature,
			@ApiParam(value = "The time when the PSU last logged in with the TPP.  All dates in the HTTP headers are represented as RFC 7231 Full Dates. An example is below:  Sun, 10 Sep 2017 19:43:31 UTC") @RequestHeader(value = "x-fapi-customer-last-logged-time", required = false) String xFapiCustomerLastLoggedTime,
			@ApiParam(value = "The PSU's IP address if the PSU is currently logged in with the TPP.") @RequestHeader(value = "x-fapi-customer-ip-address", required = false) String xFapiCustomerIpAddress,
			@ApiParam(value = "An RFC4122 UID used as a correlation id.") @RequestHeader(value = "x-fapi-interaction-id", required = false) String xFapiInteractionId,
			@ApiParam(value = "Indicates the user-agent that the PSU is using.") @RequestHeader(value = "x-customer-user-agent", required = false) String xCustomerUserAgent) {
		{
			if (getObjectMapper().isPresent()) {
				CustomIPaymentsPOSTRequest requestBody = new CustomIPaymentsPOSTRequest();
				requestBody.setData(obWriteInternational1Param.getData());
				requestBody.setRisk(obWriteInternational1Param.getRisk());

				PaymentInternationalSubmitPOST201Response submissionResponse = service
						.createInternationalPaymentsResource(requestBody);
				return new ResponseEntity<>(submissionResponse, HttpStatus.CREATED);
			}
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_INVALIDFORMAT,
					ErrorMapKeys.RESOURCE_INVALIDFORMAT));
		}
	}

	@Override
	public ResponseEntity<PaymentInternationalSubmitPOST201Response> getInternationalPaymentsInternationalPaymentId(
			@ApiParam(value = "InternationalPaymentId", required = true) @PathVariable("InternationalPaymentId") String internationalPaymentId,
			@ApiParam(value = "The unique id of the ASPSP to which the request is issued. The unique id will be issued by OB.", required = true) @RequestHeader(value = "x-fapi-financial-id", required = true) String xFapiFinancialId,
			@ApiParam(value = "An Authorisation Token as per https://tools.ietf.org/html/rfc6750", required = true) @RequestHeader(value = "Authorization", required = true) String authorization,
			@ApiParam(value = "The time when the PSU last logged in with the TPP.  All dates in the HTTP headers are represented as RFC 7231 Full Dates. An example is below:  Sun, 10 Sep 2017 19:43:31 UTC") @RequestHeader(value = "x-fapi-customer-last-logged-time", required = false) String xFapiCustomerLastLoggedTime,
			@ApiParam(value = "The PSU's IP address if the PSU is currently logged in with the TPP.") @RequestHeader(value = "x-fapi-customer-ip-address", required = false) String xFapiCustomerIpAddress,
			@ApiParam(value = "An RFC4122 UID used as a correlation id.") @RequestHeader(value = "x-fapi-interaction-id", required = false) String xFapiInteractionId,
			@ApiParam(value = "Indicates the user-agent that the PSU is using.") @RequestHeader(value = "x-customer-user-agent", required = false) String xCustomerUserAgent) {

		if (getObjectMapper().isPresent()) {
			// validate id here
			// validateDomesticPaymentsGETRequest(submissionRetrieveRequest);
			PaymentInternationalSubmitPOST201Response submissionResponse = service
					.retrieveInternationalPaymentsResource(internationalPaymentId);
			return new ResponseEntity<>(submissionResponse, HttpStatus.OK);
		}
		throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_INVALIDFORMAT,
				ErrorMapKeys.RESOURCE_INVALIDFORMAT));
	}
}
