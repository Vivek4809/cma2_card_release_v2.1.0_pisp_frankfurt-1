package com.capgemini.psd2.pisp.international.payments.service;

import com.capgemini.psd2.pisp.domain.CustomIPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.PaymentInternationalSubmitPOST201Response;

public interface InternationalPaymentsService {

	public PaymentInternationalSubmitPOST201Response createInternationalPaymentsResource(
			CustomIPaymentsPOSTRequest paymentRequest);

	public PaymentInternationalSubmitPOST201Response retrieveInternationalPaymentsResource(
			String submissionId);

}
