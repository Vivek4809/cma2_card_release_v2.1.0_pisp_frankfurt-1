package com.capgemini.psd2.test.utilities.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.account.statements.utilities.AccountStatementDateUtilities;
import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.test.mock.data.AccountStatementsApiTestData;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountStatementsDateUtilitiesTest {
	
	
	@InjectMocks
	static AccountStatementDateUtilities utility = new AccountStatementDateUtilities();

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		Map<String, String> map=new HashMap<>();
		map.put("SIGNATURE", "Something wrong with x-jws-signature request header");
		Map<String, String> specificErrorMessageMap=new HashMap<>();
		specificErrorMessageMap.put("signature_invalid_content", "Invalid content prsent in joseheader of x-jws-signature header");
		specificErrorMessageMap.put("signature_missing", "signature header missing in request");
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(map);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessageMap);
	}

	@Test
	public void convertDateStringToLocalDateTimeTest() {
		assertEquals("2017-08-17T05:05:05.123",
				utility.convertDateStringToLocalDateTime("2017-08-17T05:05:05.123").toString());
	}

	@Test(expected = PSD2Exception.class)
	public void AccountTransactionDateUtilitiesException() {
		utility.convertDateStringToLocalDateTime("2017/08/17");
	}
	
	@Test
	public void AccountTransactionDateUtilitiesNormalFlow() {
		utility.convertDateStringToLocalDateTime("2017-08-17T00:05:05");
	}
	
	@Test
	public void AccountTransactionDateUtilitiesNormal() {
		utility.convertDateStringToLocalDateTime(null);
	}

	@Test
	public void convertZonedStringToLocalDateTimeTest() {
		assertNull(utility.convertZonedStringToLocalDateTime(null));
	}
	

	@Test(expected = PSD2Exception.class)
	public void convertZonedStringToLocalDateTimeException() {
		utility.convertZonedStringToLocalDateTime("2017/08/17");
	}
	
	@Test
	public void convertZonedStringToLocalDateTimeNormalFlow() {
		utility.convertZonedStringToLocalDateTime("2017-08-17T05:05:05.12");
	}
	
	

	@Test
	public void convertZonedDateTimeToStringTest() {
        String dt = "2019-01-19T00:00:00";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
        LocalDateTime formatDateTime = LocalDateTime.parse(dt, formatter);
			assertEquals("2019-01-19T00:00:00.000", utility.convertZonedDateTimeToString(formatDateTime));
	}
	
	@Test
	public void populateSetupDatesTest() {

		utility.populateSetupDates(AccountStatementsApiTestData.getMockAispConsent());
	}

	@Test
	public void getExpirationDateTest(){
		utility.getExpirationDate(AccountStatementsApiTestData.getMockAispConsent());
	}
	
	@After
	public void tearDown() throws Exception {
		utility = null;
	}


}
