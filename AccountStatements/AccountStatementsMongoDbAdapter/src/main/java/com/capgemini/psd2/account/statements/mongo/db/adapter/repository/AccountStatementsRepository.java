package com.capgemini.psd2.account.statements.mongo.db.adapter.repository;

import java.time.Instant;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.capgemini.psd2.aisp.mongo.db.adapter.domain.AccountStatementsCMA2;

public interface AccountStatementsRepository extends MongoRepository<AccountStatementsCMA2, String> {
	
	
	/**
	 * Find by account id.
	 *
	 * @param accountId the account id
	 * @return the balances GET response data
	 */
	
	public Page<AccountStatementsCMA2> findByAccountNumberAndAccountNSC(String accountNumber, String accountNSC, Pageable pageable);
	
	@Query("{'accountNumber' :  ?0, 'accountNSC' :  ?1, 'startDateTimeCopy': {$gt: ?2}, 'endDateTimeCopy': {$lt:?3 }}")
	public Page<AccountStatementsCMA2> findByAccountNumberAndAccountNSCAndStartDateTimeCopyBetween(String accountNumber, String accountNSC, 
			Instant startDateTimeCopy, Instant endDateTimeCopy, Pageable pageable);
	
	public Page<AccountStatementsCMA2> findByAccountNumberAndAccountNSCAndStatementId(String accountNumber, String accountNSC, 
			String statementId, Pageable pageable);

}
