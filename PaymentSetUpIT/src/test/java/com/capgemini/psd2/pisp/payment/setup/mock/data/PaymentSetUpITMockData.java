package com.capgemini.psd2.pisp.payment.setup.mock.data;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.psd2.pisp.domain.CreditorAccount;
import com.capgemini.psd2.pisp.domain.CreditorAgent;
import com.capgemini.psd2.pisp.domain.CreditorAgent.SchemeNameEnum;
import com.capgemini.psd2.pisp.domain.DebtorAccount;
import com.capgemini.psd2.pisp.domain.DebtorAgent;
import com.capgemini.psd2.pisp.domain.PaymentSetup;
import com.capgemini.psd2.pisp.domain.PaymentSetupInitiation;
import com.capgemini.psd2.pisp.domain.PaymentSetupInitiationInstructedAmount;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.RemittanceInformation;
import com.capgemini.psd2.pisp.domain.Risk;
import com.capgemini.psd2.pisp.domain.Risk.PaymentContextCodeEnum;
import com.capgemini.psd2.pisp.domain.RiskDeliveryAddress;

public class PaymentSetUpITMockData {

	private static PaymentSetupPOSTRequest paymentSetupPOSTRequestMockData;

	public static PaymentSetupPOSTRequest getPaymentSetupPOSTRequestMockData() {
		paymentSetupPOSTRequestMockData = new PaymentSetupPOSTRequest();

		PaymentSetup paymentSetup = new PaymentSetup();

		PaymentSetupInitiation initiation = new PaymentSetupInitiation();
		initiation.setInstructionIdentification("ABDDC");
		initiation.setEndToEndIdentification("FS_PMPSV_002");

		PaymentSetupInitiationInstructedAmount instructedAmount = new PaymentSetupInitiationInstructedAmount();
		instructedAmount.setAmount("777777777.0");
		instructedAmount.setCurrency("EUR");
		initiation.setInstructedAmount(instructedAmount);

		CreditorAgent creditorAgent = new CreditorAgent();
		creditorAgent.setIdentification("080801");
		creditorAgent.setSchemeName(SchemeNameEnum.BICFI);
		initiation.setCreditorAgent(creditorAgent);

		CreditorAccount creditorAccount = new CreditorAccount();
		creditorAccount.setIdentification("NWBK60161331926819");
		creditorAccount.setName("Test user");
		creditorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.CreditorAccount.SchemeNameEnum.IBAN);
		creditorAccount.setSecondaryIdentification("002");
		initiation.setCreditorAccount(creditorAccount);

		DebtorAccount debtorAccount = new DebtorAccount();
		debtorAccount.setIdentification("GB29NWBK60161331926819");
		debtorAccount.setName("Andrea Smith");
		debtorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAccount.SchemeNameEnum.IBAN);
		debtorAccount.setSecondaryIdentification("002");
		initiation.setDebtorAccount(debtorAccount);

		DebtorAgent debtorAgent = new DebtorAgent();
		debtorAgent.setIdentification("SC1128010");
		debtorAgent.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAgent.SchemeNameEnum.BICFI);
		initiation.setDebtorAgent(debtorAgent);

		RemittanceInformation remittanceInformation = new RemittanceInformation();
		remittanceInformation.setReference("FRESCO-101");
		remittanceInformation.setUnstructured("Internal ops code 5120101");
		initiation.setRemittanceInformation(remittanceInformation);

		paymentSetup.setInitiation(initiation);

		paymentSetupPOSTRequestMockData.setData(paymentSetup);

		Risk risk = new Risk();

		RiskDeliveryAddress deliveryAddress = new RiskDeliveryAddress();
		List<String> addressLine = new ArrayList<>();
		addressLine.add("Flat 7 ");
		addressLine.add(" Acacia Lodge ");
		deliveryAddress.setAddressLine(addressLine);
		deliveryAddress.setCountry("UK");
		deliveryAddress.setBuildingNumber("27");
		List<String> countrySubDivision = new ArrayList<>();
		countrySubDivision.add("ABCDEFGH");
		deliveryAddress.setCountrySubDivision(countrySubDivision);
		deliveryAddress.setPostCode("425009");
		deliveryAddress.setStreetName("Coastal area");
		deliveryAddress.setTownName("Croacia");
		risk.setDeliveryAddress(deliveryAddress);

		risk.setMerchantCategoryCode("5967");
		risk.setMerchantCustomerIdentification("053598653254");
		risk.setPaymentContextCode(PaymentContextCodeEnum.ECOMMERCEGOODS);
		paymentSetupPOSTRequestMockData.setRisk(risk);
		return paymentSetupPOSTRequestMockData;
	}
}