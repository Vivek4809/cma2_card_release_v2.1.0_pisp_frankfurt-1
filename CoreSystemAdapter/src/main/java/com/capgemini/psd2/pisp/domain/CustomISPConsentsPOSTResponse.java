package com.capgemini.psd2.pisp.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.capgemini.psd2.pisp.enums.ProcessConsentStatusEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Document(collection = "ISPConsentsFoundationResources")
public class CustomISPConsentsPOSTResponse extends OBWriteInternationalScheduledConsentResponse1 {

	@Id
	private String id;
	private ProcessConsentStatusEnum consentPorcessStatus;
	
	@JsonIgnore
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonIgnore
	public ProcessConsentStatusEnum getConsentPorcessStatus() {
		return consentPorcessStatus;
	}

	public void setConsentPorcessStatus(ProcessConsentStatusEnum consentPorcessStatus) {
		this.consentPorcessStatus = consentPorcessStatus;
	}

	@Override
	public String toString() {
		return "CustomISPConsentsPOSTResponse [id=" + id + ", consentPorcessStatus=" + consentPorcessStatus + "]";
	}
}
