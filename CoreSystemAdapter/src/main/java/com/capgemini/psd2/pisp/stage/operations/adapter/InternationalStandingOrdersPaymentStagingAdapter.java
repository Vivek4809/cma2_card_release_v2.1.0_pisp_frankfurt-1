package com.capgemini.psd2.pisp.stage.operations.adapter;

import java.util.Map;
import com.capgemini.psd2.pisp.domain.CustomIStandingOrderConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomIStandingOrderConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;

public interface InternationalStandingOrdersPaymentStagingAdapter {
	// createStage
	// Called from International Standing Orders Consents API
	public CustomIStandingOrderConsentsPOSTResponse processStandingOrderConsents(
			CustomIStandingOrderConsentsPOSTRequest standingOrderConsentsRequest,
			CustomPaymentStageIdentifiers customStageIdentifiers, Map<String, String> params,
			OBExternalConsentStatus1Code successStatus, OBExternalConsentStatus1Code failureStatus);

	// retrieveStage
	// Called from International Standing Orders Consents API, Submission API,
	// Consent Application
	public CustomIStandingOrderConsentsPOSTResponse retrieveStagedInternationalStandingOrdersConsents(
			CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params);

}
