package com.capgemini.psd2.pisp.domain;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class IPaymentsRetrieveGetRequest {
	private String internationalPaymentId;

	@NotNull
	@Size(min=1,max=40)
	public String getInternationalPaymentId() {
		return internationalPaymentId;
	}

	public void setInternationalPaymentId(String internationalPaymentId) {
		this.internationalPaymentId = internationalPaymentId;
	}
	

	

}
