package com.capgemini.psd2.aisp.platform.domain;

import java.util.Map;

import com.capgemini.psd2.aisp.domain.OBReadStatement1;


public class PlatformAccountStatementsResponse {
	
	// CMA2 response
		private OBReadStatement1 oBReadStatement1;

		// Additional Information
		private Map<String, String> additionalInformation;

		public OBReadStatement1 getoBReadStatement1() {
			return oBReadStatement1;
		}

		public void setoBReadStatement1(OBReadStatement1 oBReadStatement1) {
			this.oBReadStatement1 = oBReadStatement1;
		}

		public Map<String, String> getAdditionalInformation() {
			return additionalInformation;
		}

		public void setAdditionalInformation(Map<String, String> additionalInformation) {
			this.additionalInformation = additionalInformation;
		}

}
