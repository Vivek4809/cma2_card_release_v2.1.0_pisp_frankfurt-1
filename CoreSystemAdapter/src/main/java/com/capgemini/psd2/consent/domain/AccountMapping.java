/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.consent.domain;

import java.util.List;


/**
 * The Class AccountMapping.
 */
public class AccountMapping {

	/** The correlation id. */
	private String correlationId;
	
	/** The psu id. */
	private String psuId;
	
	/** The tpp CID. */
	private String tppCID;
	
	
	/** The account details. */
	private List<AccountDetails> accountDetails;
	
 
	/**
	 * Gets the correlation id.
	 *
	 * @return the correlation id
	 */
	public String getCorrelationId() {
		return correlationId;
	}

	/**
	 * Sets the correlation id.
	 *
	 * @param correlationId the new correlation id
	 */
	public void setCorrelationId(String correlationId) {
		this.correlationId = correlationId;
	}

	/**
	 * Gets the psu id.
	 *
	 * @return the psu id
	 */
	public String getPsuId() {
		return psuId;
	}

	/**
	 * Sets the psu id.
	 *
	 * @param psuId the new psu id
	 */
	public void setPsuId(String psuId) {
		this.psuId = psuId;
	}

	/**
	 * Gets the tpp CID.
	 *
	 * @return the tpp CID
	 */
	public String getTppCID() {
		return tppCID;
	}

	/**
	 * Sets the tpp CID.
	 *
	 * @param tppCID the new tpp CID
	 */
	public void setTppCID(String tppCID) {
		this.tppCID = tppCID;
	}


	/**
	 * Gets the account details.
	 *
	 * @return the account details
	 */
	public List<AccountDetails> getAccountDetails() {
		return accountDetails;
	}

	/**
	 * Sets the account details.
	 *
	 * @param accountDetails the new account details
	 */
	public void setAccountDetails(List<AccountDetails> accountDetails) {
		this.accountDetails = accountDetails;
	}

	
}
