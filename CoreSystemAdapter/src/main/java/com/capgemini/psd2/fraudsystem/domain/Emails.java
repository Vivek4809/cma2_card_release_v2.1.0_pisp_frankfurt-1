package com.capgemini.psd2.fraudsystem.domain;

import java.util.Map;

public class Emails {

	private String email;
	private String type;
	private Map<String, String> extension;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Map<String, String> getExtension() {
		return extension;
	}

	public void setExtension(Map<String, String> extension) {
		this.extension = extension;
	}
}