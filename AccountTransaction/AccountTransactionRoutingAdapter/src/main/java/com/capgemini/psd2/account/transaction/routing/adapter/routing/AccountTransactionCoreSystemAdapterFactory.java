package com.capgemini.psd2.account.transaction.routing.adapter.routing;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.adapter.AccountTransactionAdapter;

@Component
public class AccountTransactionCoreSystemAdapterFactory implements ApplicationContextAware, AccountTransactionAdapterFactory {

	private ApplicationContext applicationContext;
	
	@Override
	public AccountTransactionAdapter getAdapterInstance(String adapterName){
		return (AccountTransactionAdapter) applicationContext.getBean(adapterName);		
	}
		
	@Override
	public void setApplicationContext(ApplicationContext context) {
		  this.applicationContext = context;
	}

}
