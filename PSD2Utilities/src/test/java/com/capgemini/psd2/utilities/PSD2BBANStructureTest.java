package com.capgemini.psd2.utilities;


import static org.junit.Assert.assertEquals;

import org.iban4j.CountryCode;
import org.junit.Test;


public class PSD2BBANStructureTest {

	@Test
	public void valueOf_Test() {
		
		PSD2BBANStructure psd2BbanStructure = PSD2BBANStructure.forCountry(CountryCode.AD);
		
		assertEquals(20,psd2BbanStructure.getBbanLength());
				
	}

}
