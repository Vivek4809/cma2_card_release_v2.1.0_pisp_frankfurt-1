package com.capgemini.psd2.pisp.file.payment.routing.adapter.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.adapter.FilePaymentConsentsAdapter;
import com.capgemini.psd2.pisp.domain.CustomFilePaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomFilePaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.PlatformFilePaymentsFileResponse;
import com.capgemini.psd2.pisp.file.payment.routing.adapter.routing.FPaymentConsentsAdapterFactory;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component("filePaymentConsentsStagingRoutingAdapter")
public class FPaymentConsentsRoutingAdapterImpl implements FilePaymentConsentsAdapter {

	private FilePaymentConsentsAdapter beanInstance;

	/** The factory. */
	@Autowired
	private FPaymentConsentsAdapterFactory factory;

	/** The default adapter. */
	@Value("${app.paymentSetupStagingAdapter}")
	private String paymentSetupStagingAdapter;

	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Override
	public CustomFilePaymentConsentsPOSTResponse fetchFilePaymentMetadata(
			CustomPaymentStageIdentifiers stageIdentifiers, Map<String, String> params) {

		return getFileRoutingAdapter().fetchFilePaymentMetadata(stageIdentifiers, addHeaderParams(params));
	}

	private FilePaymentConsentsAdapter getFileRoutingAdapter() {
		if (beanInstance == null)
			beanInstance = factory.getFilePaymentSetupStagingInstance(paymentSetupStagingAdapter);
		return beanInstance;
	}

	private Map<String, String> addHeaderParams(Map<String, String> mapParam) {
		/*
		 * Below code will be used when update service will be called from
		 * payment submission API. For submission flow -> update, PSUId and
		 * ChannelName are mandatory fields for FS
		 */
		if (NullCheckUtils.isNullOrEmpty(mapParam))
			mapParam = new HashMap<>();

		if (null != reqHeaderAtrributes.getToken() && null != reqHeaderAtrributes.getToken().getSeviceParams()) {
			mapParam.put(PSD2Constants.CHANNEL_IN_REQ_HEADER,
					reqHeaderAtrributes.getToken().getSeviceParams().get(PSD2Constants.CHANNEL_NAME));
		}
		mapParam.put(PSD2Constants.USER_IN_REQ_HEADER, reqHeaderAtrributes.getPsuId());
		mapParam.put(PSD2Constants.CORRELATION_REQ_HEADER, reqHeaderAtrributes.getCorrelationId());
		mapParam.put(PSD2Constants.TENANT_ID, reqHeaderAtrributes.getTenantId());
		return mapParam;
	}

	@Override
	public CustomFilePaymentConsentsPOSTResponse createStagingFilePaymentConsents(
			CustomFilePaymentSetupPOSTRequest paymentSetupRequest, CustomPaymentStageIdentifiers stageIdentifiers,
			Map<String, String> params) {
		return getFileRoutingAdapter().createStagingFilePaymentConsents(paymentSetupRequest, stageIdentifiers,
				addHeaderParams(params));
	}

	@Override
	public String uploadFilePaymentConsents(MultipartFile paymentFile, String consentId) {
		return getFileRoutingAdapter().uploadFilePaymentConsents(paymentFile, consentId);
	}

	@Override
	public PlatformFilePaymentsFileResponse downloadFilePaymentConsents(
			CustomPaymentStageIdentifiers paymentStageIdentifiers) {
		return getFileRoutingAdapter().downloadFilePaymentConsents(paymentStageIdentifiers);
	}

	@Override
	public CustomFilePaymentConsentsPOSTResponse updateStagingFilePaymentConsents(
			CustomFilePaymentConsentsPOSTResponse response) {
		return getFileRoutingAdapter().updateStagingFilePaymentConsents(response);
	}
}
