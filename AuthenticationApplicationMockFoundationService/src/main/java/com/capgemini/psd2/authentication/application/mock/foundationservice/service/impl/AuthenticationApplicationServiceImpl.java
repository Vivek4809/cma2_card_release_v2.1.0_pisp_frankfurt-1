package com.capgemini.psd2.authentication.application.mock.foundationservice.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.authentication.application.mock.foundationservice.domain.AuthenticationMethodCode;
import com.capgemini.psd2.authentication.application.mock.foundationservice.domain.AuthenticationRequest;
import com.capgemini.psd2.authentication.application.mock.foundationservice.domain.CustomerAuthenticationSession;
import com.capgemini.psd2.authentication.application.mock.foundationservice.domain.DigitalUser;
import com.capgemini.psd2.authentication.application.mock.foundationservice.domain.DigitalUserRequest;
import com.capgemini.psd2.authentication.application.mock.foundationservice.domain.PersonBasicInformation;
import com.capgemini.psd2.authentication.application.mock.foundationservice.repository.AuthenticationApplicationRepository;
import com.capgemini.psd2.authentication.application.mock.foundationservice.repository.DigitalUserRequestApplicationRepository;
import com.capgemini.psd2.authentication.application.mock.foundationservice.service.AuthenticationApplicationService;
import com.capgemini.psd2.foundationservice.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.foundationservice.exceptions.MockFoundationServiceAuthException;
import com.capgemini.psd2.foundationservice.validator.ValidationUtility;


@Service
public class AuthenticationApplicationServiceImpl implements AuthenticationApplicationService {

	@Autowired
	private AuthenticationApplicationRepository repository;
	
	@Autowired 
	private DigitalUserRequestApplicationRepository durepository;

	@Autowired
	private ValidationUtility validationUtility;

	@Override
	public DigitalUserRequest retrieveBOLUserCredentials(DigitalUserRequest digitalUserReq) {

		validationUtility.validateUserLogin(digitalUserReq.getDigitalId());
		//sDigitalUserRequest digitalUserReq = null;
		/*try{
			 authenticationRequest = durepository.save(digitalUser);
		}catch(Exception ex){
			throw MockFoundationServiceAuthException.populateMockFoundationServiceException(ErrorCodeEnum.INTERNAL_SERVER_ERROR);
		}
		if (authenticationRequest == null) {
			throw MockFoundationServiceAuthException.populateMockFoundationServiceException(ErrorCodeEnum.AUTHENTICATION_FAILURE_LOGIN_AUTH);
		}*/
		digitalUserReq.setDigitalId("123456");
		DigitalUser du= new DigitalUser();
		du.setDigitalUserIdentifier("12345");
		du.setDigitalUserLockedOutIndicator(false);
		du.setSecureKeyAttemptsRemainingCount(12345);
		PersonBasicInformation personInformation= new PersonBasicInformation();
		personInformation.setFirstName("David");
		personInformation.setSurname("Dirsh");
		personInformation.setMothersMaidenName("Weaver");
		personInformation.setTitleCode("Mr");
		du.setPersonInformation(personInformation);
		CustomerAuthenticationSession customerAuthenticationSession= new CustomerAuthenticationSession();
		customerAuthenticationSession.setSecureAccessKeyUsed("123456");
		customerAuthenticationSession.setAuthenticationSessionIdentifier("123456");
		customerAuthenticationSession.setAuthenticationMethodCode(AuthenticationMethodCode.Password);
		List<CustomerAuthenticationSession> list= new ArrayList<CustomerAuthenticationSession>();
		list.add(customerAuthenticationSession);
		du.setCustomerAuthenticationSession(list);

		/*List secureAccessKeyParameters= new ArrayList();
		secureAccessKeyParameters.add(secureAccessKeyParameters);
		credential.setSecureAccessKeyParameters(secureAccessKeyParameters);
		List<Credentials> list= new ArrayList<Credentials>();
		list.add(credential);
		du.setCredentials(list);*/
		digitalUserReq.setDigitalUser(du);
		durepository.save(digitalUserReq);
		

		return digitalUserReq;
	}

	@Override
	public AuthenticationRequest retrieveB365UserCredentials(String userName, String password) {

		validationUtility.validateUserLogin(userName);
		AuthenticationRequest authenticationRequest = null;
		try{
			 authenticationRequest = repository.findByUserNameAndPassword(userName, password);
		}catch(Exception ex){
			throw MockFoundationServiceAuthException.populateMockFoundationServiceException(ErrorCodeEnum.INTERNAL_SERVER_ERROR);
		}

		if (authenticationRequest == null) {
			throw MockFoundationServiceAuthException.populateMockFoundationServiceException(ErrorCodeEnum.AUTHENTICATION_FAILURE_LOGIN_AUTH);
		}

		return authenticationRequest;
	}

}
