(function(module) {
try {
  module = angular.module('loginPartials');
} catch (e) {
  module = angular.module('loginPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/views/error-notice.html',
    '<div class="alert col-xs-12 col-sm-12 col-md-12" ng-show="errorMsgText.length" role="alert" aria-atomic="true" ng-class="(errorData.errorCSSClass==\'warning\') ? \'alert-warning\':\'alert-danger\' ">\n' +
    '    <p class="message" >\n' +
    '        <span class="alert-type" ng-if="!errorData.errorCSSClass" ng-bind="alertText"></span>\n' +
    '        <span ng-bind="errorMsgText"> </span>\n' +
    '        <ng-template ng-if="errorData.correlationId">\n' +
    '            <span class="alert-type" ng-bind="corRelationText"> </span>\n' +
    '            <span ng-bind="corRelationMsgText"></span>\n' +
    '        </ng-template>\n' +
    '    </p>  \n' +
    '    <p ng-if="isErrorMsgTextDescription" ng-bind-html="errorMsgTextDescription"> </p>\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('loginPartials');
} catch (e) {
  module = angular.module('loginPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/views/lightbox.html',
    '<div class="modal-body pop-confirm-body">\n' +
    '    <div class="confirm-msg">\n' +
    '        <p class="text-center"> {{\'LIGHTBOX1.DESCRIPTION\' | translate}}</p>\n' +
    '    </div>\n' +
    '\n' +
    '    <div class="modal-footer pop-confirm-footer button-section xs-button-section ">\n' +
    '        <div class="text-center">\n' +
    '            <button role="button" class="btn btn-primary" ng-click="loginInfo()" autofocus>\n' +
    '                    {{\'LIGHTBOX1.I_DONT_HAVE_BOI_KEYCODE_BUTTON_LABEL\' | translate}}\n' +
    '                </button>\n' +
    '            <button role="button" class="btn btn-primary" ng-click="cancelBtnClicked()">\n' +
    '                    {{\'LIGHTBOX1.I_HAVE_BOI_KEYCODE_BUTTON_LABEL\' | translate}}\n' +
    '                </button>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('loginPartials');
} catch (e) {
  module = angular.module('loginPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/views/loading-spinner.html',
    '<div class="block-ui-overlay"></div>\n' +
    '<div class="block-ui-message-container" aria-live="assertive" aria-atomic="true">\n' +
    '	<div class="block-ui-message" ng-class="$_blockUiMessageClass">\n' +
    '		<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>\n' +
    '	</div>\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('loginPartials');
} catch (e) {
  module = angular.module('loginPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/views/login.html',
    '<!--header start-->\n' +
    '<header>\n' +
    '    <div class="header-container">\n' +
    '        <div class="container">\n' +
    '            <a class="skip-main" href="javascript:void(0)" ng-click="focusMainContaint()">Skip to main content</a>\n' +
    '            <img class="logo" ng-src={{logoPath}} title="{{logoAltLabel}}" alt="{{logoAltLabel}}" ng-if="logoAltLabel.length" />\n' +
    '            <div class="portal-details">\n' +
    '                <h1 ng-bind="accountAccessText"> </h1>\n' +
    '                <h2 ng-bind="thirdPartyText"></h2>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</header>\n' +
    '<!--header end-->\n' +
    '\n' +
    '<!--section start-->\n' +
    '\n' +
    '\n' +
    '<div class="main-container clearfix">\n' +
    '    <div class="login-container">\n' +
    '        <h3 ng-bind="loginHeaderText"></h3>\n' +
    '        <error-notice ng-if="errorData" error-data="errorData"></error-notice>\n' +
    '        <form class="form-container" id="loginForm" method="POST" action="login" autocomplete="off">\n' +
    '            <input type="text" style="display:none" />\n' +
    '            <div class="form-group">\n' +
    '                <label for="username" ng-bind="userText"></label>\n' +
    '                <div class="input-group input-group-unstyled">\n' +
    '                    <input type="text" ng-keypress="filterValue($event)" maxlength="8" class="form-control" placeholder="{{userPlaceHolderLabel}}" ng-model="userId" name="username" id="username" autocomplete="off" />\n' +
    '                    <span class="help-holder"> \n' +
    '                            <a href="javascript:void(0)" aria-label="{{userTooltipLbl}}" class="input-group-addon" popover-trigger="\'focus mouseenter\'" popover-placement="{{placement.selected}}" popover-class="tooltip-content" uib-popover="{{userTooltipMsg}}">\n' +
    '                                <i class="fa fa-question-circle-o" aria-hidden="true"></i> \n' +
    '                            </a>\n' +
    '                        </span>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="form-group">\n' +
    '                <label for="pwd" ng-bind="passwordText"></label>\n' +
    '                <div class="input-group input-group-unstyled">\n' +
    '                    <input type="password" maxlength="6" class="form-control" ng-model="passcode" placeholder="{{passwordPlaceHolderLabel}}" name="password" id="pwd" ng-keypress="filterValue($event)" autocomplete="off">\n' +
    '                    <span class="help-holder">\n' +
    '                            <a href="javascript:void(0)"  aria-label="{{passwordTooltipLbl}}"class="input-group-addon" popover-trigger="\'focus mouseenter\'" popover-placement="{{placement.selected}}" popover-class="tooltip-content" uib-popover="{{passwordTooltipMsg}}">\n' +
    '                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>\n' +
    '                            </a>\n' +
    '                        </span>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '\n' +
    '            <p><strong ng-bind="noteTitleText"></strong><span ng-bind="loginNoteDescText"></span></p>\n' +
    '\n' +
    '            <a href="{{loginHelpUrl}}" class="need-help" ng-bind="needHelpText" target="_blank"></a>\n' +
    '            <div class="button-container"><button type="button" class="btn btn-primary pull-right" ng-click="loginAction()" ng-bind="continueText"></button>\n' +
    '                <button type="button" class="btn btn-secondary" ng-click="backToThirdParty()" ng-bind="backtoTPPText"></button>\n' +
    '\n' +
    '            </div>\n' +
    '        </form>\n' +
    '    </div>\n' +
    '    <div class="info-container">\n' +
    '        <h4 ng-bind-html="furtherText"></h4>\n' +
    '        <h5 ng-bind="boikeycodeText"></h5>\n' +
    '\n' +
    '        <img class="pull-right" alt="{{keyImageAlt}}" ng-src="build/img/phone.png" />\n' +
    '        <p><span ng-bind-html="boikeycodeAppText"></span></p>\n' +
    '\n' +
    '\n' +
    '        <p><span ng-bind-html="boikeycodeHelpText"></span>\n' +
    '        </p>\n' +
    '        <h5 ng-bind="registreKeyCodeText"></h5>\n' +
    '        <ul>\n' +
    '            <li><strong ng-bind="step1Text"></strong> <span ng-bind="step1DescText"></span></li>\n' +
    '            <li><strong ng-bind="step2Text"></strong> <span ng-bind="step2DescText"></span> </li>\n' +
    '            <li><strong ng-bind="step3Text"></strong> <span ng-bind-html="step3DescText"></span></li>\n' +
    '        </ul>\n' +
    '        <p><strong ng-bind="noteInfoText"></strong><span ng-bind="noteDescText"></span></p>\n' +
    '        <a href="{{login365Url}}" ng-bind="loginLinkText" target="_blank"></a>\n' +
    '    </div>\n' +
    '</div>\n' +
    '<!--section end-->\n' +
    '\n' +
    '\n' +
    '<!--footer start-->\n' +
    '<footer>\n' +
    '    <div class="container">\n' +
    '        <div class="row">\n' +
    '            <div class="col-xs-12">\n' +
    '                <ul class="footer-links">\n' +
    '                    <li>\n' +
    '                        <a href="{{aboutUsUrl}}" ng-bind="aboutUsText" target="_blank"></a>\n' +
    '                    </li>\n' +
    '                    <li>\n' +
    '                        <a href="{{privacyPolicyUrl}}" ng-bind="cookieText" target="_blank"></a>\n' +
    '                    </li>\n' +
    '                    <li>\n' +
    '                        <a href="{{tncUrl}}" ng-bind="tncText" target="_blank"></a>\n' +
    '                    </li>\n' +
    '                    <li>\n' +
    '                        <a href="{{helpUrl}}" ng-bind="helpText" target="_blank"></a>\n' +
    '                    </li>\n' +
    '                </ul>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <p class="regulatory-statement" ng-bind="regulatoryText"></p>\n' +
    '    </div>\n' +
    '</footer>\n' +
    '\n' +
    '<!--footer end -->\n' +
    '<!--  <model-pop-up modal="modelPopUpConf"></model-pop-up>  -->\n' +
    '<div ui-view="modal" autoscroll="false"></div>');
}]);
})();

(function(module) {
try {
  module = angular.module('loginPartials');
} catch (e) {
  module = angular.module('loginPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/views/modal-1.html',
    '<div class="modal-body pop-confirm-body lightbox1" role="dialog" style="">\n' +
    '    <span class="sr-only" aria-label="{{modelPopUpConf.lightbox1PopUpDisplayed}}"></span>\n' +
    '    <button type="button " class="close" ng-click="cancelBtnClicked() ">\n' +
    '        <img  alt="{{modelPopUpConf.lightbox1CloseImageAlt}}" class="close" ng-src ="build/img/closeBLUE@2x.png" autofocus>\n' +
    '    </button>\n' +
    '    <div class="confirm-msg">\n' +
    '        <p ng-bind="modelPopUpConf.lightBox1Description"></p>\n' +
    '    </div>\n' +
    '    <div class="modal-footer pop-confirm-footer button-section xs-button-section footer-lightbox1">\n' +
    '        <div class="btn-container">\n' +
    '            <button role="button" class="btn btn-lightbox1 btn-left" ng-click="cancelBtnClicked()" ng-bind="modelPopUpConf.lightBox1HaveBoiKeycodeApp" title="{{lightbox1ClosePopUp}}"></button>\n' +
    '            <button role="button" class="btn btn-lightbox1 text-left" ng-click="goToModal2()" ng-bind="modelPopUpConf.lightBox1IdontHaveBoiKeycodeApp" title="{{openPopUp}}"></button>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('loginPartials');
} catch (e) {
  module = angular.module('loginPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/views/modal-2.html',
    '<div class="modal-body  lightbox2" aria-live="polite">\n' +
    '    <span class="sr-only" ng-bind="modelPopUpConf.lightbox2PopUpDisplayed"></span>\n' +
    '    <button type="button " class="close" ng-click="cancelBtnClicked() ">\n' +
    '        <img  alt="{{modelPopUpConf.lightbox2CloseImageAlt}}" class="close" ng-src ="build/img/closeBLUE@2x.png" autofocus>\n' +
    '    </button>\n' +
    '    <div class="confirm-msg">\n' +
    '        <p ng-bind="modelPopUpConf.lightBox2Description"></p>\n' +
    '    </div>\n' +
    '\n' +
    '    <div class="modal-footer  button-section xs-button-section footer-lightbox2">\n' +
    '        <div class="box-lightbox2">\n' +
    '            <span class="sr-only" ng-bind="modelPopUpConf.optionLabel1"></span>\n' +
    '            <p ng-bind="modelPopUpConf.lightBox2IdontWantToContinue"></p>\n' +
    '            <button role="button" class="btn btn-lightbox2 " ng-click="goToTPP()" title="{{redirectToTpp}}" ng-bind="modelPopUpConf.lightBox2ReturnToThirdParty"></button>\n' +
    '            <span class="sr-only" ng-bind="modelPopUpConf.button"></span>\n' +
    '        </div>\n' +
    '        <div class="box-lightbox2 ">\n' +
    '            <span class="sr-only" ng-bind="modelPopUpConf.optionLabel2 "></span>\n' +
    '            <p ng-bind="modelPopUpConf.lightBox2IwantToRegisterKeycodeApp"></p>\n' +
    '            <button role="button" class="btn btn-lightbox2 " ng-click="goToModal3()" title="{{openRegisterWindow}}" ng-bind="modelPopUpConf.lightBox2RegisterKeycodeApp">\n' +
    '            </button>\n' +
    '            <span class="sr-only" ng-bind="modelPopUpConf.button"></span>\n' +
    '        </div>\n' +
    '        <div class="box-lightbox2 ">\n' +
    '            <span class="sr-only" ng-bind="modelPopUpConf.optionLabel3 "></span>\n' +
    '            <p ng-bind="modelPopUpConf.lightBox2IhavePreviouslyRegisterKeycodeApp"></p>\n' +
    '            <button role="button" class="btn btn-lightbox2 " ng-click="cancelBtnClicked()" title="{{lightbox2ClosePopUp}}" ng-bind="modelPopUpConf.lightBox2ContinueWithLogin">\n' +
    '            </button>\n' +
    '            <span class="sr-only" ng-bind="modelPopUpConf.button"></span>\n' +
    '        </div>\n' +
    '    \n' +
    '    </div>\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('loginPartials');
} catch (e) {
  module = angular.module('loginPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/views/modal-3.html',
    '<div class="modal-body pop-confirm-body lightbox3" aria-live="polite">\n' +
    '    <button type="button " class="close " aria-hidden="true " ng-click="cancelBtnClicked() "><img alt="{{modelPopUpConf.lightbox3CloseImageAlt}}"  class="close " ng-src ="build/img/closeGREY@2x.png" autofocus></button>\n' +
    '    <div class="confirm-msg">\n' +
    '        <p style="display:none;" ng-bind-html="modelPopUpConf.lightBox3Description1"> </p>\n' +
    '        <p class="text-center-lightbox3" ng-bind-html="modelPopUpConf.lightBox3Description1"></p>\n' +
    '        <p class="text-center-lightbox3" ng-bind-html="modelPopUpConf.lightBox3Description2"></p>\n' +
    '        <p class="text-center-lightbox3" ng-bind="modelPopUpConf.lightBox3Description3"></p>\n' +
    '    </div>\n' +
    '    <div class="modal-footer pop-confirm-footer button-section xs-button-section footer-lightbox3">\n' +
    '\n' +
    '        <button type="button" class="btn btn-lightbox3 btn-left" ng-click="goToModal2()" title="{{backToOption}}" ng-bind="modelPopUpConf.lightBox3BackToOptions">\n' +
    '                </button><span class="sr-only" ng-bind="modelPopUpConf.button"></span>\n' +
    '        <button type="button" class="btn btn-lightbox3" ng-click="goTo365()" title="{{redirectToLogin}}" ng-bind="modelPopUpConf.lightBox3LoginTo365Online">\n' +
    '                </button>\n' +
    '        <span class="sr-only" ng-bind="modelPopUpConf.button"></span>\n' +
    '\n' +
    '    </div>\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('loginPartials');
} catch (e) {
  module = angular.module('loginPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/views/modal.html',
    ' <model-pop-up modal="modelPopUpConf"></model-pop-up> ');
}]);
})();

(function(module) {
try {
  module = angular.module('loginPartials');
} catch (e) {
  module = angular.module('loginPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/views/modalPopUp.html',
    '<div class="content ">\n' +
    '    <div class="content" ui-view="modal" autoscroll="false"></div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('loginPartials');
} catch (e) {
  module = angular.module('loginPartials', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/views/session-out-popup.html',
    '<div class="modal-body pop-confirm-body lightbox1">\n' +
    '  <!--   <button type="button " class="close " aria-hidden="true " ng-click="cancelBtnClicked() "><img class="close " ng-src ="{{close}}"></button>  -->\n' +
    '    <div class="confirm-msg">        \n' +
    '        <p class="text-center" ng-bind="sessionDescText"> </p>\n' +
    '    </div>\n' +
    '\n' +
    '    <div class="modal-footer pop-confirm-footer button-section xs-button-section footer-lightbox1">\n' +
    '        <div class="text-center">\n' +
    '            <button role="button" class="btn btn-lightbox1" ng-click="redirectTPP()" ng-bind="sessionPopupBtnText">\n' +
    '            </button>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>');
}]);
})();
