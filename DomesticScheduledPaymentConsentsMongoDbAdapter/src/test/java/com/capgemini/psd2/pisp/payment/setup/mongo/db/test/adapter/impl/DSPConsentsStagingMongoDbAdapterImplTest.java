package com.capgemini.psd2.pisp.payment.setup.mongo.db.test.adapter.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomDSPConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDSPConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBCashAccountCreditor2;
import com.capgemini.psd2.pisp.domain.OBCharge1;
import com.capgemini.psd2.pisp.domain.OBCharge1Amount;
import com.capgemini.psd2.pisp.domain.OBDomestic1InstructedAmount;
import com.capgemini.psd2.pisp.domain.OBDomesticScheduled1;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticScheduledConsent1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticScheduledConsentResponse1;
import com.capgemini.psd2.pisp.enums.ProcessConsentStatusEnum;
import com.capgemini.psd2.pisp.payment.setup.mongo.db.adapter.impl.DSPConsentsStagingMongoDbAdapterImpl;
import com.capgemini.psd2.pisp.payment.setup.mongo.db.adapter.repository.DSPConsentsFoundationRepository;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.utilities.SandboxValidationUtility;

@RunWith(SpringJUnit4ClassRunner.class)
public class DSPConsentsStagingMongoDbAdapterImplTest {

	@Mock
	private DSPConsentsFoundationRepository repository;

	@Mock
	private SandboxValidationUtility utility;

	@Mock
	private RequestHeaderAttributes reqAttributes;

	@InjectMocks
	private List<OBCharge1> chargesList = new ArrayList<OBCharge1>();

	@InjectMocks
	private DSPConsentsStagingMongoDbAdapterImpl adapter;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testProcessDomesticScheduledPaymentConsents() {
		CustomDSPConsentsPOSTRequest request = new CustomDSPConsentsPOSTRequest();
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();

		request.setData(new OBWriteDataDomesticScheduledConsent1());
		request.getData().setInitiation(new OBDomesticScheduled1());
		request.getData().getInitiation().setInstructedAmount(new OBDomestic1InstructedAmount());
		request.getData().getInitiation().getInstructedAmount().setAmount("745.58");
		request.getData().getInitiation().getInstructedAmount().setCurrency("EUR");
		request.getData().getInitiation().setCreditorAccount(new OBCashAccountCreditor2());
		request.getData().getInitiation().getCreditorAccount().setSecondaryIdentification("SCI");

		OBCharge1 charges = new OBCharge1();
		charges.setAmount(new OBCharge1Amount());
		charges.getAmount().setAmount("1234");
		charges.getAmount().setCurrency("EUR");
		chargesList.add(charges);
		chargesList.set(0, charges);

		CustomDSPConsentsPOSTResponse response = new CustomDSPConsentsPOSTResponse();

		when(utility.getMockedConsentProcessExecStatus(anyString(), anyString()))
				.thenReturn(ProcessConsentStatusEnum.PASS);
		when(utility.isValidAmount(anyString(), anyString())).thenReturn(false);
		when(utility.isValidSecIdentification(anyString())).thenReturn(false);

		Mockito.when(repository.save(any(CustomDSPConsentsPOSTResponse.class))).thenReturn(response);

		adapter.processDomesticScheduledPaymentConsents(request, stageIdentifiers, new HashMap<>(),
				OBExternalConsentStatus1Code.AUTHORISED, OBExternalConsentStatus1Code.REJECTED);
	}

	@Test
	public void testProcessDomesticScheduledPaymentConsentsNullConsntId() {
		CustomDSPConsentsPOSTRequest request = new CustomDSPConsentsPOSTRequest();
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();

		request.setData(new OBWriteDataDomesticScheduledConsent1());
		request.getData().setInitiation(new OBDomesticScheduled1());
		request.getData().getInitiation().setInstructedAmount(new OBDomestic1InstructedAmount());
		request.getData().getInitiation().getInstructedAmount().setAmount("745.58");
		request.getData().getInitiation().getInstructedAmount().setCurrency("EUR");
		request.getData().getInitiation().setCreditorAccount(new OBCashAccountCreditor2());
		request.getData().getInitiation().getCreditorAccount().setSecondaryIdentification("SCI");

		OBCharge1 charges = new OBCharge1();
		charges.setAmount(new OBCharge1Amount());
		charges.getAmount().setAmount("1234");
		charges.getAmount().setCurrency("EUR");
		chargesList.add(charges);
		chargesList.set(0, charges);

		CustomDSPConsentsPOSTResponse response = new CustomDSPConsentsPOSTResponse();

		when(utility.isValidSecIdentification(anyString())).thenReturn(false);

		Mockito.when(repository.save(any(CustomDSPConsentsPOSTResponse.class))).thenReturn(response);

		adapter.processDomesticScheduledPaymentConsents(request, stageIdentifiers, new HashMap<>(),
				OBExternalConsentStatus1Code.AUTHORISED, OBExternalConsentStatus1Code.REJECTED);
	}

	@Test(expected = PSD2Exception.class)
	public void testProcessDomesticScheduledPaymentConsentsValidAmount() {
		CustomDSPConsentsPOSTRequest request = new CustomDSPConsentsPOSTRequest();
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();

		request.setData(new OBWriteDataDomesticScheduledConsent1());
		request.getData().setInitiation(new OBDomesticScheduled1());
		request.getData().getInitiation().setInstructedAmount(new OBDomestic1InstructedAmount());
		request.getData().getInitiation().getInstructedAmount().setAmount("745.58");
		request.getData().getInitiation().getInstructedAmount().setCurrency("EUR");
		request.getData().getInitiation().setCreditorAccount(new OBCashAccountCreditor2());
		request.getData().getInitiation().getCreditorAccount().setSecondaryIdentification("SCI");

		OBCharge1 charges = new OBCharge1();
		charges.setAmount(new OBCharge1Amount());
		charges.getAmount().setAmount("1234");
		charges.getAmount().setCurrency("EUR");
		chargesList.add(charges);
		chargesList.set(0, charges);

		CustomDSPConsentsPOSTResponse response = new CustomDSPConsentsPOSTResponse();

		when(utility.getMockedConsentProcessExecStatus(anyString(), anyString()))
				.thenReturn(ProcessConsentStatusEnum.PASS);
		when(utility.isValidAmount(anyString(), anyString())).thenReturn(true);
		when(utility.isValidSecIdentification(anyString())).thenReturn(false);

		Mockito.when(repository.save(any(CustomDSPConsentsPOSTResponse.class))).thenReturn(response);

		adapter.processDomesticScheduledPaymentConsents(request, stageIdentifiers, new HashMap<>(),
				OBExternalConsentStatus1Code.AUTHORISED, OBExternalConsentStatus1Code.REJECTED);
	}

	@Test(expected = PSD2Exception.class)
	public void testProcessDomesticScheduledPaymentConsentsValidSecIdentification() {
		CustomDSPConsentsPOSTRequest request = new CustomDSPConsentsPOSTRequest();
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();

		request.setData(new OBWriteDataDomesticScheduledConsent1());
		request.getData().setInitiation(new OBDomesticScheduled1());
		request.getData().getInitiation().setInstructedAmount(new OBDomestic1InstructedAmount());
		request.getData().getInitiation().getInstructedAmount().setAmount("745.58");
		request.getData().getInitiation().getInstructedAmount().setCurrency("EUR");
		request.getData().getInitiation().setCreditorAccount(new OBCashAccountCreditor2());
		request.getData().getInitiation().getCreditorAccount().setSecondaryIdentification("SCI");

		OBCharge1 charges = new OBCharge1();
		charges.setAmount(new OBCharge1Amount());
		charges.getAmount().setAmount("1234");
		charges.getAmount().setCurrency("EUR");
		chargesList.add(charges);
		chargesList.set(0, charges);

		CustomDSPConsentsPOSTResponse response = new CustomDSPConsentsPOSTResponse();

		when(utility.getMockedConsentProcessExecStatus(anyString(), anyString()))
				.thenReturn(ProcessConsentStatusEnum.PASS);
		when(utility.isValidAmount(anyString(), anyString())).thenReturn(false);
		when(utility.isValidSecIdentification(anyString())).thenReturn(true);

		Mockito.when(repository.save(any(CustomDSPConsentsPOSTResponse.class))).thenReturn(response);

		adapter.processDomesticScheduledPaymentConsents(request, stageIdentifiers, new HashMap<>(),
				OBExternalConsentStatus1Code.AUTHORISED, OBExternalConsentStatus1Code.REJECTED);
	}

	@Test(expected = PSD2Exception.class)
	public void testProcessDomesticScheduledPaymentConsentsDataAccessFailureException() {
		CustomDSPConsentsPOSTRequest request = new CustomDSPConsentsPOSTRequest();
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();

		request.setData(new OBWriteDataDomesticScheduledConsent1());
		request.getData().setInitiation(new OBDomesticScheduled1());
		request.getData().getInitiation().setInstructedAmount(new OBDomestic1InstructedAmount());
		request.getData().getInitiation().getInstructedAmount().setAmount("745.58");
		request.getData().getInitiation().getInstructedAmount().setCurrency("EUR");
		request.getData().getInitiation().setCreditorAccount(new OBCashAccountCreditor2());
		request.getData().getInitiation().getCreditorAccount().setSecondaryIdentification("SCI");

		OBCharge1 charges = new OBCharge1();
		charges.setAmount(new OBCharge1Amount());
		charges.getAmount().setAmount("1234");
		charges.getAmount().setCurrency("EUR");
		chargesList.add(charges);
		chargesList.set(0, charges);

		when(utility.getMockedConsentProcessExecStatus(anyString(), anyString()))
				.thenReturn(ProcessConsentStatusEnum.FAIL);
		when(utility.isValidAmount(anyString(), anyString())).thenReturn(false);
		when(utility.isValidSecIdentification(anyString())).thenReturn(false);

		Mockito.when(repository.save(any(CustomDSPConsentsPOSTResponse.class)))
				.thenThrow(new DataAccessResourceFailureException("Test"));

		adapter.processDomesticScheduledPaymentConsents(request, stageIdentifiers, new HashMap<>(),
				OBExternalConsentStatus1Code.AUTHORISED, OBExternalConsentStatus1Code.REJECTED);
	}

	@Test
	public void testRetrieveStagedDomesticScheduledPaymentConsents() {
		CustomDSPConsentsPOSTResponse response = new CustomDSPConsentsPOSTResponse();
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();

		response.setData(new OBWriteDataDomesticScheduledConsentResponse1());
		response.getData().setInitiation(new OBDomesticScheduled1());
		response.getData().getInitiation().setInstructedAmount(new OBDomestic1InstructedAmount());
		response.getData().getInitiation().getInstructedAmount().setAmount("1436.254");

		when(reqAttributes.getMethodType()).thenReturn("GET");
		when(utility.isValidAmount(anyString(), anyString())).thenReturn(false);
		when(repository.findOneByDataConsentId(anyString())).thenReturn(response);

		adapter.retrieveStagedDomesticScheduledPaymentConsents(stageIdentifiers, new HashMap<>());
	}

	@Test(expected = PSD2Exception.class)
	public void testRetrieveStagedDomesticScheduledPaymentConsentsException() {
		CustomDSPConsentsPOSTResponse response = new CustomDSPConsentsPOSTResponse();
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();

		response.setData(new OBWriteDataDomesticScheduledConsentResponse1());
		response.getData().setInitiation(new OBDomesticScheduled1());
		response.getData().getInitiation().setInstructedAmount(new OBDomestic1InstructedAmount());
		response.getData().getInitiation().getInstructedAmount().setAmount("1436.254");

		when(reqAttributes.getMethodType()).thenReturn("GET");
		when(utility.isValidAmount(anyString(), anyString())).thenReturn(true);
		when(repository.findOneByDataConsentId(anyString())).thenThrow(new DataAccessResourceFailureException("Test"));

		adapter.retrieveStagedDomesticScheduledPaymentConsents(stageIdentifiers, new HashMap<>());
	}

	@After
	public void tearDown() {
		utility = null;
		reqAttributes = null;
		repository = null;
		adapter = null;
	}

}