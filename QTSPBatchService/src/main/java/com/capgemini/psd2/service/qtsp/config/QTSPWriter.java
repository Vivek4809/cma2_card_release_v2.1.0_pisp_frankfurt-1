package com.capgemini.psd2.service.qtsp.config;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import com.capgemini.psd2.model.qtsp.QTSPResource;
import com.capgemini.psd2.service.qtsp.integration.AWSSNSAdapter;
import com.capgemini.psd2.service.qtsp.integration.MongoDbAdapter;
import com.capgemini.psd2.service.qtsp.integration.NetScalerAdapter;
import com.capgemini.psd2.service.qtsp.integration.PingFederateAdapter;
import com.capgemini.psd2.service.qtsp.utility.QTSPServiceUtility;

public class QTSPWriter implements ItemWriter<Map<String, List<QTSPResource>>> {

	private static final Logger LOGGER = LoggerFactory.getLogger(QTSPWriter.class);

	@Autowired
	private QTSPServiceConfig qtspServiceConfig;

	@Autowired
	private MongoDbAdapter qtspServiceMongoDbAdapter;

	@Autowired
	private PingFederateAdapter pingFederateAdapter;

	@Autowired
	private NetScalerAdapter netScalerAdapter;

	@Autowired
	private AWSSNSAdapter awsSNSAdapter;

	@Autowired
	private QTSPServiceUtility utility;

	@Override
	public void write(List<? extends Map<String, List<QTSPResource>>> updatedQTSPs) throws Exception {
		
		LOGGER.info("Starting to write QTSPs");

		if (qtspServiceConfig.isWriteToDB())
			writeToDB(updatedQTSPs.get(0));

		if (qtspServiceConfig.isWriteToPingFederate())
			writeToPingFederate(updatedQTSPs.get(0));

		if (qtspServiceConfig.isWriteToNetScaler())
			writeToNetScaler(updatedQTSPs);
		
		if (!utility.getFailures().isEmpty()) {
			LOGGER.info("Removing failed cert uploads from db");
			removeFailedCerts();
		}

		if (qtspServiceConfig.isPublishToAWSSNS() && !utility.getFailures().isEmpty()) {
			LOGGER.info("Publishing failures to AWS SNS");
			awsSNSAdapter.publishFailures(utility.getFailures());
		}
	}

	private void writeToDB(Map<String, List<QTSPResource>> updatedQTSPs) {
		if (null != updatedQTSPs.get(QTSPServiceUtility.NEW_QTSPS)
				&& !updatedQTSPs.get(QTSPServiceUtility.NEW_QTSPS).isEmpty()) {
			LOGGER.info("Writing new QTSPs to MongoDB");
			qtspServiceMongoDbAdapter.saveQTSPs(updatedQTSPs.get(QTSPServiceUtility.NEW_QTSPS));
		}
		if (null != updatedQTSPs.get(QTSPServiceUtility.MODIFIED_QTSPS)
				&& !updatedQTSPs.get(QTSPServiceUtility.MODIFIED_QTSPS).isEmpty()) {
			LOGGER.info("Updating modified QTSPs to MongoDB");
			qtspServiceMongoDbAdapter.saveQTSPs(updatedQTSPs.get(QTSPServiceUtility.MODIFIED_QTSPS));
		}
	}

	private void writeToPingFederate(Map<String, List<QTSPResource>> updatedQTSPs) {
		if (null != updatedQTSPs.get(QTSPServiceUtility.NEW_QTSPS)
				&& !updatedQTSPs.get(QTSPServiceUtility.NEW_QTSPS).isEmpty()) {
			LOGGER.info("Writing new QTSPs to PingFederate");
			pingFederateAdapter.importCAInPF(updatedQTSPs.get(QTSPServiceUtility.NEW_QTSPS));
		}
		if (null != updatedQTSPs.get(QTSPServiceUtility.MODIFIED_QTSPS)
				&& !updatedQTSPs.get(QTSPServiceUtility.MODIFIED_QTSPS).isEmpty()) {
			LOGGER.info("Removing withdrawn QTSPs to PingFederate");
			pingFederateAdapter.removeCAFromPF(updatedQTSPs.get(QTSPServiceUtility.MODIFIED_QTSPS));
		}
	}

	private void writeToNetScaler(List<? extends Map<String, List<QTSPResource>>> updatedQTSPs) {
		if ((null != updatedQTSPs.get(0).get(QTSPServiceUtility.NEW_QTSPS)
				&& !updatedQTSPs.get(0).get(QTSPServiceUtility.NEW_QTSPS).isEmpty())
				|| (null != updatedQTSPs.get(0).get(QTSPServiceUtility.MODIFIED_QTSPS)
						&& !updatedQTSPs.get(0).get(QTSPServiceUtility.MODIFIED_QTSPS).isEmpty())) {
			LOGGER.info("Writing updated QTSPs to NetScaler");
			netScalerAdapter.processCert(updatedQTSPs);
		}
	}
	
	private void removeFailedCerts() {
		utility.getFailures().forEach(f -> {
			if (f.getFailureStage().equals(QTSPServiceUtility.PF_FAILURE_STAGE_ADD)
					|| f.getFailureStage().equals(QTSPServiceUtility.NS_FAILURE_STAGE_ADD))
				qtspServiceMongoDbAdapter.removeQTSP(f.getQtspResource().getId());
		});
	}

}