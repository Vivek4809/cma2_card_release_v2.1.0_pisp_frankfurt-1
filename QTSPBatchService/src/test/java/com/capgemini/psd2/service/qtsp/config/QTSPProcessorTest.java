package com.capgemini.psd2.service.qtsp.config;

import java.util.HashMap;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.util.Assert;

import com.capgemini.psd2.model.qtsp.QTSPResource;
import com.capgemini.psd2.service.qtsp.QTSPService;
import com.capgemini.psd2.service.qtsp.stub.QTSPStub;

public class QTSPProcessorTest {

	@InjectMocks
	private QTSPProcessor qtspProcessor;

	@Mock
	private QTSPService qtspService;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void processTest() {
		List<QTSPResource> qtspResponses = QTSPStub.getCerts();
		try {
			Assert.isInstanceOf(HashMap.class, qtspProcessor.process(qtspResponses));
		} catch (Exception e) {
		}
	}
}

