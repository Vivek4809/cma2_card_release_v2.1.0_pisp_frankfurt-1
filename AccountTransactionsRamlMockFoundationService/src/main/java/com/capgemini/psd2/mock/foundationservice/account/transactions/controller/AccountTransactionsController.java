package com.capgemini.psd2.mock.foundationservice.account.transactions.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.foundationservice.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.foundationservice.exceptions.MockFoundationServiceException;
import com.capgemini.psd2.foundationservice.validator.ValidationUtility;
import com.capgemini.psd2.mock.foundationservice.account.transactions.raml.domain.TransactionList;
import com.capgemini.psd2.mock.foundationservice.account.transactions.service.AccountTransactionsService;

@RestController
@RequestMapping("/exchange/dd9dfe2a-e2c9-4bb7-8357-3902557f1bf9/account-process-api/1.0.17/core-banking/p/abt")
public class AccountTransactionsController {

	@Autowired
	private AccountTransactionsService accountTransactionsService;
	
	@Autowired
	private ValidationUtility validatorUtility;
	
	@RequestMapping(value = "/{version}/accounts/{parent-national-sort-code-nsc-number}/{account-number}/transactions", method = RequestMethod.GET, produces = "application/json")
	public TransactionList reteriveAccountTransaction(
			@PathVariable("version") String version,
			@PathVariable("parent-national-sort-code-nsc-number") String accountclassifier,
			@PathVariable("account-number") String accountId,
			@RequestParam(required = false, value="dateFrom") String startDate,
			@RequestParam(required = false, value="dateTo") String endDate,
			@RequestParam(required = false, value="creditDebitEventCode") String txnType,
			@RequestParam(required = false, value="pageNumber") String pageNumber,
			@RequestParam(required = false, value="pageSize") String pageSize,
			@RequestParam(required = false, value="orderBy") String orderBy,
			@RequestParam(required = false, value="order") String order,
			@RequestHeader(required = false, value = "X-API-CHANNEL-CODE") String boiChannel,
			@RequestHeader(required = false, value = "X-API-SOURCE-USER") String boiUser,
			@RequestHeader(required = false, value = "X-API-TRANSACTION-ID") String boiTransaction,
			@RequestHeader(required = false, value = "X-API-CORRELATION-ID") String correlationID,
			@RequestHeader(required = false, value = "X-API-SOURCE-SYSTEM") String sourceSystem) throws Exception {
		
		validatorUtility.validateErrorCode(boiTransaction);
		
		if (boiUser == null || boiUser.isEmpty() || boiChannel == null || boiChannel.isEmpty() || boiTransaction == null
				|| boiTransaction.isEmpty() || sourceSystem == null || sourceSystem.isEmpty()) {
			throw MockFoundationServiceException.populateMockFoundationServiceException(ErrorCodeEnum.BAD_REQUEST_PAC_TXN);
		}
		
		if (accountclassifier == null || accountclassifier.isEmpty() || accountId == null || accountId.isEmpty()) {
			throw MockFoundationServiceException.populateMockFoundationServiceException(ErrorCodeEnum.BAD_REQUEST_PAC_TXN);
		}
		
		return accountTransactionsService.retrieveMuleJsonTransactions(accountclassifier,accountId, startDate, endDate, txnType, pageNumber, pageSize);
		
	}
	
	@RequestMapping(value = "/{version}/accounts/creditcards/{customer-reference}/{account-number}/transactions", method = RequestMethod.GET, produces = "application/json")
	public TransactionList reteriveCreditCardTransaction(
			@PathVariable("version") String version,
			@PathVariable("customer-reference") String customerReference,
			@PathVariable("account-number") String accountId,
			@RequestParam(required = false, value="dateFrom") String startDate,
			@RequestParam(required = false, value="dateTo") String endDate,
			@RequestParam(required = false, value="creditDebitEventCode") String txnType,
			/*@RequestParam(required = false, value="pageSize") String pageSize,
			@RequestParam(required = false, value="pageNumber") String pageNumber,*/
			@RequestParam(required = false, value="orderBy") String orderBy,
			@RequestParam(required = false, value="order") String order,
			@RequestHeader(required = false, value = "x-masked-pan") String maskedPan,
			@RequestHeader(required = false, value = "x-api-channel-Code") String boiChannel,
			@RequestHeader(required = false, value = "x-api-source-user") String boiUser,
			@RequestHeader(required = false, value = "x-api-transaction-id") String boiTransaction,
			@RequestHeader(required = false, value = "x-api-correlation-id") String correlationID,
			@RequestHeader(required = false, value = "x-api-source-system") String sourceSystem) throws Exception {
		
		validatorUtility.validateErrorCode(boiTransaction);
		
		if (maskedPan == null || maskedPan.isEmpty() || boiUser == null || boiUser.isEmpty() || boiChannel == null || boiChannel.isEmpty() || boiTransaction == null
				|| boiTransaction.isEmpty() || sourceSystem == null || sourceSystem.isEmpty() ) {
			throw MockFoundationServiceException.populateMockFoundationServiceException(ErrorCodeEnum.BAD_REQUEST_PAC_TXN);
		}

		if (customerReference == null || customerReference.isEmpty() || accountId == null || accountId.isEmpty()) {
			throw MockFoundationServiceException.populateMockFoundationServiceException(ErrorCodeEnum.BAD_REQUEST_PAC_TXN);
		}

		return accountTransactionsService.reteriveCreditCardTransaction(customerReference, accountId, startDate, endDate, txnType);
		
	}
}
