/*************************************************************************
* 
 * CAPGEMINI CONFIDENTIAL
* __________________
* 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
* 
 * NOTICE:  All information contained herein is, and remains
* the property of CAPGEMINI GROUP.
* The intellectual and technical concepts contained herein
* are proprietary to CAPGEMINI GROUP and may be covered
* by patents, patents in process, and are protected by trade secret
* or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from CAPGEMINI GROUP.
*//*

package com.capgemini.psd2.security.consent.test.application;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.web.accept.ContentNegotiationManagerFactoryBean;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.ContentNegotiatingViewResolver;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import com.capgemini.psd2.security.authentication.handlers.JwtAuthenticationProvider;
import com.capgemini.psd2.security.consent.filters.JwtTokenAuthenticationProcessingFilter;
import com.capgemini.psd2.security.filters.PSD2SecurityFilter;

*//**
 * WebSecurityConfig
 *//*
@Configuration("PSD2SecurityConfig")
@EnableWebSecurity
@ComponentScan(basePackages = { "com.capgemini.psd2" })
//@DependsOn("webMVC")
public class WebSecurityConfigTest extends WebSecurityConfigurerAdapter {
    
    @Autowired private AuthenticationFailureHandler failureHandler;
    @Autowired private JwtAuthenticationProvider jwtAuthenticationProvider;
    @Autowired private AuthenticationManager authenticationManager;
    @Autowired private PSD2SecurityFilter consentFilter;
	@Value("${app.saas.security.tokenSigningKey}") 
	private String tokenSigningKey;

	@Bean    
    protected JwtTokenAuthenticationProcessingFilter buildJwtTokenAuthenticationProcessingFilter() throws Exception {
		RequestMatcher antReqMatch = new AntPathRequestMatcher("/customerconsentview","GET");
        JwtTokenAuthenticationProcessingFilter filter 
            = new JwtTokenAuthenticationProcessingFilter(failureHandler, antReqMatch,tokenSigningKey);
        filter.setAuthenticationManager(this.authenticationManager);
        return filter;
    }
    
	@Bean
	public FilterRegistrationBean jwtFilter() throws Exception{
		FilterRegistrationBean filterRegBean = new FilterRegistrationBean();
		filterRegBean.setFilter(buildJwtTokenAuthenticationProcessingFilter());
	    filterRegBean.setEnabled(false);
		return filterRegBean;
	}	
	
	
	@Bean
	public ContentNegotiatingViewResolver contentViewResolver() throws Exception {
		ContentNegotiationManagerFactoryBean contentNegotiationManager = new ContentNegotiationManagerFactoryBean();
		contentNegotiationManager.addMediaType("json", MediaType.APPLICATION_JSON);

		MappingJackson2JsonView defaultView = new MappingJackson2JsonView();
		defaultView.setExtractValueFromSingleKeyModel(true);

		ContentNegotiatingViewResolver contentViewResolver = new ContentNegotiatingViewResolver();
		contentViewResolver.setContentNegotiationManager(contentNegotiationManager.getObject());
		contentViewResolver.setDefaultViews(Arrays.<View>asList(defaultView));
		return contentViewResolver;
	}
	
	
	
    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
    
    @Override
    protected void configure(AuthenticationManagerBuilder auth) {
        auth.authenticationProvider(jwtAuthenticationProvider);
    }
    
    @Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/static/**","/localization/**","/img/**","/fonts/**","/WEB-INF/**","/errors");
	}
    
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().antMatchers("/restart").permitAll();
		http.addFilterBefore(consentFilter, AbstractPreAuthenticatedProcessingFilter.class);
		http
        .csrf().disable() // We don't need CSRF for JWT based authentication
        .authorizeRequests()
            .antMatchers("/customerconsentview").permitAll().antMatchers("/errors").permitAll().antMatchers("/index.jsp").permitAll() // Protected API End-points        
         .and().addFilterBefore(buildJwtTokenAuthenticationProcessingFilter(),
        		UsernamePasswordAuthenticationFilter.class);
	}    
}*/