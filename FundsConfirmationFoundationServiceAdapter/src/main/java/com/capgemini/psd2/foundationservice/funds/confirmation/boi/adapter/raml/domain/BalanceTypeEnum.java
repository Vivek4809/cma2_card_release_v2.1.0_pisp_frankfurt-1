package com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.raml.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Gets or Sets BalanceTypeEnum
 */
public enum BalanceTypeEnum {
	  
	  LEDGER_BALANCE("Ledger Balance"),
	  
	  SHADOW_BALANCE("Shadow Balance"),
	  
	  STATEMENT_CLOSING_BALANCE("Statement Closing Balance"),
	  
	  AVAILABLE_LIMIT("Available Limit");

	  private String value;

	  BalanceTypeEnum(String value) {
	    this.value = value;
	  }

	  @Override
	  @JsonValue
	  public String toString() {
	    return String.valueOf(value);
	  }

	  @JsonCreator
	  public static BalanceTypeEnum fromValue(String text) {
	    for (BalanceTypeEnum b : BalanceTypeEnum.values()) {
	      if (String.valueOf(b.value).equals(text)) {
	        return b;
	      }
	    }
	    return null;
	  }
	}

