package com.capgemini.psd2.integration.service;

public interface CPNPOperationsService {

	public void verifyClient(String tppId, String psuId);

}
