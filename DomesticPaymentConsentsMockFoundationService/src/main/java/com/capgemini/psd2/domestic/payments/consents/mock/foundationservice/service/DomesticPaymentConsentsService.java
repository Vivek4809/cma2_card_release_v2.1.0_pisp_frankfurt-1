/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.domestic.payments.consents.mock.foundationservice.service;


import com.capgemini.psd2.domestic.payments.consents.mock.foundationservice.exception.handler.RecordNotFoundException;
import com.capgemini.psd2.foundationservice.pisp1.domain.PaymentInstruction;
import com.capgemini.psd2.foundationservice.pisp3.domain.PaymentInstructionProposal;


/**
 * The Interface DomesticPaymentConsentsService.
 */
public interface DomesticPaymentConsentsService {

	/**
	 * Retrieve Domestic Payment Consents.
	 *
	 * @param nsc the nsc
	 * @param accountNumber the account number
	 * @return the accounts
	 * @throws Exception the exception
	 */
	public PaymentInstructionProposal   retrieveAccountInformation(String paymentInstructionProposalId) throws Exception;

	public PaymentInstructionProposal createDomesticPaymentConsentsResource(PaymentInstructionProposal paymentInstProposalReq) throws RecordNotFoundException;

	public PaymentInstructionProposal validateDomesticPaymentConsentsResource(PaymentInstructionProposal paymentInstProposalReq) throws RecordNotFoundException;
	
	public PaymentInstructionProposal updateDomesticPaymentConsentsResource(String paymentInstructionProposalId,PaymentInstructionProposal paymentInstProposalReq) throws Exception;
	
	public PaymentInstruction retrievePayment(String paymentId) throws Exception;
	
}
