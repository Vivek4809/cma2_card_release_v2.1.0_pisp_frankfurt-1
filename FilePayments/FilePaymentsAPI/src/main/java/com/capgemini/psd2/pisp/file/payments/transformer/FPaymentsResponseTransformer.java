package com.capgemini.psd2.pisp.file.payments.transformer;

import java.util.Map;

import com.capgemini.psd2.pisp.domain.CustomFilePaymentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.PaymentFileSubmitPOST201Response;
import com.capgemini.psd2.pisp.domain.PaymentsPlatformResource;

public interface FPaymentsResponseTransformer {

	public PaymentFileSubmitPOST201Response filePaymentsResponseTransformer(
			PaymentsPlatformResource paymentsPlatformResponse,
			CustomFilePaymentsPOSTResponse fileConsentsTppResponse, String methodType);

	public PaymentFileSubmitPOST201Response paymentSubmissionResponseTransformer(
			PaymentFileSubmitPOST201Response paymentSubmissionResponse, Map<String, Object> paymentsPlatformResourceMap,
			String methodType);
}
