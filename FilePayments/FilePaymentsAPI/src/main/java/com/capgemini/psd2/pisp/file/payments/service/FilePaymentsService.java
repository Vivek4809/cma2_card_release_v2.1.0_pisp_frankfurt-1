package com.capgemini.psd2.pisp.file.payments.service;

import com.capgemini.psd2.pisp.domain.CustomFilePaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.PaymentFileSubmitPOST201Response;
import com.capgemini.psd2.pisp.domain.PlatformFilePaymentsFileResponse;

public interface FilePaymentsService {

	public PlatformFilePaymentsFileResponse downloadTransactionFileByConsentId(String consentId);

	public PaymentFileSubmitPOST201Response createFilePaymentsResource(CustomFilePaymentsPOSTRequest paymentRequest, boolean isPaymentValidated);

	public PaymentFileSubmitPOST201Response findStatusFilePaymentsResource(String paymentId);

}
