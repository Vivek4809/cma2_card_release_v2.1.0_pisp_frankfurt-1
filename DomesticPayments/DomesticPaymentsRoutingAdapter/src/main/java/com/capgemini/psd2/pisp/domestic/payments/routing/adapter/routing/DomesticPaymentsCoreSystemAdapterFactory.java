
package com.capgemini.psd2.pisp.domestic.payments.routing.adapter.routing;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.pisp.adapter.DomesticPaymentsAdapter;

@Component
public class DomesticPaymentsCoreSystemAdapterFactory implements ApplicationContextAware, DomesticPaymentsAdapterFactory {

	/** The application context. */
	private ApplicationContext applicationContext;
		
	@Override
	public DomesticPaymentsAdapter getDomesticPaymentsAdapterInstance(String adapterName) {
		return (DomesticPaymentsAdapter) applicationContext.getBean(adapterName);		
	}
		
	@Override
	public void setApplicationContext(ApplicationContext context) {
		  this.applicationContext = context;
	}
	
}
