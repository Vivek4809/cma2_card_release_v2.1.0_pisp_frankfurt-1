package com.capgemini.psd2.adapter.datetime.utility;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.datatype.XMLGregorianCalendar;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class CustomDateSerializer extends JsonSerializer<XMLGregorianCalendar> {

	private static final String DATE_TIME_FORMAT = "yyyy-MM-dd";

	@Override
	public void serialize(XMLGregorianCalendar fsDate, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {

		GregorianCalendar gCalendar = fsDate.toGregorianCalendar();
		DateFormat dateFormat = new SimpleDateFormat(DATE_TIME_FORMAT);
		Date date = gCalendar.getTime();
		String dateString = dateFormat.format(date);
		jsonGenerator.writeString(dateString);

	}
}
