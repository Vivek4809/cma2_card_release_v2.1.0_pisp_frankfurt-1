package com.capgemini.psd2.adapter.utility;

import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

@EnableConfigurationProperties
@ConfigurationProperties("foundationService")
@Component
public class AdapterUtility {

	private Map<String, String> foundationUrl = new HashMap<>();

	private Map<String, String> foundationCustProfileUrlAISP = new HashMap<>();
	
	private Map<String, String> foundationCustProfileUrlPISP = new HashMap<>();

	private Map<String, String> throwableError = new HashMap<>();
	
	private Map<String, String> foundationBaseRamlURL = new HashMap<>();
	
	private Map<String, String> uiErrorMap = new HashMap<>();	
	
	public Map<String, String> getUiErrorMap() {
		return uiErrorMap;
	}

	public void setUiErrorMap(Map<String, String> uiErrorMap) {
		this.uiErrorMap = uiErrorMap;
	}

	public String retriveFoundationServiceRamlURL(String channelId) {
		return foundationBaseRamlURL.get(channelId);
	}

	public String retriveFoundationServiceURL(String channelId) {
		return foundationUrl.get(channelId);
	}

	public Map<String, String> getFoundationBaseRamlURL() {
		return foundationBaseRamlURL;
	}

	public void setFoundationBaseRamlURL(Map<String, String> foundationBaseRamlURL) {
		this.foundationBaseRamlURL = foundationBaseRamlURL;
	}

	public String retriveCustProfileFoundationServiceURLAISP(String channelId) {
		return foundationCustProfileUrlAISP.get(channelId);
	}
	
	public String retriveCustProfileFoundationServiceURLPISP(String channelId) {
		return foundationCustProfileUrlPISP.get(channelId);
	}

	public Map<String, String> getThrowableError() {
		return throwableError;
	}

	public void setThrowableError(Map<String, String> throwableError) {
		this.throwableError = throwableError;
	}

	public Map<String, String> getFoundationUrl() {
		return foundationUrl;
	}

	public void setFoundationUrl(Map<String, String> foundationUrl) {
		this.foundationUrl = foundationUrl;
	}

	public Map<String, String> getFoundationCustProfileUrlAISP() {
		return foundationCustProfileUrlAISP;
	}

	public void setFoundationCustProfileUrlAISP(Map<String, String> foundationCustProfileUrl) {
		this.foundationCustProfileUrlAISP = foundationCustProfileUrl;
	}
	
	public Map<String, String> getFoundationCustProfileUrlPISP() {
		return foundationCustProfileUrlPISP;
	}

	public void setFoundationCustProfileUrlPISP(Map<String, String> foundationCustProfileUrl) {
		this.foundationCustProfileUrlPISP = foundationCustProfileUrl;
	}


}
