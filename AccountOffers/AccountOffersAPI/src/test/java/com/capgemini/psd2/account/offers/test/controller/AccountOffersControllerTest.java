package com.capgemini.psd2.account.offers.test.controller;

import static org.mockito.Matchers.anyString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.capgemini.psd2.account.offers.controller.AccountOffersController;
import com.capgemini.psd2.account.offers.service.impl.AccountOffersServiceImpl;
import com.capgemini.psd2.aisp.domain.OBReadOffer1;
import com.capgemini.psd2.exceptions.OBPSD2Exception;
import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.exceptions.PSD2Exception;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountOffersControllerTest {

	@Mock
	private AccountOffersServiceImpl service;

	/** The mock mvc. */
	private MockMvc mockMvc;

	@InjectMocks
	private AccountOffersController controller;

	OBReadOffer1 offersGETResponse = null;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(controller).dispatchOptions(true).build();

		Map<String, String> map=new HashMap<>();
		map.put("SIGNATURE", "Something wrong with x-jws-signature request header");
		Map<String, String> specificErrorMessageMap=new HashMap<>();
		specificErrorMessageMap.put("NO_ACCOUNT_ID_FOUND", "signature_invalid_content");
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(map);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessageMap); 
	}

	/**
	 * Test retrieve account offers success flow.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@Test
	public void testRetrieveAccountOffersSuccessFlow() throws Exception {

		Mockito.when(service.retrieveAccountOffers(anyString()))
				.thenReturn(offersGETResponse);

		this.mockMvc.perform(get("/accounts/{accountId}/offers", "d9d9c3dc-1427-4f03-92a7-7916fcf4cc0d")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}

	
	@After
	public void tearDown() throws Exception {
		controller = null;
	}
	
	

}
