package com.capgemini.psd2.account.offers.routing.adapter.test.impl;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.capgemini.psd2.account.offers.routing.adapter.impl.AccountOffersRoutingAdapter;
import com.capgemini.psd2.account.offers.routing.adapter.routing.AccountOffersAdapterFactory;
import com.capgemini.psd2.account.offers.routing.adapter.test.adapter.AccountOffersTestRoutingAdapter;
import com.capgemini.psd2.aisp.adapter.AccountOffersAdapter;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountOffersResponse;
import com.capgemini.psd2.logger.RequestHeaderAttributes;

public class AccountOffersRoutingAdapterTest {

	@Mock
	private AccountOffersAdapterFactory accountOffersAdapterFactory;
	
	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;

	@InjectMocks
	private AccountOffersAdapter accountOffersRoutingAdapter = new AccountOffersRoutingAdapter();

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void retrieveAccountOffersTest() {
		AccountOffersAdapter accountOffersAdapter = new AccountOffersTestRoutingAdapter();
		Mockito.when(accountOffersAdapterFactory.getAdapterInstance(anyString())).thenReturn(accountOffersAdapter);

		PlatformAccountOffersResponse offersGETResponse = accountOffersRoutingAdapter.retrieveAccountOffers(null, null);

		assertTrue(offersGETResponse.getoBReadOffer1().getData().getOffer().isEmpty());

	}

}
