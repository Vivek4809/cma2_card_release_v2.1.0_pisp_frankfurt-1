package com.capgemini.psd2.account.offers.routing.adapter.test.routing;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.context.ApplicationContext;

import com.capgemini.psd2.account.offers.routing.adapter.impl.AccountOffersRoutingAdapter;
import com.capgemini.psd2.account.offers.routing.adapter.routing.AccountOffersCoreSystemAdapterFactory;
import com.capgemini.psd2.account.offers.routing.adapter.test.adapter.AccountOffersTestRoutingAdapter;
import com.capgemini.psd2.aisp.adapter.AccountOffersAdapter;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountOffersResponse;
import com.capgemini.psd2.consent.domain.AccountMapping;

public class AccountOffersAdapterFactoryTest {
	
	
	@Mock
	private ApplicationContext applicationContext;
	
	@InjectMocks
	private AccountOffersCoreSystemAdapterFactory accountOffersCoreSystemAdapterFactory;
	
	@Before
	public void setUp () throws Exception
	{
		MockitoAnnotations.initMocks(this);	
	}
	
	@Test
	public void testAccountOffersAdapter()
	{
		AccountOffersAdapter accountOffersAdapter =  new AccountOffersRoutingAdapter();
		when(applicationContext.getBean(anyString())).thenReturn(accountOffersAdapter);
		AccountOffersAdapter accountOffersAdapterResult = (AccountOffersRoutingAdapter)accountOffersCoreSystemAdapterFactory.getAdapterInstance("accountOffersAdapter");
		assertEquals(accountOffersAdapter, accountOffersAdapterResult);
				
		
	}

}
