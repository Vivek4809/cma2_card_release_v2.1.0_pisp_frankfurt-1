package com.capgemini.psd2.account.offers.routing.adapter.routing;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.adapter.AccountOffersAdapter;

@Component
public class AccountOffersCoreSystemAdapterFactory implements ApplicationContextAware, AccountOffersAdapterFactory {

	private ApplicationContext applicationContext;

	@Override
	public AccountOffersAdapter getAdapterInstance(String adapterName) {
		
		return (AccountOffersAdapter) applicationContext.getBean(adapterName);
	}

	@Override
	public void setApplicationContext(ApplicationContext context) {

		this.applicationContext = context;

	}

}
