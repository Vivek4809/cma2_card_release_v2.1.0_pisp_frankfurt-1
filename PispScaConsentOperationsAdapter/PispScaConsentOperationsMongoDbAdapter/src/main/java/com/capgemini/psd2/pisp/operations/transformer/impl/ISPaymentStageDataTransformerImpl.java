package com.capgemini.psd2.pisp.operations.transformer.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Conditional;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.conditions.MongoDbMockCondition;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.domain.CustomISPConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;
import com.capgemini.psd2.pisp.operations.transformer.PaymentStageDataTransformer;
import com.capgemini.psd2.pisp.payment.setup.mongo.db.adapter.repository.ISPConsentsFoundationRepository;
import com.capgemini.psd2.pisp.stage.domain.AmountDetails;
import com.capgemini.psd2.pisp.stage.domain.ChargeDetails;
import com.capgemini.psd2.pisp.stage.domain.CreditorDetails;
import com.capgemini.psd2.pisp.stage.domain.CustomConsentAppViewData;
import com.capgemini.psd2.pisp.stage.domain.CustomDebtorDetails;
import com.capgemini.psd2.pisp.stage.domain.CustomFraudSystemPaymentData;
import com.capgemini.psd2.pisp.stage.domain.CustomISPData;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageUpdateData;
import com.capgemini.psd2.pisp.stage.domain.ExchangeRateDetails;
import com.capgemini.psd2.pisp.stage.domain.RemittanceDetails;
import com.capgemini.psd2.pisp.status.PaymentConstants;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Conditional(MongoDbMockCondition.class)
@Component("isPaymentStageDataTransformer")
public class ISPaymentStageDataTransformerImpl implements PaymentStageDataTransformer<CustomISPConsentsPOSTResponse> {

	@Autowired
	private ISPConsentsFoundationRepository iScheduledPaymentConsentsBankRepository;

	@Override
	public CustomConsentAppViewData transformDStageToConsentAppViewData(CustomISPConsentsPOSTResponse stageResponse) {
		CustomConsentAppViewData response = new CustomConsentAppViewData();

		/* Payment Type needed on consent page */
		response.setPaymentType(PaymentTypeEnum.INTERNATIONAL_SCH_PAY.getPaymentType());

		/* Amount details needed on consent page */
		AmountDetails amountDetails = new AmountDetails();
		if (!NullCheckUtils.isNullOrEmpty(stageResponse.getData().getInitiation().getInstructedAmount())) {
			amountDetails.setAmount(stageResponse.getData().getInitiation().getInstructedAmount().getAmount());
			amountDetails.setCurrency(stageResponse.getData().getInitiation().getInstructedAmount().getCurrency());
		}

		/* Debtor details needed on consent page */
		CustomDebtorDetails debtorDetails = null;
		if (!NullCheckUtils.isNullOrEmpty(stageResponse.getData().getInitiation().getDebtorAccount())) {
			debtorDetails = new CustomDebtorDetails();
			debtorDetails
					.setIdentification(stageResponse.getData().getInitiation().getDebtorAccount().getIdentification());
			debtorDetails.setName(stageResponse.getData().getInitiation().getDebtorAccount().getName());
			String stagedDebtorSchemeName = stageResponse.getData().getInitiation().getDebtorAccount().getSchemeName();

			debtorDetails.setSchemeName(stagedDebtorSchemeName);
			debtorDetails.setSecondaryIdentification(
					stageResponse.getData().getInitiation().getDebtorAccount().getSecondaryIdentification());
		}

		/* Creditor details needed on consent page */
		CreditorDetails creditorDetails = new CreditorDetails();
		if (!NullCheckUtils.isNullOrEmpty(stageResponse.getData().getInitiation().getCreditorAccount())) {
			creditorDetails.setIdentification(
					stageResponse.getData().getInitiation().getCreditorAccount().getIdentification());
			creditorDetails.setName(stageResponse.getData().getInitiation().getCreditorAccount().getName());
			creditorDetails.setSchemeName(stageResponse.getData().getInitiation().getCreditorAccount().getSchemeName());
			creditorDetails.setSecondaryIdentification(
					stageResponse.getData().getInitiation().getCreditorAccount().getSecondaryIdentification());
		}

		/* Remittance details needed on consent page */
		RemittanceDetails remittanceDetails = new RemittanceDetails();
		if (!NullCheckUtils.isNullOrEmpty(stageResponse.getData().getInitiation().getRemittanceInformation())) {
			remittanceDetails
					.setReference(stageResponse.getData().getInitiation().getRemittanceInformation().getReference());
			remittanceDetails.setUnstructured(
					stageResponse.getData().getInitiation().getRemittanceInformation().getUnstructured());
		}

		/* Charge details needed on consent page */
		ChargeDetails chargeDetails = new ChargeDetails();
		chargeDetails.setChargesList(stageResponse.getData().getCharges());

		/* No Exchange Rate for Domestic Payment on consent page */
		ExchangeRateDetails exchangeRateDetails = null;

		/* Custom Fields added if any */

		CustomISPData customISPData = new CustomISPData();

		customISPData
				.setRequestedExecutionDateTime(stageResponse.getData().getInitiation().getRequestedExecutionDateTime());
		if (!NullCheckUtils.isNullOrEmpty(stageResponse.getData().getInitiation().getCreditor())) {
			customISPData
					.setCreditorPostalAddress(stageResponse.getData().getInitiation().getCreditor().getPostalAddress());
		}
		customISPData.setCurrencyOfTransfer(stageResponse.getData().getInitiation().getCurrencyOfTransfer());
		response.setAmountDetails(amountDetails);
		response.setChargeDetails(chargeDetails);
		response.setCreditorDetails(creditorDetails);
		response.setDebtorDetails(debtorDetails);
		response.setRemittanceDetails(remittanceDetails);
		response.setExchangeRateDetails(exchangeRateDetails);
		response.setCustomISPData(customISPData);
		return response;
	}

	@Override
	public CustomFraudSystemPaymentData transformDStageToFraudData(CustomISPConsentsPOSTResponse stageResponse) {
		CustomFraudSystemPaymentData fraudSystemData = new CustomFraudSystemPaymentData();
		fraudSystemData.setPaymentType(PaymentTypeEnum.INTERNATIONAL_SCH_PAY.getPaymentType());
		fraudSystemData.setTransferAmount(stageResponse.getData().getInitiation().getInstructedAmount().getAmount());
		fraudSystemData
				.setTransferCurrency(stageResponse.getData().getInitiation().getInstructedAmount().getCurrency());
		if (!NullCheckUtils.isNullOrEmpty(stageResponse.getData().getInitiation().getRemittanceInformation()))
			fraudSystemData
					.setTransferMemo(stageResponse.getData().getInitiation().getRemittanceInformation().getReference());
		fraudSystemData.setTransferTime(stageResponse.getData().getCreationDateTime());
		CreditorDetails fraudSystenCreditorInfo = new CreditorDetails();
		fraudSystenCreditorInfo
				.setIdentification(stageResponse.getData().getInitiation().getCreditorAccount().getIdentification());
		fraudSystemData.setCreditorDetails(fraudSystenCreditorInfo);

		return fraudSystemData;
	}

	@Override
	public void updateStagedPaymentConsents(CustomPaymentStageIdentifiers customStageIdentifiers,
			CustomPaymentStageUpdateData stageUpdateData, Map<String, String> params) {
		try {

			// Update other than cma1 staged resource
				CustomISPConsentsPOSTResponse updatedStagedResource = iScheduledPaymentConsentsBankRepository
						.findOneByDataConsentId(customStageIdentifiers.getPaymentConsentId());
				if (stageUpdateData.getDebtorDetailsUpdated()) {
					updatedStagedResource.getData().getInitiation()
							.setDebtorAccount(stageUpdateData.getDebtorDetails());
				}

				if (stageUpdateData.getSetupStatusUpdated()) {
					updatedStagedResource.getData()
							.setStatus(OBExternalConsentStatus1Code.fromValue(stageUpdateData.getSetupStatus()));
					updatedStagedResource.getData()
							.setStatusUpdateDateTime(stageUpdateData.getSetupStatusUpdateDateTime());
				}
				iScheduledPaymentConsentsBankRepository.save(updatedStagedResource);
			
		} catch (DataAccessResourceFailureException exception) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, exception.getMessage()));
		}

	}

}
