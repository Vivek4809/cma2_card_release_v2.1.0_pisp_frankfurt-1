
package com.capgemini.psd2.foundationservice.validate.prestage.payment.boi.adapter.utility;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.foundationservice.validate.prestage.payment.boi.adapter.domain.PaymentInstruction;
import com.capgemini.psd2.foundationservice.validate.prestage.payment.boi.adapter.domain.ValidationPassed;
import com.capgemini.psd2.foundationservice.validate.prestage.payment.boi.adapter.transformer.ValidatePreStagePaymentFoundationServiceTransformer;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.PaymentSetupValidationResponse;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class ValidatePreStagePaymentFoundationServiceUtility {

	@Autowired
	private ValidatePreStagePaymentFoundationServiceTransformer validatePreStagePaymentFSTransformer;

	@Value("${foundationService.userInReqHeader:#{X-BOI-USER}}")
	private String userInReqHeader;

	@Value("${foundationService.channelInReqHeader:#{X-BOI-CHANNEL}}")
	private String channelInReqHeader;

	@Value("${foundationService.platformInReqHeader:#{X-BOI-PLATFORM}}")
	private String platformInReqHeader;

	@Value("${foundationService.correlationReqHeader:#{X-CORRELATION-ID}}")
	private String correlationReqHeader;

	/** The platform. */
	@Value("${foundationService.platform}")
	private String platform;

	public PaymentInstruction transformRequestFromAPIToFS(CustomPaymentSetupPOSTRequest paymentSetupPOSTRequest) {
		return validatePreStagePaymentFSTransformer.transformPaymentSetupPOSTRequest(paymentSetupPOSTRequest);
	}

	public PaymentSetupValidationResponse transformResponseFromFSToAPI(ValidationPassed validationPassed) {
		return validatePreStagePaymentFSTransformer.transformValidatePreStagePaymentResponse(validationPassed);
	}

	public HttpHeaders createRequestHeaders(RequestInfo requestInfo, PaymentSetupPOSTRequest paymentSetupPOSTRequest, Map<String, String> params) {

		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add(userInReqHeader, params.get(PSD2Constants.USER_IN_REQ_HEADER));
		httpHeaders.add(channelInReqHeader, params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER));
		String platformId = params.get(PSD2Constants.PLATFORM_IN_REQ_HEADER);
		httpHeaders.add(platformInReqHeader, NullCheckUtils.isNullOrEmpty(platformId) ? platform : platformId);
		httpHeaders.add(correlationReqHeader, params.get(PSD2Constants.CORRELATION_REQ_HEADER));
		return httpHeaders;
	}

}
