package com.capgemini.psd2.pisp.payment.setup.mongo.db.test.adapter.impl;

import com.capgemini.psd2.pisp.domain.CustomDSPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDSPaymentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBDomestic1InstructedAmount;
import com.capgemini.psd2.pisp.domain.OBDomesticScheduled1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticScheduled1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticScheduledResponse1;

public class DSPaymentsStagingMongoDbAdapterImplTestMockData {

	public static CustomDSPaymentsPOSTRequest getRequest() {

		CustomDSPaymentsPOSTRequest request = new CustomDSPaymentsPOSTRequest();
		request.setData(new OBWriteDataDomesticScheduled1());
		request.getData().setInitiation(new OBDomesticScheduled1());
		request.getData().getInitiation().setInstructedAmount(new OBDomestic1InstructedAmount());
		request.getData().getInitiation().getInstructedAmount().setAmount("1456.25");
		request.getData().getInitiation().getInstructedAmount().setCurrency("EUR");
		
		return request;

	}

	public static CustomDSPaymentsPOSTResponse getResponse() {
		
		CustomDSPaymentsPOSTResponse response = new CustomDSPaymentsPOSTResponse();
		response.setData(new OBWriteDataDomesticScheduledResponse1());
		response.getData().setInitiation(new OBDomesticScheduled1());
		response.getData().getInitiation().setInstructedAmount(new OBDomestic1InstructedAmount());
		response.getData().getInitiation().getInstructedAmount().setAmount("1235.25");
		
		return response;

	}
}
