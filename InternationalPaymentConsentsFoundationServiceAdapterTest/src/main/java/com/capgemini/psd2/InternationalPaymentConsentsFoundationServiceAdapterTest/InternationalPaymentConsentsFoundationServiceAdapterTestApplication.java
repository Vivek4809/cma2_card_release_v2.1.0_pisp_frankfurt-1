package com.capgemini.psd2.InternationalPaymentConsentsFoundationServiceAdapterTest;

import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.InternationalPaymentConsentsFoundationServiceAdapter;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;

/**
 * Hello world!
 *
 */

@SpringBootApplication
@ComponentScan("com.capgemini.psd2")
public class InternationalPaymentConsentsFoundationServiceAdapterTestApplication {
	public static void main(String[] args) {
		SpringApplication.run(InternationalPaymentConsentsFoundationServiceAdapterTestApplication.class, args);
	}
}

@RestController
@ResponseBody
class TestInternationalPaymentConsentsFoundationServiceAdapter {

	@Autowired
	private InternationalPaymentConsentsFoundationServiceAdapter iPaymentConsentsFoundationServiceAdapter;

	@RequestMapping(value = "/testInternationalPaymentConsentsCreate", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })

	@ResponseBody

	@ResponseStatus(HttpStatus.CREATED)
	public CustomIPaymentConsentsPOSTResponse internationalPaymentConsentsCreate(
			@RequestBody CustomIPaymentConsentsPOSTRequest paymentInstProposalReq) {
		CustomPaymentStageIdentifiers customPaymentStageIdentifiers = new CustomPaymentStageIdentifiers();
		customPaymentStageIdentifiers.setPaymentConsentId("1234567");
		customPaymentStageIdentifiers.setPaymentSetupVersion("3.0");
		customPaymentStageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.INTERNATIONAL_PAY);
		Map<String, String> params = new HashMap<String, String>();
		params.put("consentFlowType", "PISP");
		params.put("x-channel-id", "BOL");
		params.put("x-user-id", "BOI999");
		params.put("X-BOI-PLATFORM", "platform");
		params.put("x-fapi-interaction-id", "87654321");
		params.put("X-API-SOURCE-SYSTEM", "sdsa");
		params.put("tenant_id", "BOIUK");
		//X-BOI-CHANNEL,tenant_id

		return iPaymentConsentsFoundationServiceAdapter.createStagingInternationalPaymentConsents(
				paymentInstProposalReq, customPaymentStageIdentifiers, params);
	}

	@RequestMapping(value = "/testInternationalPaymentConsentsRetrieve", method = RequestMethod.GET)
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	public CustomIPaymentConsentsPOSTResponse internationalPaymentConsentsRetrieve() {
		CustomPaymentStageIdentifiers customPaymentStageIdentifiers = new CustomPaymentStageIdentifiers();
		customPaymentStageIdentifiers.setPaymentConsentId("e9d5f347-e73a-4e50-a8c3-bcaa406c4919");
		customPaymentStageIdentifiers.setPaymentSetupVersion("3.0");
		customPaymentStageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.INTERNATIONAL_PAY);
		Map<String, String> params = new HashMap<String, String>();
		params.put("consentFlowType", "PISP");
		params.put("x-channel-id", "BOL");
		params.put("x-user-id", "BOI999");
		params.put("X-BOI-PLATFORM", "platform");
		params.put("x-fapi-interaction-id", "87654321");
		params.put("tenant_id", "BOIUK");
		return iPaymentConsentsFoundationServiceAdapter
				.retrieveStagedInternationalPaymentConsents(customPaymentStageIdentifiers, params);
	}
}