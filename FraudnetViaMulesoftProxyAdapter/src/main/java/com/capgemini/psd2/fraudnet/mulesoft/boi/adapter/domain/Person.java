package com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain;

import java.time.LocalDate;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.annotations.SerializedName;

import io.swagger.annotations.ApiModelProperty;

/**
 * Person
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-03-19T13:42:54.062+05:30")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Person {
	@SerializedName("employersName")
	private String employersName = null;

	@SerializedName("birthDate")
	private LocalDate birthDate = null;

	@SerializedName("mothersMaidenName")
	private String mothersMaidenName = null;

	public Person employersName(String employersName) {
		this.employersName = employersName;
		return this;
	}

	/**
	 * Name of the employing organisation (e.g. Company, Government)
	 * 
	 * @return employersName
	 **/
	@ApiModelProperty(value = "Name of the employing organisation (e.g. Company, Government)")
	public String getEmployersName() {
		return employersName;
	}

	public void setEmployersName(String employersName) {
		this.employersName = employersName;
	}

	public Person birthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
		return this;
	}

	/**
	 * Date on which a natural Person was born.
	 * 
	 * @return birthDate
	 **/
	@ApiModelProperty(value = "Date on which a natural Person was born.")
	public LocalDate getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}

	public Person mothersMaidenName(String mothersMaidenName) {
		this.mothersMaidenName = mothersMaidenName;
		return this;
	}

	/**
	 * Mother&#39;s Maiden Name
	 * 
	 * @return mothersMaidenName
	 **/
	@ApiModelProperty(value = "Mother's Maiden Name")
	public String getMothersMaidenName() {
		return mothersMaidenName;
	}

	public void setMothersMaidenName(String mothersMaidenName) {
		this.mothersMaidenName = mothersMaidenName;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Person person = (Person) o;
		return Objects.equals(this.employersName, person.employersName)
				&& Objects.equals(this.birthDate, person.birthDate)
				&& Objects.equals(this.mothersMaidenName, person.mothersMaidenName);
	}

	@Override
	public int hashCode() {
		return Objects.hash(employersName, birthDate, mothersMaidenName);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class Person {\n");

		sb.append("    employersName: ").append(toIndentedString(employersName)).append("\n");
		sb.append("    birthDate: ").append(toIndentedString(birthDate)).append("\n");
		sb.append("    mothersMaidenName: ").append(toIndentedString(mothersMaidenName)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}

}
