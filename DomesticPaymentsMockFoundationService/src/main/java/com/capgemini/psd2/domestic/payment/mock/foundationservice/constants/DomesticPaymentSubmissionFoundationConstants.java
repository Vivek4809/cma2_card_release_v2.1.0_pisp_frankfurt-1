package com.capgemini.psd2.domestic.payment.mock.foundationservice.constants;

public class DomesticPaymentSubmissionFoundationConstants {

	public static final String RECORD_NOT_FOUND = "Payment Instruction Composite Record Not Found";
	
	public static final String INSTRUCTION_ENDTOEND_REF="FRESCO.21302.GFX.20";
	
}
