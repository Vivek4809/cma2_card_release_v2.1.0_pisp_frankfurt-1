package com.capgemini.psd2.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.capgemini.psd2.aisp.domain.AccountRequestPOSTRequest;
import com.capgemini.psd2.aisp.domain.AccountRequestPOSTResponse;
import com.capgemini.psd2.aisp.domain.Data1;
import com.capgemini.psd2.aisp.domain.Data1.StatusEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;
import com.capgemini.psd2.test.config.AccountRequestITConfig;
import com.capgemini.psd2.test.mock.data.AccountRequestITTestMockData;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = AccountRequestITConfig.class)
@WebIntegrationTest
public class AccountRequestIT {

	@Value("${app.edgeserverURL}")
	private String edgeserverURL;

	@Value("${app.accountRequestId1}")
	private String accountRequestId1;

	@Value("${app.accountRequestId2}")
	private String accountRequestId2;

	@Value("${app.accountRequestId3}")
	private String accountRequestId3;

	@Value("${app.accountRequestId4}")
	private String accountRequestId4;

	@Value("${app.accessToken}")
	private String accessToken;

	@Value("${app.accessToken1}")
	private String accessToken1;
	
	@Value("${app.accessTokenGet}")
	private String accessTokenGet;
	
	@Value("${app.accessTokenDelete}")
	private String accessTokenDelete;

	@Autowired
	protected RestClientSyncImpl restClientSync;

	@Autowired
	protected RestTemplate restTemplate;

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testGetAccountRequestSuccess() {
		String url = edgeserverURL + "account-requests/" + accountRequestId1;
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", accessTokenGet);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("client_id", "6443e15975554bce8099e35b88b40465");
		headers.add("client_secret", "7ca382cf4dff4d2dBCF034DBBA34A653");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		AccountRequestPOSTResponse accountRequestPOSTResponse = restClientSync.callForGet(requestInfo, AccountRequestPOSTResponse.class, headers);
		assertNotNull(accountRequestPOSTResponse);
		Data1 data = accountRequestPOSTResponse.getData();

		assertEquals(accountRequestId1, data.getAccountRequestId());
		assertNotNull(data.getCreationDateTime());
		assertNotNull(data.getStatus());
		assertNotNull(data.getPermissions());
	}

	@Test
	public void testPOSTAccountRequestSuccess() {
		String url = edgeserverURL + "account-requests";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", accessToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("client_id", "6443e15975554bce8099e35b88b40465");
		headers.add("client_secret", "7ca382cf4dff4d2dBCF034DBBA34A653");
		AccountRequestPOSTRequest accountRequestPOSTRequest = AccountRequestITTestMockData
				.getAccountRequestsParamatersMockData();
		HttpEntity<AccountRequestPOSTRequest> requestEntity = new HttpEntity<AccountRequestPOSTRequest>(
				accountRequestPOSTRequest, headers);
		ResponseEntity<AccountRequestPOSTResponse> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
				AccountRequestPOSTResponse.class);
		assertEquals(HttpStatus.CREATED, response.getStatusCode());
		AccountRequestPOSTResponse accountRequestPOSTResponse = (AccountRequestPOSTResponse) response.getBody();
		assertNotNull(accountRequestPOSTResponse);
		Data1 data = accountRequestPOSTResponse.getData();

		assertNotNull(data.getCreationDateTime());
		assertNotNull(data.getStatus());
		assertNotNull(data.getPermissions());
		assertEquals(StatusEnum.AWAITINGAUTHORISATION, data.getStatus());
		assertEquals(accountRequestPOSTRequest.getData().getExpirationDateTime(),
				accountRequestPOSTResponse.getData().getExpirationDateTime());

	}

	@Test
	public void testDeleteAccountRequestSuccess() {
		String url = edgeserverURL + "account-requests/" + accountRequestId1;
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", accessTokenDelete);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("client_id", "6443e15975554bce8099e35b88b40465");
		headers.add("client_secret", "7ca382cf4dff4d2dBCF034DBBA34A653");
		HttpEntity<?> requestEntity = new HttpEntity<>(headers);
		ResponseEntity<AccountRequestPOSTResponse> response = restTemplate.exchange(url, HttpMethod.DELETE,
				requestEntity, AccountRequestPOSTResponse.class);
		assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
	}

	@Test(expected = HttpClientErrorException.class)
	public void testPOSTAccountRequestWrongMethod() {
		String url = edgeserverURL + "account-requests";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", accessToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("client_id", "6443e15975554bce8099e35b88b40465");
		headers.add("client_secret", "7ca382cf4dff4d2dBCF034DBBA34A653");
		AccountRequestPOSTRequest accountRequestPOSTRequest = AccountRequestITTestMockData
				.getAccountRequestsParamatersMockData();
		HttpEntity<AccountRequestPOSTRequest> requestEntity = new HttpEntity<AccountRequestPOSTRequest>(
				accountRequestPOSTRequest, headers);
		restTemplate.exchange(url, HttpMethod.PUT, requestEntity, AccountRequestPOSTResponse.class);
	}
	
	@Test(expected = PSD2Exception.class)
	public void testGetAccountRequestInvalidAccountRequestId() {
		String url = edgeserverURL + "account-requests/" + accountRequestId2;
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", accessToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("client_id", "6443e15975554bce8099e35b88b40465");
		headers.add("client_secret", "7ca382cf4dff4d2dBCF034DBBA34A653");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			restClientSync.callForGet(requestInfo, AccountRequestPOSTResponse.class, headers);
		} catch (PSD2Exception e) {
			assertEquals("400", e.getErrorInfo().getStatusCode());
			throw e;
		}
	}

	@Test(expected = HttpClientErrorException.class)
	public void testDeleteAccountRequestInvalidAccountRequestId() {
		String url = edgeserverURL + "account-requests/" + accountRequestId2;
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", accessToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("client_id", "6443e15975554bce8099e35b88b40465");
		headers.add("client_secret", "7ca382cf4dff4d2dBCF034DBBA34A653");
		HttpEntity<?> requestEntity = new HttpEntity<>(headers);
		try {
			restTemplate.exchange(url, HttpMethod.DELETE, requestEntity, AccountRequestPOSTResponse.class);
		} catch (HttpClientErrorException e) {
			assertEquals("400", e.getStatusCode().toString());
			throw e;
		}

	}

	@Test(expected = PSD2Exception.class)
	public void testGetAccountRequestNoDataFound() {
		String url = edgeserverURL + "account-requests/" + accountRequestId3;
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", accessToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("client_id", "6443e15975554bce8099e35b88b40465");
		headers.add("client_secret", "7ca382cf4dff4d2dBCF034DBBA34A653");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			restClientSync.callForGet(requestInfo, AccountRequestPOSTResponse.class, headers);
		} catch (PSD2Exception e) {
			assertEquals("400", e.getErrorInfo().getStatusCode());
			throw e;
		}
	}

	@Test(expected = HttpClientErrorException.class)
	public void testDeleteAccountRequestNoDataFound() {
		String url = edgeserverURL + "account-requests/" + accountRequestId3;
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", accessToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("client_id", "6443e15975554bce8099e35b88b40465");
		headers.add("client_secret", "7ca382cf4dff4d2dBCF034DBBA34A653");
		HttpEntity<?> requestEntity = new HttpEntity<>(headers);
		try {
			restTemplate.exchange(url, HttpMethod.DELETE, requestEntity, AccountRequestPOSTResponse.class);
		} catch (HttpClientErrorException e) {
			assertEquals("400", e.getStatusCode().toString());
			throw e;
		}
	}

	@Test(expected = HttpClientErrorException.class)
	public void testPOSTAccountRequestInvalidFinancialId() {
		String url = edgeserverURL + "account-requests";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", accessToken);
		headers.add("client_id", "6443e15975554bce8099e35b88b40abc465");
		headers.add("client_secret", "7ca382cf4dff4d2dBCF034DBBA34A653");
		AccountRequestPOSTRequest accountRequestPOSTRequest = AccountRequestITTestMockData
				.getAccountRequestsParamatersMockData();
		HttpEntity<AccountRequestPOSTRequest> requestEntity = new HttpEntity<AccountRequestPOSTRequest>(
				accountRequestPOSTRequest, headers);
		try {
			restTemplate.exchange(url, HttpMethod.POST, requestEntity, AccountRequestPOSTResponse.class);
		} catch (HttpClientErrorException e) {
			assertEquals("400", e.getStatusCode().toString());
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void testGetAccountRequestInvalidFinancialId() {
		String url = edgeserverURL + "account-requests/" + accountRequestId1;
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", accessToken);
		headers.add("client_id", "6443e15975554bce80abasa99e35b88b40465");
		headers.add("client_secret", "7ca382cf4dff4d2dBCF034DBBA34A653");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			restClientSync.callForGet(requestInfo, AccountRequestPOSTResponse.class, headers);
		} catch (PSD2Exception e) {
			assertEquals("400", e.getErrorInfo().getStatusCode());
			throw e;
		}
	}

	@Test(expected = HttpClientErrorException.class)
	public void testDeleteAccountRequestInvalidFinancialId() {
		String url = edgeserverURL + "account-requests/" + accountRequestId1;
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", accessToken);
		headers.add("client_id", "6443e15975554bce8099e35b88b40465");
		headers.add("client_secret", "7ca382cf4dff4d2dBCF034DBBA34A653");
		HttpEntity<?> requestEntity = new HttpEntity<>(headers);
		try {
			restTemplate.exchange(url, HttpMethod.DELETE, requestEntity, AccountRequestPOSTResponse.class);
		} catch (HttpClientErrorException e) {
			assertEquals("400", e.getStatusCode().toString());
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void testPOSTAccountRequestInvalidPermissions() {
		String url = edgeserverURL + "account-requests";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", accessToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("client_id", "6443e15975554bce8099e35b88b40465");
		headers.add("client_secret", "7ca382cf4dff4d2dBCF034DBBA34A653");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		AccountRequestPOSTRequest accountRequestPOSTRequest = AccountRequestITTestMockData
				.getAccountRequestsParamatersMockDataInvalidPermissions();
		HttpEntity<AccountRequestPOSTRequest> requestEntity = new HttpEntity<AccountRequestPOSTRequest>(
				accountRequestPOSTRequest, headers);
		try {
			restClientSync.callForPost(requestInfo, requestEntity, AccountRequestPOSTResponse.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(HttpStatus.BAD_REQUEST.value(), Integer.parseInt(e.getErrorInfo().getStatusCode()));
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void testPOSTAccountRequestInvalidRequestBody() {
		String url = edgeserverURL + "account-requests";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", accessToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("client_id", "6443e15975554bce8099e35b88b40465");
		headers.add("client_secret", "7ca382cf4dff4d2dBCF034DBBA34A653");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		String accountRequestPOSTRequest = AccountRequestITTestMockData
				.getAccountRequestsParamatersMockDataInvalidRequest();
		HttpEntity<String> requestEntity = new HttpEntity<String>(accountRequestPOSTRequest, headers);
		try {
			restClientSync.callForPost(requestInfo, requestEntity, AccountRequestPOSTResponse.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(HttpStatus.BAD_REQUEST.value(), Integer.parseInt(e.getErrorInfo().getStatusCode()));
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void testPOSTAccountRequestInvalidExpirationDateTime() {
		String url = edgeserverURL + "account-requests";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", accessToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("client_id", "6443e15975554bce8099e35b88b40465");
		headers.add("client_secret", "7ca382cf4dff4d2dBCF034DBBA34A653");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		AccountRequestPOSTRequest accountRequestPOSTRequest = AccountRequestITTestMockData
				.getAccountRequestsParamatersMockDataInvalidExpirationDateTime();
		HttpEntity<AccountRequestPOSTRequest> requestEntity = new HttpEntity<AccountRequestPOSTRequest>(
				accountRequestPOSTRequest, headers);
		try {
			restClientSync.callForPost(requestInfo, requestEntity, AccountRequestPOSTResponse.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(HttpStatus.BAD_REQUEST.value(), Integer.parseInt(e.getErrorInfo().getStatusCode()));
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void testPOSTAccountRequestInvalidToAndFromDateTime() {
		String url = edgeserverURL + "account-requests";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", accessToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("client_id", "6443e15975554bce8099e35b88b40465");
		headers.add("client_secret", "7ca382cf4dff4d2dBCF034DBBA34A653");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		AccountRequestPOSTRequest accountRequestPOSTRequest = AccountRequestITTestMockData
				.getAccountRequestsParamatersMockDataInvalidToAndFromDateTime();
		HttpEntity<AccountRequestPOSTRequest> requestEntity = new HttpEntity<AccountRequestPOSTRequest>(
				accountRequestPOSTRequest, headers);
		try {
			restClientSync.callForPost(requestInfo, requestEntity, AccountRequestPOSTResponse.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(HttpStatus.BAD_REQUEST.value(), Integer.parseInt(e.getErrorInfo().getStatusCode()));
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void testPOSTAccountRequestPastExpirationDateTime() {
		String url = edgeserverURL + "account-requests";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", accessToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("client_id", "6443e15975554bce8099e35b88b40465");
		headers.add("client_secret", "7ca382cf4dff4d2dBCF034DBBA34A653");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		AccountRequestPOSTRequest accountRequestPOSTRequest = AccountRequestITTestMockData
				.getAccountRequestsParamatersMockDataPastExpirationDateTime();
		HttpEntity<AccountRequestPOSTRequest> requestEntity = new HttpEntity<AccountRequestPOSTRequest>(
				accountRequestPOSTRequest, headers);
		try {
			restClientSync.callForPost(requestInfo, requestEntity, AccountRequestPOSTResponse.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(HttpStatus.BAD_REQUEST.value(), Integer.parseInt(e.getErrorInfo().getStatusCode()));
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void testPOSTAccountRequestToBeforeFromDateTime() {
		String url = edgeserverURL + "account-requests";
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", accessToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("client_id", "6443e15975554bce8099e35b88b40465");
		headers.add("client_secret", "7ca382cf4dff4d2dBCF034DBBA34A653");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		AccountRequestPOSTRequest accountRequestPOSTRequest = AccountRequestITTestMockData
				.getAccountRequestsParamatersMockDataToBeforeFromDateTime();
		HttpEntity<AccountRequestPOSTRequest> requestEntity = new HttpEntity<AccountRequestPOSTRequest>(
				accountRequestPOSTRequest, headers);
		try {
			restClientSync.callForPost(requestInfo, requestEntity, AccountRequestPOSTResponse.class, headers);
		} catch (PSD2Exception e) {
			assertEquals(HttpStatus.BAD_REQUEST.value(), Integer.parseInt(e.getErrorInfo().getStatusCode()));
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void testGetAccountRequestInvalidAccountRequestIdLength() {
		String url = edgeserverURL + "account-requests/" + accountRequestId4;
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", accessToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("client_id", "6443e15975554bce80abasa99e35b88b40465");
		headers.add("client_secret", "7ca382cf4dff4d2dBCF034DBBA34A653");
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);
		try {
			restClientSync.callForGet(requestInfo, AccountRequestPOSTResponse.class, headers);
		} catch (PSD2Exception e) {
			assertEquals("400", e.getErrorInfo().getStatusCode());
			throw e;
		}
	}

	@Test(expected = HttpClientErrorException.class)
	public void testDeleteAccountRequestInvalidAccountRequestIdLength() {
		String url = edgeserverURL + "account-requests/" + accountRequestId4;
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", accessToken);
		headers.add("x-fapi-financial-id", "1234");
		headers.add("client_id", "6443e15975554bce8099e35b88b40465");
		headers.add("client_secret", "7ca382cf4dff4d2dBCF034DBBA34A653");
		HttpEntity<?> requestEntity = new HttpEntity<>(headers);
		try {
			restTemplate.exchange(url, HttpMethod.DELETE, requestEntity, AccountRequestPOSTResponse.class);
		} catch (HttpClientErrorException e) {
			assertEquals("400", e.getStatusCode().toString());
			throw e;
		}
	}

}
