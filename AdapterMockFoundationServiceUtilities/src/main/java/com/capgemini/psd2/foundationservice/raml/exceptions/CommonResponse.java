package com.capgemini.psd2.foundationservice.raml.exceptions;

import java.util.Date;

public class CommonResponse{
  private String source;

  private Date timestamp;

  private String code;

  private TypeEnum type;

  private String summary;

  private String description;

  public String getSource() {
    return this.source;
  }

  public void setSource(String source) {
    this.source = source;
  }

  public Date getTimestamp() {
    return this.timestamp;
  }

  public void setTimestamp(Date timestamp) {
    this.timestamp = timestamp;
  }

  public String getCode() {
    return this.code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public TypeEnum getType() {
    return this.type;
  }

  public void setType(TypeEnum type) {
    this.type = type;
  }

  public String getSummary() {
    return this.summary;
  }

  public void setSummary(String summary) {
    this.summary = summary;
  }

  public String getDescription() {
    return this.description;
  }

  public void setDescription(String description) {
    this.description = description;
  }
}
