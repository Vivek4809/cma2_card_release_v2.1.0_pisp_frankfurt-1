package com.capgemini.psd2.pisp.isp.test.transformer.impl;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.bind.annotation.RequestMethod;

import com.capgemini.psd2.pisp.domain.OBWriteDataInternationalScheduled1;
import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomISPaymentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.Links;
import com.capgemini.psd2.pisp.domain.Meta;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBExternalStatus1Code;
import com.capgemini.psd2.pisp.domain.OBExternalStatus2Code;
import com.capgemini.psd2.pisp.domain.OBInternationalScheduled1;
import com.capgemini.psd2.pisp.domain.OBMultiAuthorisation1;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternationalScheduledResponse1;
import com.capgemini.psd2.pisp.domain.OBWriteInternationalScheduled1;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentsPlatformResource;
import com.capgemini.psd2.pisp.isp.test.transformer.impl.Mock.ISPaymentsResponseTransformerImplMockData;
import com.capgemini.psd2.pisp.isp.transformer.impl.ISPaymentsResponseTransformerImpl;
import com.capgemini.psd2.pisp.payment.submission.platform.adapter.PaymentsPlatformAdapter;
import com.capgemini.psd2.pisp.status.PaymentConstants;
import com.capgemini.psd2.pisp.utilities.PispDateUtility;

@RunWith(SpringJUnit4ClassRunner.class)
public class ISPaymentsResponseTransformerImplTest {

	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Mock
	private PaymentsPlatformAdapter paymentsPlatformAdapter;

	@Mock
	private PispDateUtility pispDateUtility;

	@InjectMocks
	private ISPaymentsResponseTransformerImpl transformer;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);

		Map<String, String> map = new HashMap<>();

		map.put("SIGNATURE", "Something wrong with x-jws-signature request header");

		Map<String, String> specificErrorMessageMap = new HashMap<>();

		specificErrorMessageMap.put("signature_invalid_content",
				"Invalid content prsent in joseheader of x-jws-signature header");

		specificErrorMessageMap.put("signature_missing", "signature header missing in request");

		OBPSD2ExceptionUtility.genericErrorMessages.putAll(map);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessageMap);

	}

	@Test
	public void contextLoads() {
	}

	@After
	public void tearDown() {
		reqHeaderAtrributes = null;
		paymentsPlatformAdapter = null;
		pispDateUtility = null;
		transformer = null;
	}

	@Test
	public void updateMetaAndLinksTest() {
		CustomISPaymentsPOSTResponse paymentSubmissionResponse = new CustomISPaymentsPOSTResponse();
		Links links = new Links();
		Meta meta = new Meta();
		OBWriteDataInternationalScheduledResponse1 data = new OBWriteDataInternationalScheduledResponse1();
		paymentSubmissionResponse.setLinks(links);
		paymentSubmissionResponse.setMeta(meta);
		paymentSubmissionResponse.setData(data);

		String methodType = RequestMethod.POST.toString();

		transformer.updateMetaAndLinks(paymentSubmissionResponse, methodType);

	}

	@Test
	public void paymentSubmissionResponseTransformerTest_POST() {
		ISPaymentsResponseTransformerImplMockData iSPaymentsResponseTransformerImplMockData = new ISPaymentsResponseTransformerImplMockData();

		Map<String, Object> paymentsPlatformResourceMap = new HashMap<>();
		String methodType = RequestMethod.POST.toString();

		PaymentsPlatformResource paymentsPlatformResource = new PaymentsPlatformResource();
		paymentsPlatformResource.setCreatedAt("");
		paymentsPlatformResource.setStatus("InitiationCompleted");
		paymentsPlatformResource.setStatusUpdateDateTime("");

		CustomISPaymentsPOSTResponse paymentSubmissionResponse = new CustomISPaymentsPOSTResponse();

		OBWriteDataInternationalScheduledResponse1 oBWriteDataInternationalScheduledResponse1 = iSPaymentsResponseTransformerImplMockData
				.getOBInternationalScheduled1();
		// Setting Consents
		PaymentConsentsPlatformResource paymentConsentsPlatformResponse = new PaymentConsentsPlatformResource();
		paymentConsentsPlatformResponse.setTppDebtorDetails("false");

		paymentSubmissionResponse.setData(oBWriteDataInternationalScheduledResponse1);

		paymentsPlatformResourceMap.put(PaymentConstants.SUBMISSION, paymentsPlatformResource);
		paymentsPlatformResourceMap.put(PaymentConstants.CONSENT, paymentConsentsPlatformResponse);

		paymentSubmissionResponse.getData().setStatus(OBExternalStatus1Code.INITIATIONCOMPLETED);

		paymentSubmissionResponse.getData().getInitiation().setDebtorAccount(null);

		transformer.paymentSubmissionResponseTransformer(paymentSubmissionResponse, paymentsPlatformResourceMap,
				methodType);

	}

	@Test
	public void paymentSubmissionResponseTransformerTest_InitiationCompleted() {
		ISPaymentsResponseTransformerImplMockData iSPaymentsResponseTransformerImplMockData = new ISPaymentsResponseTransformerImplMockData();
		Map<String, Object> paymentsPlatformResourceMap = new HashMap<>();
		String methodType = RequestMethod.PUT.toString();

		PaymentsPlatformResource paymentsPlatformResource = new PaymentsPlatformResource();
		paymentsPlatformResource.setCreatedAt("");
		paymentsPlatformResource.setStatus("InitiationCompleted");
		paymentsPlatformResource.setStatusUpdateDateTime("");

		CustomISPaymentsPOSTResponse paymentSubmissionResponse = new CustomISPaymentsPOSTResponse();
		OBWriteDataInternationalScheduledResponse1 oBWriteDataInternationalScheduledResponse1 = iSPaymentsResponseTransformerImplMockData
				.getOBInternationalScheduled1();

		// Setting Consents
		PaymentConsentsPlatformResource paymentConsentsPlatformResponse = new PaymentConsentsPlatformResource();

		paymentSubmissionResponse.setData(oBWriteDataInternationalScheduledResponse1);

		paymentsPlatformResourceMap.put(PaymentConstants.SUBMISSION, paymentsPlatformResource);
		paymentsPlatformResourceMap.put(PaymentConstants.CONSENT, paymentConsentsPlatformResponse);

		paymentSubmissionResponse.getData().setStatus(OBExternalStatus1Code.INITIATIONCOMPLETED);

		paymentSubmissionResponse.getData().getInitiation().setDebtorAccount(new OBCashAccountDebtor3());
		paymentSubmissionResponse.getData().getInitiation().getDebtorAccount().setSchemeName("IBAN");

		transformer.paymentSubmissionResponseTransformer(paymentSubmissionResponse, paymentsPlatformResourceMap,
				methodType);

	}

	@Test
	public void paymentSubmissionResponseTransformerTest_PendingAuthorized() {
		ISPaymentsResponseTransformerImplMockData iSPaymentsResponseTransformerImplMockData = new ISPaymentsResponseTransformerImplMockData();
		Map<String, Object> paymentsPlatformResourceMap = new HashMap<>();
		String methodType = RequestMethod.PUT.toString();

		PaymentsPlatformResource paymentsPlatformResource = new PaymentsPlatformResource();
		paymentsPlatformResource.setCreatedAt("");
		paymentsPlatformResource.setStatus("InitiationPending");
		paymentsPlatformResource.setStatusUpdateDateTime("");

		CustomISPaymentsPOSTResponse paymentSubmissionResponse = new CustomISPaymentsPOSTResponse();
		OBWriteDataInternationalScheduledResponse1 oBWriteDataInternationalScheduledResponse1 = iSPaymentsResponseTransformerImplMockData
				.getOBInternationalScheduled1();

		// Setting Consents
		PaymentConsentsPlatformResource paymentConsentsPlatformResponse = new PaymentConsentsPlatformResource();
		paymentConsentsPlatformResponse.setTppDebtorDetails("true");
		paymentConsentsPlatformResponse.setTppDebtorNameDetails("false");
		
		paymentSubmissionResponse.setData(oBWriteDataInternationalScheduledResponse1);

		OBMultiAuthorisation1 multiAuth = new OBMultiAuthorisation1();
		multiAuth.setStatus(OBExternalStatus2Code.AUTHORISED);
		paymentSubmissionResponse.getData().setMultiAuthorisation(multiAuth);

		paymentsPlatformResourceMap.put(PaymentConstants.SUBMISSION, paymentsPlatformResource);
		paymentsPlatformResourceMap.put(PaymentConstants.CONSENT, paymentConsentsPlatformResponse);

		paymentSubmissionResponse.getData().setStatus(OBExternalStatus1Code.INITIATIONPENDING);

		paymentSubmissionResponse.getData().getInitiation().setDebtorAccount(new OBCashAccountDebtor3());

		transformer.paymentSubmissionResponseTransformer(paymentSubmissionResponse, paymentsPlatformResourceMap,
				methodType);

	}

	@Test
	public void paymentSubmissionResponseTransformerTest_Pendingrejected() {
		ISPaymentsResponseTransformerImplMockData iSPaymentsResponseTransformerImplMockData = new ISPaymentsResponseTransformerImplMockData();
		Map<String, Object> paymentsPlatformResourceMap = new HashMap<>();
		String methodType = RequestMethod.PUT.toString();

		PaymentsPlatformResource paymentsPlatformResource = new PaymentsPlatformResource();
		paymentsPlatformResource.setCreatedAt("");
		paymentsPlatformResource.setStatus(OBExternalStatus1Code.INITIATIONPENDING.toString());
		paymentsPlatformResource.setStatusUpdateDateTime("");

		CustomISPaymentsPOSTResponse paymentSubmissionResponse = new CustomISPaymentsPOSTResponse();
		OBWriteDataInternationalScheduledResponse1 oBWriteDataInternationalScheduledResponse1 = iSPaymentsResponseTransformerImplMockData
				.getOBInternationalScheduled1();

		// Setting Consents
		PaymentConsentsPlatformResource paymentConsentsPlatformResponse = new PaymentConsentsPlatformResource();
		paymentConsentsPlatformResponse.setTppDebtorDetails("false");

		paymentSubmissionResponse.setData(oBWriteDataInternationalScheduledResponse1);

		OBMultiAuthorisation1 multiAuth = new OBMultiAuthorisation1();
		multiAuth.setStatus(OBExternalStatus2Code.REJECTED);
		paymentSubmissionResponse.getData().setMultiAuthorisation(multiAuth);

		paymentsPlatformResourceMap.put(PaymentConstants.SUBMISSION, paymentsPlatformResource);
		paymentsPlatformResourceMap.put(PaymentConstants.CONSENT, paymentConsentsPlatformResponse);

		paymentSubmissionResponse.getData().setStatus(OBExternalStatus1Code.INITIATIONPENDING);

		paymentSubmissionResponse.getData().getInitiation().setDebtorAccount(null);

		transformer.paymentSubmissionResponseTransformer(paymentSubmissionResponse, paymentsPlatformResourceMap,
				methodType);

	}

	@Test
	public void paymentSubmissionRequestTransformerTest() {
		OBWriteInternationalScheduled1 paymentSubmissionRequest = new OBWriteInternationalScheduled1();
		OBWriteDataInternationalScheduled1 data = new OBWriteDataInternationalScheduled1();
		OBInternationalScheduled1 initiation = new OBInternationalScheduled1();
		initiation.requestedExecutionDateTime("2018-05-09T11:10:12.064Z");
		paymentSubmissionRequest.setData(data);
		data.setInitiation(initiation);
		transformer.paymentSubmissionRequestTransformer(paymentSubmissionRequest);

	}
}
