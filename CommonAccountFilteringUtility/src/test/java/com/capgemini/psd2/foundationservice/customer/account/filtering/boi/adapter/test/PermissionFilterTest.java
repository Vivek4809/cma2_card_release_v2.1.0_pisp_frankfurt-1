package com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.constants.CustomerAccountsFilterFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accounts;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.FilteredAccounts;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Jurisdiction;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.filterartion.FilterationChain;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.filterartion.PermissionFilter;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.Account;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.AccountEntitlements;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PartyEntitlements;

public class PermissionFilterTest {


	@InjectMocks
	private PermissionFilter permissionFilter;

	@Mock
	private FilterationChain nextInChain;

	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	
	






/*	@Test
	public void testAccountTypeFilterProcess() {
		List<AccountEntitlements> acenList =new ArrayList<AccountEntitlements>();
		acenList.add(null);
		Account acc=new Account();
		Account acc1=new Account();
		AccountEntitlements acen = new AccountEntitlements();
		acen.setAccount(acc);
		acen.setAccount(acc1);
		acenList.add(acen);
		Map<String, List<String>> permissionList =new HashMap<String, List<String>>();
		List<String> li=new  ArrayList<String>();
		permissionList.put("1", li);
		String consentFlowType="AISP";
		boolean accountEntitlementFalg =true;
		Map<String, Map<String, List<String>>> accountFiltering = createAccountFilteringList();
		permissionFilter.setNext(nextInChain);
		Mockito.when(nextInChain.process(any(),any(), any(), any())).thenReturn(acenList);
		acenList =	permissionFilter.process( acenList,any(),consentFlowType, accountFiltering);
		assertNotNull(acenList);

	}
*/

	@Test
	public void testAccountTypeFilterProcess1() {
		List<AccountEntitlements> acenList =new ArrayList<AccountEntitlements>();
		acenList.add(null);
		Account acc=new Account();
		AccountEntitlements acen = new AccountEntitlements();
		acen.setAccount(acc);
		acenList.add(acen);
		String consentFlowType="AISP";
		Map<String, Map<String, List<String>>> accountFiltering = new HashMap<>();
		permissionFilter.setNext(nextInChain);
		PartyEntitlements partyEntitlements= new PartyEntitlements();
		Mockito.when(nextInChain.process(any(),any(), any(), any())).thenReturn(acenList);
		acenList =	permissionFilter.process( acenList,partyEntitlements,consentFlowType, accountFiltering);
		assertNotNull(acenList);

	}


	public Map<String, Map<String, List<String>>> createAccountFilteringList() {

		Map<String, Map<String, List<String>>> accountFiltering = new HashMap<>();

		Map<String, List<String>> map1 = new HashMap<String, List<String>>();

		List<String> accountType = new ArrayList<>();
		accountType.add("Current Account");
		map1.put("AISP", accountType);
		map1.put("CISP", accountType);
		map1.put("PISP", accountType);

		accountFiltering.put("accountType", map1);

		Map<String, List<String>> map2 = new HashMap<String, List<String>>();

		List<String> jurisdictionList = new ArrayList<>();
		jurisdictionList.add("NORTHERN_IRELAND.SchemeName=BBAN");
		jurisdictionList.add("NORTHERN_IRELAND.Identification=IBAN");
		jurisdictionList.add("NORTHERN_IRELAND.ServicerSchemeName=UKSORTCODE");
		jurisdictionList.add("NORTHERN_IRELAND.ServicerIdentification=BIC");
		jurisdictionList.add("GREAT_BRITAIN.SchemeName=BBAN");
		jurisdictionList.add("GREAT_BRITAIN.Identification=IBAN");
		jurisdictionList.add("GREAT_BRITAIN.ServicerSchemeName=UKSORTCODE");
		jurisdictionList.add("GREAT_BRITAIN.ServicerIdentification=BIC");

		map2.put("AISP", jurisdictionList);
		map2.put("CISP", jurisdictionList);
		map2.put("PISP", jurisdictionList);

		accountFiltering.put("jurisdiction", map2);

		Map<String, List<String>> map3 = new HashMap<String, List<String>>();

		List<String> permissionList1 = new ArrayList<>();
		permissionList1.add("A");
		permissionList1.add("V");
		List<String> permissionList2 = new ArrayList<>();
		permissionList2.add("A");
		permissionList2.add("X");

		map3.put("AISP", permissionList1);
		map3.put("CISP", permissionList1);
		map3.put("PISP", permissionList2);

		accountFiltering.put("permission", map3);
		return accountFiltering;
	}



}
