package com.capgemini.psd2.security.test.exceptions;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.security.exceptions.SCAConsentErrorCodeEnum;

@RunWith(SpringJUnit4ClassRunner.class)
public class SCAConsentErrorCodeEnumTest {
	
	private SCAConsentErrorCodeEnum consentErrorCode;
	
	@Before
	public void setUp() throws Exception {	
		MockitoAnnotations.initMocks(this);
		consentErrorCode = SCAConsentErrorCodeEnum.INVALID_REQUEST;
	}
	
	@Test
	public void testGetters(){
		assertEquals("700",consentErrorCode.getErrorCode());
		assertEquals("Invalid request",consentErrorCode.getErrorMessage());
		assertEquals("Invalid request",consentErrorCode.getDetailErrorMessage());
		assertEquals("400",consentErrorCode.getStatusCode());
	}
}
