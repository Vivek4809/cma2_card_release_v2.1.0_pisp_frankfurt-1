package com.capgemini.psd2.domestic.standing.order.mock.foundationservice.service.impl;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.domestic.standing.order.mock.foundationservice.constants.DomesticStandingOrderFoundationConstants;
import com.capgemini.psd2.domestic.standing.order.mock.foundationservice.exception.handler.RecordNotFoundException;
import com.capgemini.psd2.domestic.standing.order.mock.foundationservice.raml.domain.PaymentInstructionStatusCode2;
import com.capgemini.psd2.domestic.standing.order.mock.foundationservice.raml.domain.ScheduledPaymentInstructionProposal2;
import com.capgemini.psd2.domestic.standing.order.mock.foundationservice.raml.domain.StandingOrderInstructionComposite;
import com.capgemini.psd2.domestic.standing.order.mock.foundationservice.repository.DomesticStandingOrderFoundationRepository;
import com.capgemini.psd2.domestic.standing.order.mock.foundationservice.service.DomesticStandingOrderService;


@Service
public class DomesticStandingOrderFoundationServiceImpl implements DomesticStandingOrderService {

	@Autowired
	private DomesticStandingOrderFoundationRepository repository;
	
	@Override
	public StandingOrderInstructionComposite retrieveAccountInformation(String paymentInstructionNumber)
			throws Exception {

		StandingOrderInstructionComposite standingOrderInstructionComposite = repository
				.findByPaymentInstructionPaymentInstructionNumber(paymentInstructionNumber);

		if (null == standingOrderInstructionComposite) {
			throw new RecordNotFoundException(DomesticStandingOrderFoundationConstants.RECORD_NOT_FOUND);
		}

		return standingOrderInstructionComposite;
	}

	@Override
	public StandingOrderInstructionComposite createDomesticStandingOrdersConsentsResource(
			StandingOrderInstructionComposite standingOrderInstructionProposalCompositeReq) throws Exception {
		
		ScheduledPaymentInstructionProposal2 standingOrderInstructionProposalReq=new ScheduledPaymentInstructionProposal2();
		String submissionID=null;
		if (null == standingOrderInstructionProposalCompositeReq) {

			throw new RecordNotFoundException(DomesticStandingOrderFoundationConstants.RECORD_NOT_FOUND);
			
		}
		
		submissionID = UUID.randomUUID().toString();
		String[] check=standingOrderInstructionProposalCompositeReq.getPaymentInstructionProposal().getReference().split(":");
		if (check.length > 1 && check[1].equalsIgnoreCase("REJECTED")) {
			standingOrderInstructionProposalReq.setPaymentInstructionStatusCode(PaymentInstructionStatusCode2.INITIATIONFAILED);
		} else {
			standingOrderInstructionProposalReq.setPaymentInstructionStatusCode(PaymentInstructionStatusCode2.INITIATIONCOMPLETED);
		}
		
		standingOrderInstructionProposalReq.setPaymentInstructionNumber(submissionID);
		standingOrderInstructionProposalCompositeReq.setPaymentInstruction(standingOrderInstructionProposalReq);
		
		repository.save(standingOrderInstructionProposalCompositeReq);
		
		return standingOrderInstructionProposalCompositeReq;
	}

}
