package com.capgemini.psd2.funds.confirmation.mongo.db.adapter.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.cisp.mock.domain.MockFundData;

public interface FundsConfirmationRepository extends MongoRepository<MockFundData, String>{

	public MockFundData findByAccountNumberAndAccountNsc(String accNum,String accNsc);
	
	public MockFundData findByAccountNumber(String accNum);
}
