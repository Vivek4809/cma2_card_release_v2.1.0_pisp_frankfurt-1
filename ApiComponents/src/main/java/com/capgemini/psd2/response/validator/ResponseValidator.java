package com.capgemini.psd2.response.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.validator.PSD2Validator;

@Component
public class ResponseValidator {

	@Autowired
	private PSD2Validator cmaValidator;

	public void validateResponse(Object input) {

		/* Platform validations other than CMA-swagger validations. */
		performPlatformValidation(input);

		/* Platform validations for CMA-swagger validations. */
		performSwaggerValidation(input);

	}

	public void performPlatformValidation(Object input) {
		/* Override this method to provide custom validations. */
	}

	private void performSwaggerValidation(Object input) {
		cmaValidator.validate(input);
	}
}
