package com.capgemini.psd2.security.consent.aisp.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.capgemini.psd2.account.request.routing.adapter.impl.AccountRequestRoutingAdapter;
import com.capgemini.psd2.aisp.adapter.AccountRequestAdapter;
import com.capgemini.psd2.scaconsenthelper.filters.ConsentBlockBackActionFilter;
import com.capgemini.psd2.scaconsenthelper.filters.ConsentCookieHandlerFilter;
import com.capgemini.psd2.scaconsenthelper.config.helpers.ConsentAuthorizationHelper;
import com.capgemini.psd2.scaconsenthelper.filters.CookieHandlerFilter;
import com.capgemini.psd2.scaconsenthelper.models.IntentTypeEnum;
import com.capgemini.psd2.security.consent.aisp.helpers.AispConsentAuthorizationHelper;

@Configuration
public class AispConsentConfig {

	@Bean(name = "AccountRequestAdapter")
	public AccountRequestAdapter accountRequestAdapter(){
		return new AccountRequestRoutingAdapter();
	}
	
	@Bean
	public CookieHandlerFilter buildAispCookieHandlerFilter(){
		return new ConsentCookieHandlerFilter(IntentTypeEnum.AISP_INTENT_TYPE.getIntentType());
	}
	
	@Bean
	public ConsentBlockBackActionFilter buildAispBlockActionFilter(){
		return new ConsentBlockBackActionFilter();		
	}
	
	@Bean
	public FilterRegistrationBean blockAISPRefreshEventFilter(){
		FilterRegistrationBean filterRegBean = new FilterRegistrationBean();
		filterRegBean.setFilter(buildAispBlockActionFilter());
		List<String> urlPatterns = new ArrayList<>();
		urlPatterns.add("/aisp/home");
		filterRegBean.setName("aispBlockBackEventFilter");
		filterRegBean.setUrlPatterns(urlPatterns);
		return filterRegBean;
	}	
	
	@Bean
	public FilterRegistrationBean aispFilter(){
		FilterRegistrationBean filterRegBean = new FilterRegistrationBean();
		filterRegBean.setFilter(buildAispCookieHandlerFilter());
		List<String> urlPatterns = new ArrayList<>();
		urlPatterns.add("/aisp/customerconsentview");
		urlPatterns.add("/aisp/consent");
		urlPatterns.add("/aisp/cancelConsent");		
		urlPatterns.add("/aisp/checkSession");
		filterRegBean.setName("aispCookieFilter");
		filterRegBean.setUrlPatterns(urlPatterns);
		return filterRegBean;
	}	
	
	@Bean(name = "aispConsentAuthorizationHelper")
	public ConsentAuthorizationHelper aispConsentAuthorizationHelper(){
		return new AispConsentAuthorizationHelper();
	}
	
}
