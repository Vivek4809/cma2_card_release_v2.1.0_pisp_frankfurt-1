"use strict";
describe("ConsentApp.acctPagination_test", function() {
    beforeEach(module("consentTemplates", "consentApp"));
    var compile, scope, element, allAccounts;

    beforeEach(inject(function($rootScope, $compile) {
        allAccounts = [{ "userId": "1245", "accountNumber": "10203355", "accountName": "John Doe", "accountType": "checking", "currency": "eur", "nickname": "Salary", "accountNSC": " SC802001", "selected": true, "$$hashKey": "object:47" }, { "userId": "1246", "accountNumber": "10203356", "accountName": "John Doe", "accountType": "checking", "currency": "doller", "nickname": "Salary", "accountNSC": " SC802001", "selected": true, "$$hashKey": "object:48" }];
        scope = $rootScope.$new();
        element = angular.element("<acct-pagination acct-page='pageSize' max-size='maxPaginationSize' force-ellipse='true' rotate='false' current-page='currentPage' filtered-accounts='allAccounts' accounts-by-page='AccountsByPage' ng-show='allAccounts.length > pageSize'></acct-pagination>");
        $compile(element)(scope);
        scope.$digest();
    }));

    describe("test all", function() {
        it("test watch on accountsByPage", function() {
            element.isolateScope().accountsByPage = ["a", "b", "c"];
            element.isolateScope().filteredAccounts = allAccounts;
            scope.$digest();
            expect(element.isolateScope().pages).toEqual([0, 1, 2]);
            expect(element.isolateScope().totalAccounts).toBe(2);
        });

        it("test range", function() {
            expect(element.isolateScope().range(4)).toEqual([0, 1, 2, 3]);
            expect(element.isolateScope().range(1, 4)).toEqual([1, 2, 3]);
            expect(element.isolateScope().range(4, 1)).toEqual([]);
        });

        it("should detect chrome verion less than 49", function() {
            spyOn(navigator, 'userAgent').and.returnValue('Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36');;
            expect(element.isolateScope().chromeBrowserVersion()).toEqual(60);
        });
    });
});