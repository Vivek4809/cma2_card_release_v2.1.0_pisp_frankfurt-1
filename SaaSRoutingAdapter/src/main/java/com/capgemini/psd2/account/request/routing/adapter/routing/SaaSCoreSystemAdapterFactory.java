package com.capgemini.psd2.account.request.routing.adapter.routing;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.authentication.adapter.AuthenticationAdapter;

@Component
public class SaaSCoreSystemAdapterFactory implements ApplicationContextAware, SaaSAdapterFactory {

	private ApplicationContext applicationContext;

	@Override
	public AuthenticationAdapter getAdapterInstance(String adapterName) {
		return (AuthenticationAdapter) applicationContext.getBean(adapterName);
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {

		this.applicationContext = applicationContext;
	}
}
