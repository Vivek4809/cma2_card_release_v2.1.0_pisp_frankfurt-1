package com.capgemini.psd2.validator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.config.WellKnownConfig;
import com.capgemini.psd2.exception.ClientRegistrationException;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.tpp.registration.model.RegistrationRequest;
import com.capgemini.tpp.ssa.model.SSAModel;


@RunWith(SpringJUnit4ClassRunner.class)
public class PayloadValidatorTest {
	Set<ValidationStrategy> payloadstrategies=null;
	
	RegistrationRequest regRequest=null;
	RegistrationRequest regRequest1=null;
	
	SSAModel ssaModel=null;
	
	 RequestHeaderAttributes headerAttributes=null;
	@InjectMocks
	private PayloadValidationContext payloadValidationContext;
	
	@Mock
	private WellKnownConfig wellKnownConfig;
	
	/*@Mock
	RequestHeaderAttributes requestHeaderAttributes;*/
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(PayloadValidationContext.class);
		payloadstrategies = new LinkedHashSet<ValidationStrategy>(EnumSet.allOf(PayloadValidationStrategy.class));
		regRequest = new RegistrationRequest(1537257843, "4cpi8P4dkTk200Inv1cPvO", "abcd", "1234", 1539357843, "1234",
				"4cpi8P4dkTk200Inv1cPvO", new ArrayList<String>(Arrays.asList("https://test.com")), "openid accounts", "https://auth-test.apibank-cma2plus.in",
				"client_secret_basic",
				new ArrayList<String>(Arrays.asList("authorization_code", "refresh_token", "client_credentials")),
				new ArrayList<String>(Arrays.asList("id_token")), "software_statement", "WEB", "RS256", "RS256","CN=7Jk3WzvUwVYm2rEeLFK7hb, OU=0015800000jfQ9aAAE, O=OpenBanking, C=GB");
		ssaModel = new SSAModel();
		ssaModel.setIss("OpenBanking Ltd");
		ssaModel.setOrg_id("0015800000jfQ9aAAE");
		ssaModel.setSoftware_id("4cpi8P4dkTk200Inv1cPvO");
		ssaModel.setSoftware_client_id("4cpi8P4dkTk200Inv1cPvO");
		ssaModel.setSoftware_roles(new ArrayList<String>(Arrays.asList("AISP", "PISP")));
		ssaModel.setSoftware_redirect_uris(new ArrayList<String>(Arrays.asList("https://test.com")));
		WellKnownConfig wellKnownConfig = new WellKnownConfig();
		Map<String, String>  issuer = new HashMap<>();
		issuer.put("BOI", "https://auth-test.apibank-cma2plus.in");
		issuer.put("BOIUK", "https://auth-test.apibank-cma2plus.in");
		wellKnownConfig.getIssuer().putAll(issuer);
		wellKnownConfig.setScopes(new ArrayList<String>(Arrays.asList("openid", "payments", "accounts")));
		wellKnownConfig.setResponse_types_supported(new ArrayList<String>(Arrays.asList("code", "id_token")));
		wellKnownConfig.setGrant_types_supported(
				new ArrayList<String>(Arrays.asList("authorization_code", "refresh_token", "client_credentials")));
		wellKnownConfig.setId_token_signing_alg_values_supported(new ArrayList<String>(Arrays.asList("RS256")));
		wellKnownConfig
				.setToken_endpoint_auth_methods_supported(new ArrayList<String>(Arrays.asList("client_secret_basic")));
		wellKnownConfig
				.setRequest_object_signing_alg_values_supported(new ArrayList<String>(Arrays.asList("RS256", "PS256")));
	//	wellKnownConfig.setTls_client_auth_dn("CN=7Jk3WzvUwVYm2rEeLFK7hb, OU=0015800000jfQ9aAAE, O=OpenBanking, C=GB");
		payloadValidationContext = new PayloadValidationContext(payloadstrategies,null);
		ReflectionTestUtils.setField(payloadValidationContext, "wellKnownConfig", wellKnownConfig);
		RequestHeaderAttributes requestheaderAttributes=new RequestHeaderAttributes();
		requestheaderAttributes.setChannelId("OB_EXISTING_CERT");
		requestheaderAttributes.setX_ssl_client_cn("7Jk3WzvUwVYm2rEeLFK7hb");
		requestheaderAttributes.setX_ssl_client_ncaid("7Jk3WzvUwVYm2rEeLFK7hb");
		requestheaderAttributes.setTenantId("BOI");
		ReflectionTestUtils.setField(payloadValidationContext, "headerAttributes", requestheaderAttributes);
	}
	
	@Test
	public void testValidationForAllValid(){
		RequestHeaderAttributes requestheaderAttributes=new RequestHeaderAttributes();
		requestheaderAttributes.setChannelId("OB_EXISTING_CERT");
		requestheaderAttributes.setX_ssl_client_cn("7Jk3WzvUwVYm2rEeLFK7hb");
		requestheaderAttributes.setX_ssl_client_ncaid("7Jk3WzvUwVYm2rEeLFK7hb");
		requestheaderAttributes.setTenantId("BOI");
		ReflectionTestUtils.setField(payloadValidationContext, "headerAttributes", requestheaderAttributes);
		payloadValidationContext.execute(regRequest, ssaModel,false);
	}
	
	
	
	@Test(expected = ClientRegistrationException.class)
	public void testValidationForinValid(){
		regRequest1 = new RegistrationRequest(1537257843, "4cpi8P4dkTk200Inv1cPvO", "abcd", "1234", 1539357843, "1234",
				"4cpi8P4dkTk200Inv1cPvO", new ArrayList<String>(Arrays.asList("https://test.com")), "openid accounts", "https://auth-test.apibank-cma2plus.in",
				"client_secret_basic",
				new ArrayList<String>(Arrays.asList("authorization_code", "refresh_token", "client_credentials")),
				new ArrayList<String>(Arrays.asList("id_token")), "software_statement", "WEB", "RS256", "RS256","CN=2.5.4.977Jk3WzvUwVYm2rEeLFK7hb, OU=0015800000jfQ9aAAE, O=OpenBanking, C=GB");
		RequestHeaderAttributes requestheaderAttributes=new RequestHeaderAttributes();
		requestheaderAttributes.setChannelId("OB_CERT");
		requestheaderAttributes.setX_ssl_client_cn("7Jk3WzvUwVYm2rEeLFK7hb");
		requestheaderAttributes.setX_ssl_client_ncaid("2.5.4.977Jk3WzvUwVYm2rEeLFK7h");
		ReflectionTestUtils.setField(payloadValidationContext, "headerAttributes", requestheaderAttributes);
		payloadValidationContext.execute(regRequest1, ssaModel,false);
	}
	
	@Test(expected = ClientRegistrationException.class)
	public void testValidationForIssuerMismatch(){
		Set<ValidationStrategy> payloadstrategies1=new LinkedHashSet<ValidationStrategy>();
		payloadstrategies1.add(PayloadValidationStrategy.ISSUER);
		PayloadValidationContext cnxt=new PayloadValidationContext(payloadstrategies1,null);
		SSAModel ssaModel=new SSAModel();
		ssaModel.setSoftware_id("1123");
		cnxt.execute(regRequest, ssaModel,false);
	}
	
	@Test(expected = ClientRegistrationException.class)
	public void testValidationForAudMismatch() {
		Set<ValidationStrategy> payloadstrategies1 = new LinkedHashSet<ValidationStrategy>();
		payloadstrategies1.add(PayloadValidationStrategy.AUDIENCE);
		payloadValidationContext = new PayloadValidationContext(payloadstrategies1,null);
		ReflectionTestUtils.setField(payloadValidationContext, "wellKnownConfig", wellKnownConfig);
		regRequest.setAud("1234");
		RequestHeaderAttributes requestheaderAttributes=new RequestHeaderAttributes();
		requestheaderAttributes.setChannelId("OB_EXISTING_CERT");
		requestheaderAttributes.setX_ssl_client_cn("7Jk3WzvUwVYm2rEeLFK7hb");
		requestheaderAttributes.setX_ssl_client_ncaid("7Jk3WzvUwVYm2rEeLFK7hb");
		requestheaderAttributes.setTenantId("BOI");
		ReflectionTestUtils.setField(payloadValidationContext, "headerAttributes", requestheaderAttributes);
		payloadValidationContext.execute(regRequest, ssaModel,false);
	}
	
	@Test(expected = ClientRegistrationException.class)
	public void testValidationForRedirectMismatch() {
		Set<ValidationStrategy> payloadstrategies1 = new LinkedHashSet<ValidationStrategy>();
		payloadstrategies1.add(PayloadValidationStrategy.REDIRECT_URIS);
		payloadValidationContext = new PayloadValidationContext(payloadstrategies1,null);
		ReflectionTestUtils.setField(payloadValidationContext, "wellKnownConfig", wellKnownConfig);
		regRequest.setRedirect_uris(new ArrayList<String>(Arrays.asList("https://testmismatch.com")));
		payloadValidationContext.execute(regRequest, ssaModel,false);
	}
	
	@Test(expected = ClientRegistrationException.class)
	public void testValidationForTokeEndpointMismatch() {
		Set<ValidationStrategy> payloadstrategies1 = new LinkedHashSet<ValidationStrategy>();
		payloadstrategies1.add(PayloadValidationStrategy.TOKEN_ENDPOINTS_AUTH_METHODS);
		payloadValidationContext = new PayloadValidationContext(payloadstrategies1,null);
		ReflectionTestUtils.setField(payloadValidationContext, "wellKnownConfig", wellKnownConfig);
		regRequest.setToken_endpoint_auth_method("private_jwt");
		payloadValidationContext.execute(regRequest, ssaModel,false);
	}
	
	@Test(expected = ClientRegistrationException.class)
	public void testValidationForGrantTypesMismatch() {
		Set<ValidationStrategy> payloadstrategies1 = new LinkedHashSet<ValidationStrategy>();
		payloadstrategies1.add(PayloadValidationStrategy.GRANT_TYPES);
		payloadValidationContext = new PayloadValidationContext(payloadstrategies1,null);
		ReflectionTestUtils.setField(payloadValidationContext, "wellKnownConfig", wellKnownConfig);
		regRequest.setGrant_types(
				new ArrayList<String>(Arrays.asList("authorization", "refresh_token", "client_credentials")));
		payloadValidationContext.execute(regRequest, ssaModel,false);
	}
	
	
	@Test(expected = ClientRegistrationException.class)
	public void testValidationForResponseTypesMismatch() {
		Set<ValidationStrategy> payloadstrategies1 = new LinkedHashSet<ValidationStrategy>();
		payloadstrategies1.add(PayloadValidationStrategy.RESPONSE_TYPES);
		payloadValidationContext = new PayloadValidationContext(payloadstrategies1,null);
		ReflectionTestUtils.setField(payloadValidationContext, "wellKnownConfig", wellKnownConfig);
		regRequest.setGrant_types(
				new ArrayList<String>(Arrays.asList("authorization", "refresh_token", "client_credentials")));
		payloadValidationContext.execute(regRequest, ssaModel,false);
	}
	
	@Test(expected = ClientRegistrationException.class)
	public void testValidationForSoftwareIdMismatch() {
		Set<ValidationStrategy> payloadstrategies1 = new LinkedHashSet<ValidationStrategy>();
		payloadstrategies1.add(PayloadValidationStrategy.SOFTWARE_ID);
		payloadValidationContext = new PayloadValidationContext(payloadstrategies1,null);
		ReflectionTestUtils.setField(payloadValidationContext, "wellKnownConfig", wellKnownConfig);
		regRequest.setSoftware_id("4cpi8P4dkTk200Inv1cPvO123");
		payloadValidationContext.execute(regRequest, ssaModel,false);
	}
	
	@Test(expected = ClientRegistrationException.class)
	public void testValidationForScopes() {
		Set<ValidationStrategy> payloadstrategies1 = new LinkedHashSet<ValidationStrategy>();
		payloadstrategies1.add(PayloadValidationStrategy.SCOPES);
		payloadValidationContext = new PayloadValidationContext(payloadstrategies1,null);
		ReflectionTestUtils.setField(payloadValidationContext, "wellKnownConfig", wellKnownConfig);
		regRequest.setScope("payments");
		payloadValidationContext.execute(regRequest, ssaModel,false);
	}
	
	@Test(expected = ClientRegistrationException.class)
	public void testValidationForAppType() {
		Set<ValidationStrategy> payloadstrategies1 = new LinkedHashSet<ValidationStrategy>();
		payloadstrategies1.add(PayloadValidationStrategy.APPLICATION_TYPE);
		payloadValidationContext = new PayloadValidationContext(payloadstrategies1,null);
		ReflectionTestUtils.setField(payloadValidationContext, "wellKnownConfig", wellKnownConfig);
		regRequest.setApplication_type("mobile");
		payloadValidationContext.execute(regRequest, ssaModel,false);
	}
	
	@Test(expected = ClientRegistrationException.class)
	public void testValidationForSigningAlg() {
		Set<ValidationStrategy> payloadstrategies1 = new LinkedHashSet<ValidationStrategy>();
		payloadstrategies1.add(PayloadValidationStrategy.ID_TOKEN_SIGNING_ALG);
		payloadValidationContext = new PayloadValidationContext(payloadstrategies1,null);
		ReflectionTestUtils.setField(payloadValidationContext, "wellKnownConfig", wellKnownConfig);
		regRequest.setId_token_signed_response_alg("RS526");
		payloadValidationContext.execute(regRequest, ssaModel,false);
	}
	
	@Test(expected = ClientRegistrationException.class)
	public void testValidationForRequestAlg() {
		Set<ValidationStrategy> payloadstrategies1 = new LinkedHashSet<ValidationStrategy>();
		payloadstrategies1.add(PayloadValidationStrategy.REQUEST_OBJECT_SIGNING_ALG);
		payloadValidationContext = new PayloadValidationContext(payloadstrategies1,null);
		ReflectionTestUtils.setField(payloadValidationContext, "wellKnownConfig", wellKnownConfig);
		regRequest.setRequest_object_signing_alg("ES152");
		payloadValidationContext.execute(regRequest, ssaModel,false);
	}
	
	@Test(expected = UnsupportedOperationException.class)
	public void testValidationForUnsupportedOperation() {
		ValidationStrategy payloadValidation = PayloadValidationStrategy.UNSUPPORTED;
		payloadValidation.validate(headerAttributes, ssaModel);
	}
}
