package com.capgemini.psd2.adapter.routing.test.data;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.joda.time.LocalDateTime;

import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1Data;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1;
import com.capgemini.psd2.aisp.domain.OBExternalPermissions1Code;
import com.capgemini.psd2.aisp.domain.OBExternalRequestStatus1Code;



public class AccountRequestRoutingAdapterTestMockData {

	public static OBReadConsentResponse1 postAccountRequestPOSTResponse(OBReadConsentResponse1Data data) {

		OBReadConsentResponse1 accountRequestPOSTResponse = new OBReadConsentResponse1();
		
		accountRequestPOSTResponse.setData(data);
		accountRequestPOSTResponse.setRisk(null);
		return accountRequestPOSTResponse;
	}

	public static OBReadConsentResponse1 getAccountRequestGETResponse(String accountRequestId) {
		
		OBReadConsentResponse1 accountRequestPOSTResponse = new OBReadConsentResponse1();
		OBReadConsentResponse1Data data = createData();
		
		accountRequestPOSTResponse.setData(data);
		accountRequestPOSTResponse.setRisk(null);		
		return accountRequestPOSTResponse;
	}

	public static OBReadConsentResponse1 updateAccountRequestResponse(String accountRequestId, OBExternalRequestStatus1Code statusEnum) {

		OBReadConsentResponse1 accountRequestPOSTResponse = new OBReadConsentResponse1();
		OBReadConsentResponse1Data data = createData();
		data.setStatus(statusEnum);
		accountRequestPOSTResponse.setData(data);
		accountRequestPOSTResponse.setRisk(null);		
		return accountRequestPOSTResponse;
	}

	public static OBReadConsentResponse1Data createData() {
		
		OBReadConsentResponse1Data data = new OBReadConsentResponse1Data();
		data.setConsentId(UUID.randomUUID().toString());
		data.setStatus(OBExternalRequestStatus1Code.AWAITINGAUTHORISATION);
		
		List<OBExternalPermissions1Code> permissions = new ArrayList<>();
		permissions.add(OBExternalPermissions1Code.READACCOUNTSBASIC);
		permissions.add(OBExternalPermissions1Code.READACCOUNTSDETAIL);
		data.setPermissions(permissions);

		data.setCreationDateTime(LocalDateTime.now().toString());
		data.setExpirationDateTime("2017-08-02T00:00:00-00:00");
		data.setTransactionFromDateTime("2017-05-03T00:00:00-00:00");
		data.setTransactionToDateTime("2017-05-03T00:00:00-00:00");
		return data;
	}

}
